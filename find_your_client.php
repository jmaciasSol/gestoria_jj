<?php

// www.cursosmegaup.bid

$active = "active";
include "head.php";
include "header.php";
include "aside.php";
include_once "config/config.php";

$_SESSION['tiempo'] = time();

?>

<div class="content-wrapper">
    <section class="content-header" hidden>
        <h1></h1>
        <ol class="breadcrumb">
            <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Importar Activaciones</li>
        </ol>
    </section>
    <!DOCTYPE html>
    <html>
        <head>
            <title></title>
        </head>
        <body>
            <h1 style="padding-top: 25px;">&nbsp;&nbsp;Buscar Cliente</h1>
            <div id="loader" style="display: none"></div>
            <!--Inicio de Formulario-->
            <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
                <form>
                    <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="vin">VIN</label>
                                <input type="text" class="form-control" id="vin" name="vin" minlength="17"maxlength="17" pattern="[A-Za-z0-9]{17}" title="El VIN es una combinación de 17 letras y números">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="numero_cliente">Número Cliente</label>
                                <input type="text" class="form-control" id="numero_cliente" name="numero_cliente"  title="No se permiten letras ni caracteres especiales.">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="folio">Folio</label>
                                <input type="text" class="form-control" id="folio" name="folio" minlength="8" maxlength="8" pattern="[0-9]{8}" title="El Folio debe contener 8 dígitos, no se permiten letras ni caracteres especiales." >
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" pattern="[A-Za-z]{8}" title="El Nombre de cliente debe contener sólo letras, no se permiten digitos ni caracteres especiales.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="fecha_1">Fecha 1</label>
                                <input type="date" class="form-control" id="fecha_1" name="fecha_1">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="fecha_2">Fecha 2</label>
                                <input type="date" class="form-control" id="fecha_2" name="fecha_2">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="proceso">Proceso</label>
                                <select name="proceso" id="proceso" class="form-control">
                                    <option value="">Seleccionar </option>
                                    <option value="1">Cotización</option>
                                    <option value="2">Revisión</option>
                                    <option value="3">Trámite</option>
                                    <option value="4">Auditoría</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="estatus">Estatus</label>
                                <select name="estatus" id="estatus" class="form-control">
                                    <option value="">Seleccionar </option>
                                    <option value="0">Pendiente</option>
                                    <option value="1">Aprobado</option>
                                    <option value="2">Rechazado</option>
                                    <option value="3">Incompleto</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                            <div class="col pull-left">
                                <button type="button" name="buscar" id="buscar" class="btn btn-info" style="font-size: 20px;">
                                    <i class="fa fa-search"></i>
                                    <b>&nbsp; &nbsp; Buscar</b>
                                </button>
                                <button type="reset" id="borrar" name="borrar" class="btn btn-danger" style="font-size: 20px;">
                                    <i class="glyphicon glyphicon-erase" aria-hidden="true"></i>
                                    <b>&nbsp; &nbsp;Borrar</b>
                                </button>
                            </div>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
            <!--Fin formulario-->
            <!--Inicio Tabla-->
            <div class="container-fluid">
                <div class="row" id="tabla">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <table id="ResultTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <th style="text-align:center">NO. CLIENTE</th>
                                <th style="text-align:center">NOMBRE DEL CLIENTE</th>
                                <th style="text-align:center">FECHA DE ALTA</th>
                                <th style="text-align:center">ASIGNADO</th>
                                <th style="text-align:center">PROCESO</th>
                                <th style="text-align:center">ESTATUS</th>
                                <th style="text-align:center">EXPEDIENTE</th>
                                <th style="text-align:center">DOCUMENTOS</th>
                                <th style="text-align:center">BITACORA</th>
                                <th style="text-align:center">TAREAS</th>
                            </thead>
                            <tbody id="respuesta">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--Fin tabla-->
            <!--Modal Bitacora-->
            <div class="modal fade modal-xl" id="Modal-Bitacora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="vertical-alignment-helper">
                    <div class="modal-dialog vertical-align-center">
                        <div class="modal-content" style=" min-width:1000px !important;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel"><b>Bitacora</b></h4>
                            </div>
                            <div class="container text-left" >
                                <label class="modal-title" id="label_bitacora"></label>
                            </div>
                            <div class="modal-body"  style="overflow-y: auto;">
                                <div class="outer" style=" min-height: 200px !important;">                            
                                    <table border="1" id="bitacora" style="border-collapse: collapse; width: 100%;">
                                        <thead>
                                            <tr>
                                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">NO. CLIENTE</th>
                                            <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">FECHA</th>
                                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">ROL</th>
                                            <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">USUARIO</th>
                                            <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">PROCESO</th>
                                            <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">ESTATUS</th>
                                            <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">COMENTARIO</th>
                                            </tr>
                                        </thead>
                                        <tbody id="respuesta_bitacora">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade modal-xl" id="Modal-Documentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="vertical-alignment-helper">
      <div class="modal-dialog vertical-align-center">
          <div class="modal-content" style=" min-width:700px !important; ">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel"><b>Documentos</b></h4>
              </div>
              <div class="container text-left" >
                  <label class="modal-title" id="label_bitacora"></label>
              </div>
              <div class="modal-body" style="overflow-y: auto;">
                  <div class="outer" style=" min-height: 200px !important;">
                  <table border="1" id="bitacora">
                      <thead>
                          <tr>
                          <th class="cabecera_popup" style="width: 100px;" >FECHA</th>
                          <th class="cabecera_popup">DOCUMENTO</th>
                          <th class="cabecera_popup" style="width: 100px;">Descargar</th>

                          </tr>
                      </thead>
                      <tbody id="documentos">
                      </tbody>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>

            <form id="formDescargarDocumento" method="post" action="download.php" hidden>
              <input id="keyvalue" type="text" name="key">
            </form>


          </div>
      </div>
  </div>
</div>
            <!--Final Modal Bitacora-->
            <!--Script necesarios-->
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

            <!--Script para el ajax-->
            <script type="text/javascript">


                $(document).on('click', '.descargarDocumento', function(){

                  $('#keyvalue').val(this.id);

                  document.getElementById("formDescargarDocumento").submit();          

                });


                
                //Preparar tabla
                $(document).ready(function() {
                    $('#ResultTable').DataTable();
                    
                    //Inicio de función buscar
                    $('#buscar').click(function() {

                        let vin = document.getElementById('vin').value;
                        let numero_cliente = document.getElementById('numero_cliente').value;
                        let folio = document.getElementById('folio').value;
                        let nombre = document.getElementById('nombre').value;
                        let fecha_1 = document.getElementById('fecha_1').value;
                        let fecha_2 = document.getElementById('fecha_2').value;
                        let proceso = document.getElementById('proceso').value;
                        let estatus = document.getElementById('estatus').value;
                        let respuesta = document.querySelector('#respuesta');
                        respuesta.innerHTML = '';

                        //Validación formulario vacío
                        if (vin == "" && numero_cliente == "" && folio == "" && nombre == "" && fecha_1 == "" && fecha_2 == "" && proceso == "" && estatus == "") {
                            swal({
                                title: "Atención!",
                                text: "El formulario está vacío, por favor intente de nuevo.",
                                icon: "warning",
                                button: "Reintentar",
                            });
                        } else if (nombre != "" && (fecha_1 == "" || fecha_2 == "")) {
                            swal({
                                title: "Atención!",
                                text: "Seleccione un intervalo de fechas para realizar una búsqueda por nombre.",
                                icon: "warning",
                                button: "Reintentar",
                            });
                        } else if ((proceso != "" || estatus != "") && (fecha_1 == "" || fecha_2 == "")) {
                            swal({
                                title: "Atención!",
                                text: "Seleccione un intervalo de fechas para realizar una búsqueda por estatus o proceso.",
                                icon: "warning",
                                button: "Reintentar",
                            });
                        } else {
                            $.ajax({
                                url: 'buscar.php',
                                type: 'POST',
                                async: true,
                                dataType: "json",
                                data: {
                                    'vin': vin,
                                    'numero_cliente': numero_cliente,
                                    'folio': folio,
                                    'nombre': nombre,
                                    'fecha_1': fecha_1,
                                    'fecha_2': fecha_2,
                                    'proceso': proceso,
                                    'estatus': estatus
                                },
                                success: function(response) {

                                    if (response == "") {
                                        swal({
                                            title: "Aviso!",
                                            text: "No encontramos ningún registro con los datos ingresados.",
                                            icon: "warning",
                                            button: "Reintentar",
                                        });
                                    } else {
                                        //$('#respuesta').val(response);

                                        for (let i = 0; i < response.length; i++) {
                                            respuesta.innerHTML += `
                                        <tr align="center">
                                            <td>${response [i][0]}</td>
                                            <td>${response [i][3]}</td>
                                            <td>${response [i][4]}</td>
                                            <td>+</td>
                                            <td>${response [i][5]}</td>
                                            <td>${response [i][6]}</td>
                                            <td>+</td>
                                            <td>
                                                <a data-toggle="modal" class="popup-documentos" id="${response[i][7]}" href="#Modal-Documentos">Ver</a>
                                            </td>
                                            <td>
                                                <a data-toggle="modal" class="llamar-popup" id="${response[i][7]}" href="#Modal-Bitacora">Ver</a>
                                            </td>
                                            <td>
                                                <form id="form_tareas${(i+1)}"method = "POST" action = "tareas.php">
                                                    <input type="hidden" class="form-control" id="input_${response[i][7]}" name="id" value = "${response[i][7]}" aria-hidden="true">
                                                    <a href="javascript:;" onclick="document.getElementById('form_tareas${(i+1)}').submit();">Ver</a>
                                                </form>

                                            </td>
                                        </tr> 
                                    `
                                        }
                                    }

                                },
                                error: function(request, err) {
                                    console.log(err)
                                }
                            });
                        }
                    });



                    $('#borrar').click(function() {
                        respuesta.innerHTML = '';
                    });

                    //Funcion popup
                    $(document).on('click', '.llamar-popup', function(){

                        let id_cliente = $(this).attr('id');
                        let respuesta_bitacora = document.querySelector('#respuesta_bitacora');
                        respuesta_bitacora.innerHTML = '';

                        $.ajax({
                            url:'popup_bitacora.php',
                            type: 'POST',
                            async: true,
                            dataType: "json",
                            data: {
                                'id_cliente' : id_cliente
                            },
                            success: function(respuesta) {
                                if (respuesta == "") {
                                        swal({
                                            title: "Aviso!",
                                            text: "No se encontraron datos.",
                                            icon: "warning",
                                            button: "Reintentar",
                                        });
                                    }else {
                                        //$('#respuesta-bitacora').val(respuesta_bitacora);
                                        for(let i= 0; i<respuesta.length; i++){
                                            respuesta_bitacora.innerHTML += `
                                            <tr align="center">
                                                <td>${respuesta[i][0]}</td>
                                                <td>${respuesta[i][1]}</td>
                                                <td>${respuesta[i][2]}</td>
                                                <td>${respuesta[i][3]}</td>
                                                <td>${respuesta[i][4]}</td>
                                                <td>${respuesta[i][5]}</td>
                                                <td>${respuesta[i][6]}</td>
                                            </tr>
                                            `
                                        }
                                    }
                            },
                            error: function(request, err){
                                console.log(err);
                            }
                        });
                    });

                    //Sección de validaciones

                    $('#vin').change(function(){
                        let vin = $('#vin').val();
                        if(vin.length != 17){
                            swal({
                                title: "Atención!",
                                text: "El VIN debe tener 17 digitos, por favor intente de nuevo.",
                                icon: "warning",
                                button: "Reintentar",
                            });
                        }
                    });


                    $('#folio').change(function(){
                        let vin = $('#folio').val();
                        if(vin.length != 8){
                            swal({
                                title: "Atención!",
                                text: "El Folio debe tener 8 digitos, por favor intente de nuevo.",
                                icon: "warning",
                                button: "Reintentar",
                            });
                        }
                    });


                    $(document).on('click', '.popup-documentos', function(){

                      let identificador= this.id;
                      let values = identificador.split("_");
                      let id = values[0];
                      let no_cliente = values[0];
                      let respuesta_documentos = document.querySelector('#documentos');
                      respuesta_documentos.innerHTML = '';

                          $.ajax({
                            url:'action/getDocumentos.php',
                            type: 'POST',
                            async: true,
                            dataType: "json",
                            data: {
                                'id' : id
                            },
                            success: function(resp) {
                              for(let i= 0; i<resp.length; i++){
                                respuesta_documentos.innerHTML += `
                                <tr align="center">
                                  <td>${resp[i]['FECHA']}</td>
                                  <td>${resp[i]['DOCUMENTO']}</td>
                                  <td><button class="descargarDocumento btn-default" id="${resp[i]['DOCUMENTO']}">Descargar</button></td>
                                </tr>`;
                              }
                            },
                            error: function(request, err){
                                console.log(err);
                            }
                        });

                    });



                });
            </script>

        </body>

    </html>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>