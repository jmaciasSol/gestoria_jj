
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>

<?php 
    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 
    include "action/encript.php";


    function alert($msg,$val) {

        if($val == 1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val == 2){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','info');</script>";
        }else if($val == 3){
             echo "<script type='text/javascript'>swal('Archivo Imcopatible o vacio','$msg','error');</script>";
        }else if($val == 4){
           echo "<script type='text/javascript'>swal('Comprobante cargado y Registro movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Registro rechazado','$msg','error');</script>";
        }        
    
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

          switch ($_GET['success']) {
              case 'true':
                  alert("Se han cargado exitosamente",1); 
                  break;
              case 'false':
                  alert("No se pudo guradar el archivo ",2);
                  break;
              case 'error':
                  alert("Revise el formato (Columnas del archivo)",3);
                  break;
          }
        
        }

        if(!empty($_GET['move_success'])){

          switch ($_GET['move_success']) {
              case 'true':
                  alert("A tabla En proceso",4); 
                  break;
              case 'false':
                  alert("",5);
                  break;
          }
        
        }
    
    }

?>

<div class="content-wrapper" >
  <section class="content-header" hidden>
    <h1></h1>
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Importar Activaciones</li>
    </ol>
  </section>

        
<?php

include_once "config/config.php";
$datos = $con->query("SELECT * FROM v_cotizacion WHERE MANUAL = 1 and ACTUALIZADO = 0; ");
$datos_cotizaciones = $con->query("select * from v_cotizacion where NO_CLIENTE <> '' ");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>

  <h1 style="padding-top: 25px;">&nbsp;&nbsp;COTIZACIONES MANUALES</h1>

  <div id="loader" style="display: none" ></div>

  <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
    <form method="post" id="addproduct" action="import_4.php" enctype="multipart/form-data" role="form">
      <div <?php if($usuario == 0 && $comercial == 0 && $admin_crfact == 0){echo "hidden";} ?> >
        <div class="row">
          <label class="custom-file-upload btn-info" style="padding-bottom: 10px; border-radius: 5px; border:solid 1px; ">
          <input type="file" name="name" class="custom-file-upload" id="carga_archivo" placeholder="Archivo (.xlsx)">
            <i class="fa fa-cloud-upload"></i>&nbsp; &nbsp;Cargar Archivo
          </label>
          <button type="submit" class="btn btn-success" id="btn-submit" style="font-size: 20px;" onclick="start_loader()" ><i class="fa fa-cloud-download" aria-hidden="true">
          </i><b>&nbsp; &nbsp;Importar Datos</b></button>
        </div>
      </div>
      <div class="row" id="div_file_name" style="display: none; width: 500px;" >
        <input type="text" id="name_file" style="width: 390px;" disabled>
      </div>
    </form>
  </div>

<?php if($datos->num_rows > 0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table-bordered table-hover display compact" style="width:100%" >
  <thead>
    <th style="text-align:center">Fecha</th>
    <th style="text-align:center">N°cliente</th>
    <th style="text-align:center">Tipo</th>  
    <th style="text-align:center">Marca</th>
    <th  style="text-align:center">Ejecutivo</th>
    <th  style="text-align:center">Proceso de Cotizaci&oacute;n</th> 
    <th style="text-align:center">Guardar</th>
   
  <tbody>
    <?php while($d= $datos->fetch_object()):?>

    <tr align="center">
    <td >
      <?php 
        echo 
        '<a data-toggle="modal" href="#Modal_datos_'.$d->NO_CLIENTE.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></a>
        <div class="modal fade" id="Modal_datos_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                            </button>
                             <h3 class="modal-title" id="myModalLabel"><b>'.encrypt_decrypt('decrypt',$d->NOMBRE).'</b></h3>

                        </div>
                        <div class="modal-body" style="text-align:left;">
                         <ul>
                          <li>MODELO: '.$d->MODELO.'</li>
                          <li>VERSI&Oacute;N: '.$d->VERSION.'</li>
                          <li>AÑO: '.$d->ANIO.'</li>
                          <li>VALOR FACTURA: '.'$'.number_format($d->VALOR_FACTURA, 2).'</li>
                          <li>ESTADO ORIGEN: '.$d->EDO_ORIGEN.'</li>
                          <li>PDV: '.$d->PDV.'</li>
                          <li>VENDEDOR: '.$d->VENDEDOR.'</li>
                         </ul>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
      ?>
      <?php echo "&nbsp".$d->FECHA; ?>
    </td>

    <td class="numero_cliente"><?php echo $d->NO_CLIENTE; ?></td>
    <td  id="<?php echo "tipo_unidad_$d->NO_CLIENTE"; ?>" class="<?php echo "vin_".$d->NO_CLIENTE; ?>"><?php echo $d->TIPO; ?></td>
    <td class="<?php echo "nombre_".$d->NO_CLIENTE; ?>"><?php echo $d->MARCA; ?></td>
    <td class="<?php echo "entidad_".$d->NO_CLIENTE; ?>"><?php echo $d->EJECUTIVO; ?></td>
    <td class="estado">
      <?php
        echo 
        '<button class="llamar-popup '.encrypt_decrypt('decrypt',$d->NOMBRE).'" id="'.$d->ID.'" data-toggle="modal" data-target="#Modal-Cotizacion" style="width: 80px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </button>
        ';
      ?>
  
    <td>
      <?php
        echo
        '<form id="form_'.$d->NO_CLIENTE.'" action="en_proceso.php" method="POST" style="display:block; margin:auto;">
          <input type="text" name="numero_cliente" value="'.$d->NO_CLIENTE.'" hidden>
          <input type="text" id="ID_'.$d->NO_CLIENTE.'" name="ID" value="'.$d->ID.'" hidden >
          <input type="text" name="nombre_cliente" value="'.$d->NOMBRE.'" hidden>
          <input type="text" name="agencia" value="'.$d->EDO_ORIGEN.'" hidden>
          <input type="text" name="unidad" value="'.$d->MARCA.'_'.$d->VERSION.'" hidden>
        </form>
        <button class="btn btn-primary" type="button" id="Guardar_'.$d->NO_CLIENTE.'" onclick="mi_funcion('.$d->NO_CLIENTE.')" disabled 
          ><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
        ';
      ?>
    </td>
  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<?php else:?>
  <h3 style="padding-left:15px;">NO SE HAN CARGADO DATOS</h3>
<?php endif; ?>

  <div class="modal fade" id="Modal-Cotizacion" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
              <div class="modal-content" style=" min-width:1000px !important;">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Cerrar</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel"><b id="texto-modal"></b></h4>

                </div>
                <div class="container text-left">
                  <label class="modal-title" id="label_' . $d->NO_CLIENTE . '"></label>
                </div>
                <div class="modal-body">
                  <table id="modal-table" border="1" class="table-bordered table-hover" style="width:100%">
                    <thead>
                      <th class="cabecera_popup" >COTIZACI&Oacute;N</th>
                      <th class="cabecera_popup" >ESTADO</th>
                      <th class="cabecera_popup" >TIPO</th>
                      <th class="cabecera_popup" >TRAMITE</th>
                      <th class="cabecera_popup" >SLA</th>
                      <th class="cabecera_popup" >AVG</th>

                      <th class="cabecera_popup" >DERECHOS</th>
                      <th class="cabecera_popup" >TENENCIA</th>
                      <th class="cabecera_popup" >TARJETA</th>
                      <th class="cabecera_popup" >OTRO</th>
                      <th class="cabecera_popup" >TOTAL</th>
                      <th class="cabecera_popup" >ESTATUS</th>
                      <th class="cabecera_popup" >ACTUALIZAR</th>
                      <th class="cabecera_popup" >ACEPTAR</th>
                    </thead>
                    <tbody id="respuesta_cotizacion">
                    </tbody>

                  </table>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
  </div>
  
</body>

</html>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
<script type="text/javascript">

  $("#myTable").DataTable({
    "destroy":true
  });
  
  $(".btn-click-action").click(function() {

    var name= $(this).attr('name');
    var identificador = name.split("_");
     
    var valor1= "valor1_"+identificador[1];
    var valor2= "valor2_"+identificador[1];
    var valor3= "valor3_"+identificador[1];
    
    var nombre = $('.nombre_'+identificador[1]).val();
    var entidad = $('.entidad_'+identificador[1]).val();
    var vin = $('.vin_'+identificador[1]).val();
    
    if($("#"+valor1).val() == "" || $("#"+valor2).val() == "" || $("#"+valor3).val() == ""){
      swal("Campos Vacios", "Llene los campos de cotizacion", "info");
    }else{
      $.ajax({
          url: "action/updatelayout.php",
          type: 'POST',
         
          async : true,
          data:{ 
            'valor1' : $("#"+input_valor1).val(),
            'valor2' : $("#"+input_valor2).val(),
            'valor3' : $("#"+input_valor3).val(),
            'nombre' : nombre,  
            'entidad' : entidad,
            'vin' : vin,
            'id' : identificador[1]
           } ,
          success: function(resp) {
            swal("Datos Actualizados", "", "success");
          },
          error: function(request,err){
            console.log("error")
            console.log(err)
          }
      });
      
    }

  });


  $(".factura").change(function(){

    var identificador=$(this).attr('id');
    var data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    var no_cliente = data[1];

    var archivos = new FormData();
    var factura = $('#Factura_'+no_cliente)[0].files[0];
    var tipo_unidad = $("#tipo_unidad_"+no_cliente).text();
    //var id = $('#ID').val();
    var id = $('#ID_'+no_cliente).val();

    archivos.append('factura', factura);
    archivos.append('no_cliente', no_cliente);
    archivos.append('tipo_unidad', tipo_unidad);
    archivos.append('id', id);
    
    swal({
      title: factura.name,
      text: "¿Cargar archivo?",
      icon: "info",
      buttons: ["Cancelar",true],
      dangerMode: true,
    })
    .then((value) => {
      if (value){
        $.ajax({
            url: 'action/uploads_factura.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response){
                if(response > 0){
                  swal('Archivo cargado exitosamente','','success');
                  $('#Factura_'+no_cliente).val('');
                }else{
                  swal({
                    title:'Error al cargar el archivo',
                    text: 'intentalo más tarde',
                    icon: 'error',
                    dangerMode: true,
                  });
                }
            },
        });      
      }else{
        $('#Factura_'+no_cliente).val('');
      }
    });
  
  });


  $(".comprobante").change(function(){

    var identificador=$(this).attr('id');
    var data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    var no_cliente = data[1];
    var comprobante = $('#Comprobante_'+no_cliente)[0].files[0];
    
    swal({
      title: comprobante.name,
      text: "Archivo seleccionado",
      icon: "info",
      buttons: ["Cancelar",true],
      dangerMode: true,
    })
    .then((value) => {
      if (!value){
        $('#Comprobante_'+no_cliente).val('');
      }
    });
  
  });

    
  function mi_funcion(no_cliente){

    if($('#Comprobante_'+no_cliente).val() != ""){

      var archivos = new FormData();
      var comprobante = $('#Comprobante_'+no_cliente)[0].files[0];
      var id = $('#ID_'+no_cliente).val();

      archivos.append('comprobante',comprobante);
      archivos.append('no_cliente',no_cliente);
      archivos.append('id', id);

      document.getElementById("loader").style.display = "block";

      $.ajax({
          url: 'action/uploads_files.php',
          type: 'post',
          data: archivos,
          contentType: false,
          processData: false,
          success: function(response){
              if(response > 0){
                   document.getElementById("form_"+no_cliente).submit();                      
              }else{
                  alert('file not uploaded');
              }
          },
      });

    }else{
      swal({
        title: "No se ha cargado Comprobante",
        text: "Agrege el documento",
        icon: "info",
        dangerMode: true,
      });
    }

  }

  function start_loader(){

    document.getElementById("loader").style.display = "block";

  }

  $("#carga_archivo").change(function(){

      var path= $(this).val();
      var name  = path.split('\\');
      var ext = name[2].split(".");

      if(ext[1] == 'xlsx'){
        console.log(ext[1]);

        $("#name_file").val("Archivo: "+name[2]);

        console.log(name);
        $("#btn-submit").attr("disabled",false);
        document.getElementById("div_file_name").style.display = "block";
       
      }else{
        console.log(ext[1]);
        $("#btn-submit").attr("disabled",true);
        swal('Formato no permitido','Cargue un documento .xlsx', 'error');

      }

  });

  $(".cotizar").click(function(){

    var name= $(this).attr('id');
    var identificador = name.split("_");
    
    document.getElementById(name).classList.remove("btn-secondary");
    document.getElementById(name).classList.add("btn-success");

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;

    derechos = $('#derechos_'+identificador[0]+'_'+identificador[2]).val();
    tenencia = $('#tenencia_'+identificador[0]+'_'+identificador[2]).val();
    tarjeta = $('#tarjeta_'+identificador[0]+'_'+identificador[2]).val();
    otros = $('#otros_'+identificador[0]+'_'+identificador[2]).val();

    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    console.log(identificador);

    console.log(derechos);
    console.log(tenencia);
    console.log(tarjeta);
    console.log(otros);

    switch(identificador[0]){

      case "cotizacion1" :

        document.getElementById("cotizacion1_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;

      
        break;

      case "cotizacion2" :

        document.getElementById("cotizacion2_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;

        break;

      case "cotizacion3" :

        document.getElementById("cotizacion3_"+(parseInt(identificador[1]))+"_"+identificador[2]).disabled = true;

        break;

    }

    var id = $('#ID_'+identificador[2]).val();
    console.log(id);
    

    $.ajax({
          url: 'action/update_cotizacionesM.php',
          type: 'post',
          data: {'folio': identificador[1],
                'id' : id,
                'derechos' : d,
                'tenencia' : t,
                'tarjeta' : tar,
                'otros' : ot
                },
          success: function(response){

            $("#label_"+identificador[2]).text("Se ha actualizado la cotización: "+identificador[1]);
            $("#label_"+identificador[2]).css("color", "green");

            $('#derechos_'+identificador[0]).prop('disabled', true);
            $('#tenencia_'+identificador[0]).prop('disabled', true);
            $('#tarjeta_'+identificador[0]).prop('disabled', true);
            $('#otros_'+identificador[0]).prop('disabled', true);

            console.log(response);
      
          },
      });

  });


  $('.test-popup-link').magnificPopup({

    type: 'image'

  });



  /*
  $(".cotizacion1").change(function(){

    //let total = parseInt($(this).val());
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[2];
    console.log(numero_cliente);

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;
    let total = 0;

    derechos = $('#derechos_cotizacion2_'+numero_cliente).val();
    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = $('#tenencia_cotizacion2_'+numero_cliente).val();
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = $('#tarjeta_cotizacion2_'+numero_cliente).val();
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = $('#otros_cotizacion2_'+numero_cliente).val();    
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    total = d + t + tar + ot;
    tot = parseFloat((total).toFixed(2));
    
    $('#total_cotizacion2_'+numero_cliente).text('$'+tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));  
    
    console.log(idElement);

    console.log($('#tenencia_cotizacion2_'+numero_cliente).val());
    console.log(d);
    console.log(t);
    console.log(tar);
    console.log(ot);

    console.log(tot);

  });

  $(".cotizacion2").change(function(){

    //let total = parseInt($(this).val());
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[2];
    console.log(numero_cliente);

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;
    let total = 0;

    derechos = $('#derechos_cotizacion2_'+numero_cliente).val();
    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = $('#tenencia_cotizacion2_'+numero_cliente).val();
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = $('#tarjeta_cotizacion2_'+numero_cliente).val();
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = $('#otros_cotizacion2_'+numero_cliente).val();    
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    total = d + t + tar + ot;
    tot = parseFloat((total).toFixed(2));
    
    $('#total_cotizacion2_'+numero_cliente).text('$'+tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));  
    
    console.log(idElement);

    console.log($('#tenencia_cotizacion2_'+numero_cliente).val());
    console.log(d);
    console.log(t);
    console.log(tar);
    console.log(ot);

    console.log(tot);


  });

  $(".cotizacion3").change(function(){

    //let total = parseInt($(this).val());
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[2];
    console.log(numero_cliente);

    let derechos = 0;
    let tenencia = 0;
    let tarjeta = 0;
    let otros = 0;
    let total = 0;

    derechos = $('#derechos_cotizacion2_'+numero_cliente).val();
    derechos = derechos.replace(/,/g, "");
    d = parseFloat(derechos);
    tenencia = $('#tenencia_cotizacion2_'+numero_cliente).val();
    tenencia = tenencia.replace(/,/g, "");
    t = parseFloat(tenencia);
    tarjeta = $('#tarjeta_cotizacion2_'+numero_cliente).val();
    tarjeta = tarjeta.replace(/,/g, "");
    tar = parseFloat(tarjeta);
    otros = $('#otros_cotizacion2_'+numero_cliente).val();    
    otros = otros.replace(/,/g, "");
    ot = parseFloat(otros);

    total = d + t + tar + ot;
    tot = parseFloat((total).toFixed(2));
    
    $('#total_cotizacion2_'+numero_cliente).text('$'+tot.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));  
    
    console.log(idElement);

    console.log($('#tenencia_cotizacion2_'+numero_cliente).val());
    console.log(d);
    console.log(t);
    console.log(tar);
    console.log(ot);

    console.log(tot);
    
  });

  */

   function formatoMexico(number){

      const exp = /(\d)(?=(\d{3})+(?!\d))/g;
      const rep = '$1,';
      let arr = number.toString().split('.');
      arr[0] = arr[0].replace(exp,rep);
      
      return arr[1] ? arr.join('.'): arr[0];

    }


     function aceptarcotizacion(num_cotizacion, id){

      let folio = $('#folio'+num_cotizacion).text();
      let edo_origen = $('#edo_origen'+num_cotizacion).text();

      $.ajax({
        url: 'action/set_autorizacion.php',
        type: 'post',
        data: {
          'folio': folio,
          'edo_origen' : edo_origen,
          'id' : id,
        },
        success: function(response){
          console.log(response);
          
          $( "#btn-aceptar1" ).prop( "disabled", true );
          $( "#btn-aceptar2" ).prop( "disabled", true );
          $( "#btn-aceptar3" ).prop( "disabled", true );

          $( "#documentos"+id ).prop( "disabled", false );

          switch(num_cotizacion){

            case 1:
              $( "#estatus1").text('ACEPTADA');
              $( "#estatus2").text('RECHAZADA');
              $( "#estatus3").text('RECHAZADA');
            break;

            case 2:
              $( "#estatus1").text('RECHAZADA');
              $( "#estatus2").text('ACEPTADA');
              $( "#estatus3").text('RECHAZADA');
            break;

            case 3:
              $( "#estatus1").text('RECHAZADA');
              $( "#estatus2").text('RECHAZADA');
              $( "#estatus3").text('ACEPTADA');
            break;

          }

        },
        error: function(request, err) {
          console.log(err);
        }
      });

    }

    function sumar(num_cotizacion){

      let folio = $('#folio'+num_cotizacion).text();
      let derechos = parseInt($('#derechos_'+num_cotizacion).val());
      let tenencia = parseInt($('#tenencia_'+num_cotizacion).val());
      let tarjeta = parseInt($('#tarjeta_'+num_cotizacion).val());
      let otro = parseInt($('#otro_'+num_cotizacion).val());

      $('#total_'+num_cotizacion).text(formatoMexico(derechos+tenencia+tarjeta+otro));

      $.ajax({
        url: 'action/update_cotizacionesM.php',
        type: 'post',
        data: {
          'folio': folio,
          'derechos' : derechos,
          'tenencia' : tenencia,
          'tarjeta' : tarjeta,
          'otros' : otro
        },
        success: function(response){
          console.log(response);
        },
        error: function(request, err) {
          console.log(err);
        }
      });
    
    }




    $(document).on('click', '.llamar-popup', function() {

      let no_cliente = $(this).attr('id');
      let nombre_cliente = $(this).attr('class');
      let spltittedName = nombre_cliente.substring(13);
      let texto_modal = document.querySelector('#texto-modal');
      let respuesta_cotizacion = document.querySelector('#respuesta_cotizacion');

      respuesta_cotizacion.innerHTML = '';
      texto_modal.innerHTML = '';
      
      $.ajax({
        url: 'action/listar_cotizaciones.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
          'no_cliente': no_cliente
        },
        success: function(respuesta) {
          texto_modal.innerHTML = `Iniciar Proceso de Cotización: ${spltittedName}`;
            
            let numberFormat = new Intl.NumberFormat();
            let valdisabled = '';

            for (let i = 0; i < respuesta.length; i++) {


              disabled = (respuesta[i]['ESTATUS'] !== 'PENDIENTE' ) ? 'disabled' : '' ;

              respuesta_cotizacion.innerHTML += `
                <tr align='center'>
                  <td id='folio${i+1}' >${respuesta[i]['FOLIO']}</td>
                  <td id='edo_origen${i+1}'>${respuesta[i]['EDO_COTIZACION']}</td>
                  <td>${respuesta[i]['AUTOMATICA']}</td>
                  <td>${respuesta[i]['TRAMITE']}</td>
                  <td>${respuesta[i]['SLA']}</td>
                  <td>${respuesta[i]['SLA_AVG']}</td>
                  <td>
                    <input type='text' id='derechos_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_DERECHOS']}' maxlength="6" size="4">
                  </td>
                  <td>
                    <input type='text' id='tenencia_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_TENENCIA']}' maxlength="6" size="4">
                  </td>
                  <td>
                    <input type='text' id='tarjeta_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_TARJETA']}' maxlength="6" size="4">
                  </td>
                  <td>
                    <input type='text' id='otro_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_OTRO']}' maxlength="6" size="4">
                  </td>
                  <td style='width:90px;' id='total_${i+1}' >${formatoMexico(parseInt(respuesta[i]['MTO_DERECHOS'])+parseInt(respuesta[i]['MTO_TENENCIA'])+
                    parseInt(respuesta[i]['MTO_TARJETA'])+parseInt(respuesta[i]['MTO_OTRO']))}
                  </td>
                  <td id='estatus${i+1}' >${respuesta[i]['ESTATUS']}</td>
                  <td>
                    <button class='btn btn-info' onclick='sumar(${i+1})' >
                      <i class="fa fa-refresh" aria-hidden="true"></i>
                    </button>
                  </td>
                  <td>
                    <button id='btn-aceptar${i+1}' class='btn btn-success' onclick='aceptarcotizacion(${i+1},${respuesta[i]['ID']})' ${disabled} >
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
                `;
               
            }

             respuesta_cotizacion.innerHTML += `
                <br><br>
                <tr>
                  <td>
                    <input type='text' placeholder='VIN' />
                  </td>
                  <td>
                    <button class='btn btn-info'>
                      <i class="fa fa-refresh" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type='text' placeholder='PLACAS' />
                  </td>
                  <td>
                    <button class='btn btn-info'>
                      <i class="fa fa-refresh" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
                
                `;
        },
        error: function(request, err) {
          console.log(err);
        }
      });

    });



  function blockinputs(cotizacion_n, numero_cliente){

    derechos = parseInt($('#derechos_'+cotizacion_n+'_'+numero_cliente).val());
    tenencia = parseFloat($('#tenencia_'+cotizacion_n+'_'+numero_cliente).val());
    tarjeta = parseInt($('#tarjeta_'+cotizacion_n+'_'+numero_cliente).val());
    otros = parseInt($('#otros_'+cotizacion_n+'_'+numero_cliente).val());

    (derechos > 0) ? 
      $('#derechos_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#derechos_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;

    (tenencia > 0) ? 
      $('#tenencia_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#tenencia_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;

    (tarjeta > 0) ? 
      $('#tarjeta_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#tarjeta_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;

    (otros > 0) ? 
      $('#otros_'+cotizacion_n+'_'+numero_cliente).prop('disabled', true) 
      : $('#otros_'+cotizacion_n+'_'+numero_cliente).prop('disabled', false) ;
  }

  $(document).on('click','.popup',function(){
    let idElement = $(this).attr('id');
    let attrElement = idElement.split("_");
    let numero_cliente = attrElement[1];
    //console.log(attrElement);
    blockinputs("cotizacion1", numero_cliente);
    blockinputs("cotizacion2", numero_cliente);
    blockinputs("cotizacion3", numero_cliente);

  });


</script>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>
