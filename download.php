<?php

  require 'composer/vendor/autoload.php';
  include "action/encript.php";
  include "config/config.php";

  use Aws\S3\S3Client;
  use Aws\Exception\AwsException;

  $datos = $con->query("SELECT * FROM tbl_keys");
  $d = $datos->fetch_all(MYSQLI_ASSOC);

  $key = $d[count($d)-1]['KEY_'];
  $secrectkey = $d[count($d)-1]['SECRETKEY'];

    $S3_Options = [
    'version' => 'latest',
    'region' => 'us-west-2',
    'credentials' => [

      'key' => encrypt_decrypt('decrypt', $key),
      'secret' => encrypt_decrypt('decrypt', $secrectkey)
    ]
  ];

  $S3 = new S3Client($S3_Options);

  if($_POST){

      $getFile = $S3->getObject([
        'Key' => 'test/'.$_POST['key'],
        'Bucket' => 'crfact'
      ]);

      $getFile = $getFile->toArray();

      header("Content-Type: {$getFile['ContentType']}");
      header('Content-Disposition: attachment; filename='.$_POST['key']);
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');

      echo $getFile['Body'];
      
  }

?>