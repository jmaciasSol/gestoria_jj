<?php

  require 'PHPMailer/PHPMailerAutoload.php';

    function sendMail($folio,$estado,$sla,$derechos,$tenencia,$tarjeta,$otro, $total,$num_cliente){


      $mail = new PHPMailer(true);
      $mail->isSMTP();
          
     
      $mail->Host = 'smtp.office365.com';
      $mail->Port = 587;

      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'tls';
      

      $mail->isHTML(true);
      
      $mail->Username = 'tramites@jjsolucionesempresariales.com';
      $mail->Password = 'QWnxa309';
      $mail->CharSet = 'UTF-8';

      $mail->setFrom('tramites@jjsolucionesempresariales.com');

      $mail->addAddress('jonathan.macias@jjsolucionesempresariales.com');
      $mail->addAddress('aldo1rodrigo@gmail.com');
      
      $mail->Subject = '¡Nueva solicitud recibida con éxito! ['.$num_cliente.']';

      $mail->Body = ' 

      <html>
        <head>
          <style type="text/css">
            table {border: 2px solid #A40808;background-color: #EEE7DB;width: 100%;text-align: center;border-collapse: collapse;}
            table td, table th {border: 1px solid #AAAAAA;padding: 3px 2px;}
            table tbody td {font-size: 13px;}
            table tr:nth-child(even) {background: #F5C8BF;}
            table thead {background: #A40808;}
            table thead th {font-size: 15px;font-weight: bold;color: #FFFFFF;text-align: center;border-left: 2px solid #A40808;}
            table thead th:first-child {border-left: none;}
            table tfoot {font-size: 13px;font-weight: bold;color: #FFFFFF;background: #A40808;}
            table tfoot td {font-size: 13px;}
            table tfoot .links {text-align: right;}
            table tfoot .links a{display: inline-block;background: #FFFFFF;color: #A40808;padding: 2px 8px;border-radius: 5px;}            
          </style>
        </head>

        <body>

          <p>Hola, tenemos noticias de tu cotización a nombre de: <b>'.$nombre.'</b></p>
          <p>Actualizamos los montos conforme a la siguiente tabla:</p>

          <table class="default" style="border: 2px solid #A40808;background-color: #EEE7DB;width: 100%;text-align: center;border-collapse: collapse;">
            <thead style="background: #A40808;">
              <tr>
                <th style="color:red;">FOLIO</th>
                <th style="border: 1px solid #AAAAAA;padding: 3px 2px;">ESTADO</th>
                <th style="border: 1px solid #AAAAAA;padding: 3px 2px;">SLA</th>
                <th style="border: 1px solid #AAAAAA;padding: 3px 2px;">$ DERECHOS</th>
                <th style="border: 1px solid #AAAAAA;padding: 3px 2px;">$ TENENCIA</th>
                <th style="border: 1px solid #AAAAAA;padding: 3px 2px;">$ TARJETA</th>
                <th style="border: 1px solid #AAAAAA;padding: 3px 2px;">$ OTRO</th>
                <th style="border: 1px solid #AAAAAA;padding: 3px 2px;">$ TOTAL</th>
              </tr>
            </thead>
            <tbody style="font-size: 13px;">
              <tr>
               <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$folio.'</th>
                <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$estado.'</th>
                <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$sla.'</th>
                <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$derechos.'</th>
                <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$tenencia.'</th>
                <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$tarjeta.'</th>
                <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$otro.'</th>
                <td style="border: 1px solid #AAAAAA;padding: 3px 2px;">'.$total.'</th>
              </tr>
            </tbody>
           
          </table>

          <p>*Recuerda que estos precios pueden cambiar.</p>
          <b>Gestoría J&J IB.</b>
       
        </body>
        </html>

        ';
      
      $mail->send();

    }

?>
