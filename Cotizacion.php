<?php
$active = "active";
include "head.php";
include "header.php";
include "aside.php";
include_once "config/config.php";
include "action/encript.php";

$_SESSION['tiempo'] = time();

function alert($msg, $val) {
  if ($val == 1) {
    echo "
      <script type='text/javascript'>
        swal({
          title: 'Datos Cargados',
          text: '$msg',
          icon: 'success',
        }).then((willDelete) => {";
    $_SESSION['success'] = '';
    echo "});
      </script>";
  } else if ($val == 2) {
    echo "<script type='text/javascript'>swal('Datos Cargados','$msg','info');</script>";
  } else if ($val == 3) {
    echo "<script type='text/javascript'>swal('Archivo Incompatible o vacio','$msg','error');</script>";
  } else if ($val == 4) {
    echo "<script type='text/javascript'>swal('Comprobante cargado y Registro movido','$msg','success');</script>";
  } else {
    echo "<script type='text/javascript'>swal('Registro rechazado','$msg','error');</script>";
  }
}

if (empty($_SESSION)) {
} else {
  if (!empty($_SESSION['success'])) {
    switch ($_SESSION['success']) {
      case 'true':
        alert("Se han cargado exitosamente", 1);
        break;
      case 'false':
        alert("No se pudo guradar el archivo ", 2);
        break;
      case 'error':
        alert("Revise el formato (Columnas del archivo)", 3);
        break;
    }
  }

  if (!empty($_GET['move_success'])) {

    switch ($_GET['move_success']) {
      case 'true':
        alert("A Revisión", 4);
        break;
      case 'false':
        alert("", 5);
        break;
    }
  }
}
?>

<div class="content-wrapper">
  <section class="content-header" hidden>
    <h1></h1>
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Importar Activaciones</li>
    </ol>
  </section>

  <?php
  $datos = $con->query("SELECT * FROM v_cotizacion WHERE NO_CLIENTE <> '' ");
  ?>

  <!DOCTYPE html>
  <html>

  <body>
    <div class="container-fluid row">
      <div class="col-lg-4 col-xs-6">
        <h1 style="padding-top: 25px;">&nbsp;&nbsp;COTIZACIONES</h1>
      </div>
      <div class="col-lg-5 col-md-offset-3 col-xs-6">
        <a class="test-popup-link" href="images/Requisitos.jpeg"><img class="img-responsive center-block" src="images/Requisitos.jpeg" alt="Italian Trulli" height="150px" width="200px"></a>
      </div>
    </div>
    <div id="loader" style="display: none"></div>
    <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
      <form method="post" id="addproduct" action="import_4.php" enctype="multipart/form-data" role="form">
        <div <?php if ($usuario == 0 && $comercial == 0 && $admin_crfact == 0) {
                echo "hidden";
              } ?>>
          <div class="row">
            <label class="custom-file-upload btn-info" style="padding-bottom: 10px; border-radius: 5px; border:solid 1px; ">
              <input type="file" name="name" class="custom-file-upload" id="carga_archivo">
              <i class="fa fa-cloud-upload"></i>&nbsp; &nbsp;Cargar Archivo
            </label>
            <button type="submit" class="btn btn-success" id="btn-submit" style="font-size: 20px;" onclick="start_loader()" disabled><i class="fa fa-cloud-download" aria-hidden="true">
              </i><b>&nbsp; &nbsp;Importar Datos</b></button>
          </div>
        </div>
        <div class="row" id="div_file_name" style="display: none; width: 500px;">
          <input type="text" id="name_file" style="width: 390px;" disabled>
        </div>
      </form>
    </div>
    <?php if ($datos->num_rows > 0) : ?>
      <div class="container-fluid">
        <div class="row" id="tabla" style=" overflow-x: auto;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <table border="1" id="myTable" class="table-bordered table-hover" style="width:100%" >
              <thead>
                <th class="centrar" >NO_CLIENTE</th>
                <th class="centrar" >FECHA</th>
                <th class="centrar" >TRAMITE</th>
                <th class="centrar" >NOMBRE</th>
                <th class="centrar" >TIPO</th>
                <th class="centrar" >EDO_ORIGEN</th>
                <th class="centrar" >VALOR_FACTURA</th>
                <th class="centrar" >COTIZACIONES</th>
                <th class="centrar" >DOCUMENTOS</th>
                <th class="centrar" >INCOMPLETOS</th>
                <th class="centrar" >GUARDAR</th>
                <th class="centrar" >RECHAZAR</th>
              </thead>
              <tbody>
                <?php while ($d = $datos->fetch_object()) : ?>
                  <tr align="center">

                    <td> <?php
                      echo
                      '<a id="'. $d->ID.'_'.encrypt_decrypt('decrypt',$d->NOMBRE).'" class="llamar-popup-cliente" data-toggle="modal" href="#Modal-Clientes"><i class="fa fa-plus-circle icon" aria-hidden="true"></i></a>';
                      ?><?php echo "&nbsp" . $d->NO_CLIENTE; ?></td>

                    <td>
                      <?php echo $d->FECHA; ?>
                    </td>

                    <td class="tramite">ALTA</td>

                    <td><?php echo encrypt_decrypt('decrypt',$d->NOMBRE) ?></td>

                    <td id="unidad_<?php echo $d->NO_CLIENTE; ?>"><?php echo $d->TIPO; ?></td>

                    <td><?php echo $d->EDO_ORIGEN; ?></td>

                    <td><?php echo '$'.number_format($d->VALOR_FACTURA,2) ?></td>

                    <td>
                      <?php echo '
                      <button class="llamar-popup '.encrypt_decrypt('decrypt',$d->NOMBRE).'" id="'.$d->ID.'" data-toggle="modal" data-target="#Modal-Cotizacion" style="width: 80px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                      </button>
                      <input type="text" value="'.$d->NO_CLIENTE.'" id="NOCLIENTE_'.$d->ID.'" hidden>
                      '; ?>
                    </td>
               
                    <td>
                      <?php echo '
                      <button id="documentos'.$d->ID.'" data-toggle="modal" data-target="#Modal-Documentos'.$d->ID.'" style="width: 80px;" ><i class="fa fa-file-text" aria-hidden="true"></i>
                      </button> 
                      '; ?>
                      <div class="modal fade" id="Modal-Documentos<?php echo $d->ID; ?>" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="vertical-alignment-helper">
                          <div class="modal-dialog vertical-align-center">
                            <div class="modal-content" style=" min-width:600px !important;">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                  <span aria-hidden="true">&times;</span>
                                  <span class="sr-only">Cerrar</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel" align="center"><b id="texto-modal">CARGA DE DOCUMENTOS</b></h4>

                              </div>
                              <div class="container text-left">
                                <label class="modal-title"></label>
                              </div>
                              <div class="modal-body">
                                <table id="modal-table" border="1" class="table-bordered table-hover table-layout:fixed" style="width:100%">
                                  <thead>
                                    <th class="cabecera_popup" width="100px">DOCUMENTO</th>
                                    <th class="cabecera_popup" width="50px">TIPO</th>
                                    <th class="cabecera_popup" width="50px">CARGA</th>                   
                                  </thead>
                                  <tbody>
                                    <tr class="centrar">
                                      <td>FACTURA</td>
                                      <td>XML</td>
                                      <td>
                                        <label class="custom-file-upload ">
                                          <input id="<?php echo 'Factura1_'.$d->NO_CLIENTE; ?>" type="file" class="factura1" accept="text/plain,.xml" >
                                          <i class="fa fa-cloud-upload"></i>
                                        </label>
                                      </td>
                                    </tr>
                                    <tr class="centrar">
                                      <td>FACTURA</td>
                                      <td>PDF</td>
                                      <td>
                                        <label class="custom-file-upload ">
                                          <input id="<?php echo 'Factura2_'.$d->NO_CLIENTE; ?>" type="file" class="factura2" accept="application/pdf">
                                          <i class="fa fa-cloud-upload"></i>
                                        </label>
                                      </td>
                                    </tr>
                                    <tr class="centrar">
                                      <td>PÓLIZA DE SEGURO</td>
                                      <td>PDF</td>
                                      <td>
                                        <label class="custom-file-upload ">
                                          <input id="<?php echo 'Poliza_'.$d->NO_CLIENTE; ?>" type="file" class="poliza" accept="application/pdf">
                                          <i class="fa fa-cloud-upload"></i>
                                        </label>
                                      </td>
                                    </tr>
                                    <tr class="centrar" <?php if($d->ID_TRAMITE == 1){ echo 'hidden'; } ?>>
                                      <td>OTROS</td>
                                      <td>ZIP</td>
                                      <td>
                                        <label class="custom-file-upload ">
                                          <input id="<?php echo 'Otros_'.$d->NO_CLIENTE; ?>" class="otros" type="file" accept=".zip,.rar,.7zip">
                                          <i class="fa fa-cloud-upload"></i>
                                        </label>
                                      </td>
                                    </tr>
                                  </tbody>

                                </table>

                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td>
                      <input type="checkbox" <?php echo "id=check_".$d->NO_CLIENTE; ?> >
                    </td>
                    <td>
                      <?php 
                        $continua = ($d->CONTINUA == 1) ? '' : 'disabled';
                        echo '
                        <form id="form_proceso' . $d->NO_CLIENTE . '" action="en_proceso.php" method="POST" style="display:block; margin:auto;">
                            <input type="text" name="numero_cliente" value="' . $d->NO_CLIENTE . '" hidden>
                            <input type="text" id="ID_' . $d->NO_CLIENTE . '" name="ID" value="' . $d->ID . '" hidden >
                            <input type="text" id="name_'.$d->NO_CLIENTE.'" name="nombre_cliente" value="' . encrypt_decrypt('decrypt',$d->NOMBRE) . '" hidden>
                            <input type="text" name="agencia" value="' . $d->EDO_ORIGEN . '" hidden>
                            <input type="text" name="unidad" value="' . $d->MARCA . '_' . $d->VERSION . '" hidden>
                        </form>
                        <button class="btn btn-primary"  id="Guardar_'.$d->ID.'"  
                        onclick="enProceso(' . $d->NO_CLIENTE . ');"  '.$continua .' >
                          <i class="fa fa-floppy-o" aria-hidden="true"></i>
                        </button>
                        '; 
                      ?>
                    </td>

                    <td>
                      <?php
                        echo '
                        <form id="formRechazar_' . $d->NO_CLIENTE . '" action="rechazados2.php" method="post" style="display:block; margin:auto;">
                          <input type="text" name="numero_cliente" value="' . $d->NO_CLIENTE . '" hidden >
                          <input type="text" name="ID" value="' . $d->ID . '" hidden >
                          <input type="text" name="vista" value="cotizacion" hidden >
                        </form>
                        <button class="btn btn-danger" onclick="return messageRechazar(' . $d->NO_CLIENTE . ');" >
                          <i class="fa fa-times" aria-hidden="true"></i></button>
                        '; 
                      ?>
                    </td>
                  </tr>

                  <div class="modal fade modal-xl" id="Modal-Incompleto_<?php echo $d->NO_CLIENTE; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="vertical-alignment-helper">
                      <div class="modal-dialog vertical-align-center">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel"><b>MOTIVO INCOMPLETO</b></h4>
                          </div>
                          <div class="container text-left" >
                              <label class="modal-title"></label>
                          </div>
                          <div class="modal-body">
                              <?php 
                                  $query = "SELECT ID, MOTIVO FROM DB_CATALOGOS.TBL_MOTIVOS_INCOMPLETO
                                  WHERE ACTIVO = 1;";

                                  $result = mysqli_query($con, $query);
                                  $data_files = mysqli_fetch_all($result, MYSQLI_ASSOC);

                              ?>
                            <form>
                              <div class="row">
                                <div class="col-xs-6">
                                  <label for="entregadoA">Entregado a:</label>
                                  <select class="form-control" id="codigoFiles<?php echo $d->NO_CLIENTE; ?>">
                                    <option selected disabled>Seleccione una opcion</option>
                                    <?php 
                                      foreach ($data_files as $df) {

                                          echo "<option value=".$df['ID'].">".$df['MOTIVO']."</option>";
                                        }  
                                    ?>
                                  </select>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-xs-12">
                                  <label for="comentarios">Comentarios:</label>
                                  <input type="text" class="form-control" id="comentarios_popup<?php echo $d->NO_CLIENTE;  ?>" aria-describedby="comentarios" placeholder="Agrega Comentarios">
                                </div>
                              </div>
                              <br>
                              <div class="row">
                                <div class="col-xs-12">
                                   <button type="button" class="btn btn-primary pull-right" onclick="sentIncompletos(<?php echo $d->NO_CLIENTE; ?>);" >Aceptar</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                <?php endwhile; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!--Final de la Tabla-->
      <!--Modal Cotización-->
      <div class="modal fade" id="Modal-Cotizacion" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style=" min-width:1000px !important;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Cerrar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel" align="center"><b id="texto-modal">ACEPTAR COTIZACIÓN</b></h4>

              </div>
              <div class="container text-left">
                <label class="modal-title" id="label_' . $d->NO_CLIENTE . '"></label>
              </div>
              <div class="modal-body">
                <table id="modal-table" border="1" class="table-bordered table-hover" style="width:100%">
                  <thead>
                    <th class="cabecera_popup" >COTIZACI&Oacute;N</th>
                    <th class="cabecera_popup" >ESTADO</th>
                    <th class="cabecera_popup" >TIPO</th>
                    <th class="cabecera_popup" >TRAMITE</th>
                    <th class="cabecera_popup" >SLA</th>
                    <th class="cabecera_popup" >AVG</th>

                    <th class="cabecera_popup" >DERECHOS</th>
                    <th class="cabecera_popup" >TENENCIA</th>
                    <th class="cabecera_popup" >TARJETA</th>
                    <th class="cabecera_popup" >OTRO</th>
                    <th class="cabecera_popup" >TOTAL</th>
                    <th class="cabecera_popup" >ESTATUS</th>
                    <th class="cabecera_popup" >ACTUALIZAR</th>
                    <th class="cabecera_popup" >ACEPTAR</th>
                  </thead>
                  <tbody id="respuesta_cotizacion">
                  </tbody>

                </table>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Final Modal Cotizacion-->

      <!--Modal Clientes-->
      <div class="modal fade" id="Modal-Clientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Cerrar</span>
                </button>
                <h3 class="modal-title" id="myModalLabel"><b id="texto-cliente"></b></h3>
              </div>
              <div class="modal-body" style="text-align:left;">
                <ul id="lista-modal">
                </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Final Modal Clientes-->
    <?php else : ?>
      <h3 style="padding-left:15px;">NO SE HAN CARGADO DATOS</h3>
    <?php endif; ?>

  </body>

  </html>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>



  <script type="text/javascript">

    $(".factura1").change(function() {

      let identificador = $(this).attr('id');

      console.log(identificador);


      let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
      let no_cliente = data[1];
      let archivos = new FormData();

      let factura = $('#Factura1_' + no_cliente)[0].files[0];
      //.prop('files');
      //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

      let tipo_unidad = $("#unidad_" + no_cliente).text();
      let id = $('#ID_' + no_cliente).val();

      archivos.append('file', factura);
      archivos.append('no_cliente', no_cliente);
      archivos.append('tipo_unidad', tipo_unidad);
      archivos.append('id', id);
      archivos.append('archivo', 'Factura');
     
      swal({
          title: factura.name,
          text: "¿Desea cargar el archivo?",
          icon: "info",
          buttons: ["Cancelar", true],
          dangerMode: true,
        })
        .then((value) => {
          if (value) {
            $.ajax({
              url: 'action/uploads_factura.php',
              type: 'post',
              data: archivos,
              contentType: false,
              processData: false,
              success: function(response) {
                if (response > 0) {
                  swal('Archivo cargado exitosamente', '', 'success');
                  $('#Factura1_' + no_cliente).val('');
                } else {
                  swal({
                    title: 'Error al cargar el archivo',
                    text: 'intentalo más tarde',
                    icon: 'error',
                    dangerMode: true,
                  });
                }
              },
            });
          } else {
            $('#Factura1_' + no_cliente).val('');
          }
        });
        

    });


    $(".poliza").change(function() {

      let identificador = $(this).attr('id');

      console.log(identificador);


      let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
      let no_cliente = data[1];
      let archivos = new FormData();

      let poliza = $('#Poliza_' + no_cliente)[0].files[0];
      //.prop('files');
      //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

      let tipo_unidad = $("#unidad_" + no_cliente).text();
      let id = $('#ID_' + no_cliente).val();

      archivos.append('file', poliza);
      archivos.append('no_cliente', no_cliente);
      archivos.append('tipo_unidad', tipo_unidad);
      archivos.append('id', id);
      archivos.append('archivo', 'poliza');
     
      swal({
          title: poliza.name,
          text: "¿Desea cargar el archivo?",
          icon: "info",
          buttons: ["Cancelar", true],
          dangerMode: true,
        })
        .then((value) => {
          if (value) {
            $.ajax({
              url: 'action/uploads_poliza.php',
              type: 'post',
              data: archivos,
              contentType: false,
              processData: false,
              success: function(response) {
                if (response > 0) {
                  swal('Archivo cargado exitosamente', '', 'success');
                  $('#Poliza_' + no_cliente).val('');
                } else {
                  swal({
                    title: 'Error al cargar el archivo',
                    text: 'intentalo más tarde',
                    icon: 'error',
                    dangerMode: true,
                  });
                }
              },
            });
          } else {
            $('#Poliza_' + no_cliente).val('');
          }
        });
        
    });


    $(".factura2").change(function() {

      let identificador = $(this).attr('id');

      console.log(identificador);


      let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
      let no_cliente = data[1];
      let archivos = new FormData();

      let factura = $('#Factura2_' + no_cliente)[0].files[0];
      //.prop('files');
      //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

      let tipo_unidad = $("#unidad_" + no_cliente).text();
      let id = $('#ID_' + no_cliente).val();

        console.log(no_cliente);
      console.log(tipo_unidad);
      console.log(id);
      
      archivos.append('file', factura);
      archivos.append('no_cliente', no_cliente);
      archivos.append('tipo_unidad', tipo_unidad);
      archivos.append('id', id);
      archivos.append('archivo', 'Factura');
    

      swal({
          title: factura.name,
          text: "¿Desea cargar el archivo?",
          icon: "info",
          buttons: ["Cancelar", true],
          dangerMode: true,
        })
        .then((value) => {
          if (value) {
            $.ajax({
              url: 'action/uploads_factura2.php',
              type: 'post',
              data: archivos,
              contentType: false,
              processData: false,
              success: function(response) {
                if (response > 0) {
                  swal('Archivo cargado exitosamente', '', 'success');
                  $('#Factura2_' + no_cliente).val('');
                } else {
                  swal({
                    title: 'Error al cargar el archivo',
                    text: 'intentalo más tarde',
                    icon: 'error',
                    dangerMode: true,
                  });
                }
              },
            });
          } else {
            $('#Factura2_' + no_cliente).val('');
          }
        });  

    });

    $(".otros").change(function() {

      let identificador = $(this).attr('id');

      console.log(identificador);


      let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
      let no_cliente = data[1];
      let archivos = new FormData();

      let factura = $('#Otros_' + no_cliente)[0].files[0];
      //.prop('files');
      //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

      let tipo_unidad = $("#unidad_" + no_cliente).text();
      let id = $('#ID_' + no_cliente).val();

      console.log(no_cliente);
      console.log(tipo_unidad);
      console.log(id);
      
      archivos.append('file', factura);
      archivos.append('no_cliente', no_cliente);
      archivos.append('tipo_unidad', tipo_unidad);
      archivos.append('id', id);
      archivos.append('archivo', 'Factura');
    

      swal({
          title: factura.name,
          text: "¿Desea cargar el archivo?",
          icon: "info",
          buttons: ["Cancelar", true],
          dangerMode: true,
        })
        .then((value) => {
          if (value) {
            $.ajax({
              url: 'action/uploads_otros.php',
              type: 'post',
              data: archivos,
              contentType: false,
              processData: false,
              success: function(response) {
                if (response > 0) {
                  swal('Archivo cargado exitosamente', '', 'success');
                  $('#Otros_' + no_cliente).val('');
                } else {
                  swal({
                    title: 'Error al cargar el archivo',
                    text: 'intentalo más tarde',
                    icon: 'error',
                    dangerMode: true,
                  });
                }
              },
            });
          } else {
            $('#Otros_' + no_cliente).val('');
          }
        });  

    });

    function messageRechazar(no_cliente) {

      swal({
          title: '',
          text: "¿Desea rechazar este registro?",
          icon: "info",
          buttons: ["Cancelar", true],
          dangerMode: true,
        })
        .then((value) => {
          if (value) {
            document.getElementById("formRechazar_" + no_cliente).submit();
          }
        });

    }

    function enProceso(no_cliente) {

      //console.log("here!");

      if ($('#check_'+no_cliente).is(":checked")){
        $('#Modal-Incompleto_'+no_cliente).modal('show');
      }else{
          document.getElementById("form_proceso"+no_cliente).submit();
      }

    }


    function sentIncompletos(no_cliente){

      let id = $('#ID_' + no_cliente).val();
      let name = $('#name_' + no_cliente).val();
      let comentarios = $('#comentarios_popup' + no_cliente).val();  
      let codigo = $('#codigoFiles' + no_cliente).val();  


      if(comentarios !== ''){
         $.ajax({
          url: 'action/incompletos.php',
          type: 'post',
          data: {
            'ID' : id ,
            'ESTATUS' : 13,
            'no_cliente' : no_cliente,
            'nombre' : name,
            'COMENTARIOS' : comentarios,
            'CODIGO' : codigo
          },
          success: function(response) {
            
            console.log(response);
            location.reload(); 
          },
        });
      }else{
        swal({
          title: "Campo comentarios vacio",
          text: "Agrege un texto",
          icon: "info",
          dangerMode: true,
        });
      }

     

    }

    function mi_funcion(no_cliente) {

      if ($('#Comprobante_' + no_cliente).val() != "") {

        var archivos = new FormData();
        var comprobante = $('#Comprobante_' + no_cliente)[0].files[0];
        var id = $('#ID_' + no_cliente).val();

        archivos.append('file', comprobante);
        archivos.append('no_cliente', no_cliente);
        archivos.append('id', id);

        document.getElementById("loader").style.display = "block";

        $.ajax({
          url: 'action/uploads_files.php',
          type: 'post',
          data: archivos,
          contentType: false,
          processData: false,
          success: function(response) {
            if (response > 0) {
              document.getElementById("form_" + no_cliente).submit();
            } else {
              alert('file not uploaded');
            }
          },
        });

      } else {
        swal({
          title: "No se ha cargado Comprobante",
          text: "Agrege el documento",
          icon: "info",
          dangerMode: true,
        });
      }

    }


    function start_loader() {

      document.getElementById("loader").style.display = "block";

    }

    $("#carga_archivo").change(function() {

      var path = $(this).val();
      var name = path.split('\\');
      var ext = name[2].split(".");

      console.log("carga archivo");

      if (ext[1] == 'xlsx') {
        console.log(ext[1]);

        $("#name_file").val("Archivo: " + name[2]);

        console.log(name);
        $("#btn-submit").attr("disabled", false);
        document.getElementById("div_file_name").style.display = "block";

      } else {
        console.log(ext[1]);
        $("#btn-submit").attr("disabled", true);
        swal('Formato no permitido', 'Cargue un documento .xlsx', 'error');

      }

    });

    $('.test-popup-link').magnificPopup({
      type: 'image'
    });


    $(document).ready(function() {

      var table = $("#myTable").DataTable({
      
      });

      table.columns.adjust().draw();
    
    });

   
    function formatoMexico(number){

      const exp = /(\d)(?=(\d{3})+(?!\d))/g;
      const rep = '$1,';
      let arr = number.toString().split('.');
      arr[0] = arr[0].replace(exp,rep);
      
      return arr[1] ? arr.join('.'): arr[0];

    }

    function aceptarcotizacion(num_cotizacion, id){

      let folio = $('#folio'+num_cotizacion).text();
      let edo_origen = $('#edo_origen'+num_cotizacion).text();

      $.ajax({
        url: 'action/set_autorizacion.php',
        type: 'post',
        data: {
          'folio': folio,
          'edo_origen' : edo_origen,
          'id' : id,
        },
        success: function(response){
          console.log(response);
          
          $( "#btn-aceptar1" ).prop( "disabled", true );
          $( "#btn-aceptar2" ).prop( "disabled", true );
          $( "#btn-aceptar3" ).prop( "disabled", true );

          $( "#documentos"+id ).prop( "disabled", false );

          switch(num_cotizacion){

            case 1:
              $( "#estatus1").text('ACEPTADA');
              $( "#estatus2").text('RECHAZADA');
              $( "#estatus3").text('RECHAZADA');
            break;

            case 2:
              $( "#estatus1").text('RECHAZADA');
              $( "#estatus2").text('ACEPTADA');
              $( "#estatus3").text('RECHAZADA');
            break;

            case 3:
              $( "#estatus1").text('RECHAZADA');
              $( "#estatus2").text('RECHAZADA');
              $( "#estatus3").text('ACEPTADA');
            break;

          }

          $('#Guardar_'+id).prop('disabled', false);

        },
        error: function(request, err) {
          console.log(err);
        }
      });

    }

    function sumar(num_cotizacion, id_cotizacion, id){

      let folio = $('#folio'+num_cotizacion).text();
      let derechos = parseInt($('#derechos_'+num_cotizacion).val());
      let tenencia = parseInt($('#tenencia_'+num_cotizacion).val());
      let tarjeta = parseInt($('#tarjeta_'+num_cotizacion).val());
      let otro = parseInt($('#otro_'+num_cotizacion).val());

      let no_cliente = $('#NOCLIENTE_'+id_cotizacion).val();

      let nombre = $('#name_'+no_cliente).val();

      //console.log(nombre);
      
      $('#total_'+num_cotizacion).text(formatoMexico(derechos+tenencia+tarjeta+otro));

      
      $.ajax({
        url: 'action/update_cotizaciones.php',
        type: 'post',
        data: {
          'folio': folio,
          'derechos' : derechos,
          'tenencia' : tenencia,
          'tarjeta' : tarjeta,
          'otros' : otro,
          'no_cliente' : no_cliente, 
          'nombre' : nombre
        },
        success: function(response){
          swal('Monto Actualizado', '', 'success');
        },
        error: function(request, err) {
          console.log(err,request);
        }
      });
      
    
    }

    //Funcion popup
    $(document).on('click', '.llamar-popup', function() {

      let no_cliente = $(this).attr('id');
      let nombre_cliente = $(this).attr('class');
      let spltittedName = nombre_cliente.substring(13);
      let texto_modal = document.querySelector('#texto-modal');
      let respuesta_cotizacion = document.querySelector('#respuesta_cotizacion');

      console.log(spltittedName);

      respuesta_cotizacion.innerHTML = '';
      texto_modal.innerHTML = '';
      
      $.ajax({
        url: 'action/listar_cotizaciones.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
          'id': no_cliente
        },
        success: function(respuesta) {
            

            let numberFormat = new Intl.NumberFormat();
            let valdisabled = '';

            for (let i = 0; i < respuesta.length; i++) {


              disabled = (respuesta[i]['ESTATUS'] !== 'PENDIENTE' ) ? 'disabled' : '' ;

              respuesta_cotizacion.innerHTML += `
                <tr align='center'>
                  <td id='folio${i+1}' >${respuesta[i]['FOLIO']}</td>
                  <td id='edo_origen${i+1}'>${respuesta[i]['EDO_COTIZACION']}</td>
                  <td>${respuesta[i]['AUTOMATICA']}</td>
                  <td>${respuesta[i]['TRAMITE']}</td>
                  <td>${respuesta[i]['SLA']}</td>
                  <td>${respuesta[i]['SLA_AVG']}</td>
                  <td>
                    <input type='text' id='derechos_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_DERECHOS']}' maxlength="6" size="4">
                  </td>
                  <td>
                    <input type='text' id='tenencia_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_TENENCIA']}' maxlength="6" size="4">
                  </td>
                  <td>
                    <input type='text' id='tarjeta_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_TARJETA']}' maxlength="6" size="4">
                  </td>
                  <td>
                    <input type='text' id='otro_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_OTRO']}' maxlength="6" size="4">
                  </td>
                  <td style='width:90px;' id='total_${i+1}' >${formatoMexico(parseInt(respuesta[i]['MTO_DERECHOS'])+parseInt(respuesta[i]['MTO_TENENCIA'])+
                    parseInt(respuesta[i]['MTO_TARJETA'])+parseInt(respuesta[i]['MTO_OTRO']))}
                  </td>
                  <td id='estatus${i+1}' >${respuesta[i]['ESTATUS']}</td>
                  <td>
                    <button class='btn btn-info' onclick='sumar(${i+1},${no_cliente},${respuesta[i]['ID']})' >
                      <i class="fa fa-refresh" aria-hidden="true"></i>
                    </button>
                  </td>
                  <td>
                    <button id='btn-aceptar${i+1}' class='btn btn-success' onclick='aceptarcotizacion(${i+1},${respuesta[i]['ID']})' ${disabled} >
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>`
            }
        },
        error: function(request, err) {
          console.log(err);
        }
      });

    });

    //Popup Cliente
    $(document).on('click', '.llamar-popup-cliente', function() {
      let elements = $(this).attr('id').split('_');
      let no_cliente = elements[0];
      let nombre_cliente = elements[1];
      let lista_modal = document.querySelector('#lista-modal');
      let texto_cliente = document.querySelector('#texto-cliente');
      lista_modal.innerHTML = '';
      texto_cliente.innerHTML = '';

      $.ajax({
        url: 'action/popup_fecha.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
          'no_cliente': no_cliente
        },
        success: function(respuesta) {
          texto_cliente.innerHTML = nombre_cliente;
          lista_modal.innerHTML = `
            <li>AUTO: ${respuesta[0]['MARCA']}</li>
          `;
        }
      })

    });


  </script>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>