<link rel="icon" href="images/JJ.ico">
<?php

    $active1="active";
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 

    $_SESSION['tiempo'] = time();
 


    $sql = "SELECT * FROM users";

    if(!empty($_GET)){

        if($_GET["success"]==true){
            echo "
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
            <script>
                swal('Contraseña Restablecida','cierre sesión para comprobar','success');
            </script>";
        }else{
            echo "
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
            <script>
                swal('Error al actualizar','Intente m&aacute;s tarde','error');
            </script>";
        }

    }else{

    }

    $count_files = mysqli_query($con, "select * from file");
    $count_staging = mysqli_query($con, "SELECT COUNT(*) COTIZACIONES FROM db_sistema.tbl_contratos C");
    $count_rechazados=mysqli_query($con, "SELECT COUNT(*) RECHAZADOS FROM db_sistema.tbl_contratos C
    WHERE C.ESTATUS IN(12,22)");
    $count_pendientes=mysqli_query($con, "SELECT COUNT(*) PENDIENTES FROM db_sistema.tbl_contratos C
    WHERE C.ESTATUS IN(10)");
    $count_revision=mysqli_query($con, "SELECT COUNT(*) REVISION FROM db_sistema.tbl_contratos C
    WHERE C.ESTATUS IN(20,23)");
    $count_tramite=mysqli_query($con, "SELECT COUNT(*) TRAMITE FROM db_sistema.tbl_contratos C
    WHERE C.ESTATUS IN(30,33)");
    $count_finalizados=mysqli_query($con, "SELECT COUNT(*) FINALIZADOS FROM db_sistema.tbl_contratos C
    WHERE LEFT(C.ESTATUS,1)=4");
    $val_total = mysqli_query($con, "SELECT SUM(SALDO) SALDO_ACTUAL FROM ( SELECT a.MONTO SALDO FROM tbl_abonos a 
    UNION ALL SELECT b.MONTO SALDO FROM tbl_cargos b ) TMP");

    $count_download = mysqli_query($con, "select sum(download) as download from file");
    $count_user=mysqli_query($con, "select * from user");
    $count_comments=mysqli_query($con, "select * from comment");
    $user_id=$_SESSION['user_id'];
    $data=mysqli_query($con,"select * from tbl_usuarios  where ID=$user_id");
    

    $cantidad=mysqli_query($con,"select sum(monto) from tbl_movimientos ");
    //$valor=mysqli_fetch_assoc($cantidad);

    $result_rechazados = $count_rechazados->fetch_assoc();
    $rechazados = $result_rechazados["RECHAZADOS"];

    $result_cotizaciones = $count_staging->fetch_assoc();
    $cotizaciones = $result_cotizaciones["COTIZACIONES"];

    $result_pendientes = $count_pendientes->fetch_assoc();
    $pendientes = $result_pendientes["PENDIENTES"];

    $result_revision = $count_revision->fetch_assoc();
    $revision = $result_revision["REVISION"];

    $result_tramite = $count_tramite->fetch_assoc();
    $tramite = $result_tramite["TRAMITE"];

    $result_finalizados = $count_finalizados->fetch_assoc();
    $finalizados = $result_finalizados["FINALIZADOS"];

    $result_total = $val_total->fetch_assoc();
    $total = $result_total["SALDO_ACTUAL"];

    
    $activos= $con->query("SELECT COUNT(*) FROM tbl_staging;");    
    
?>
    <div class="content-wrapper"  >
        <!-- Content Wrapper. Contains page content -->
        <section class="content-header" <?php if($comercial==1){ echo "hidden";} ?>><!-- Content Header (Page header) -->
          

          <h1>Dashboard<small>Panel de control</small> </h1>
          <h2><?php echo $_SESSION['dbconection']; ?></h2>
            <ol class="breadcrumb">
                <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <section class="content" <?php if( $comercial==1){ echo "hidden";} ?>><!-- Main content -->
        
            <div class="row">
              <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-gray">
                    <div class="inner">
                        <h3><?php echo '$'.number_format($total,2); ?></h3>
                        <p>SALDO DISPONIBLE</p>
                    </div>
                     <a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <div class="col-lg-3 col-md-offset-3 col-xs-6">
                <div class="small-box" style="background-color: #20afee !important;">
                    <div class="inner">
                        <h3><?php echo $tramite; ?></h3>
                        <p>TRAMITE</p>
                    </div>
                     <a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
                <div class="small-box" style="background-color: #4d9b03 !important;">
                    <div class="inner">
                        <h3><?php echo $finalizados; ?></h3>
                        <p>FINALIZADOS</p>

                    </div>
                     <a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>

            
            <div class="row"><!-- Small boxes (Stat box) -->
              <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo $cotizaciones; ?></h3>
                        <p>COTIZACIONES</p>
                    </div>
                     <a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>    

              <div class="col-lg-3 col-xs-6">
                  <div class="small-box bg-blue">
                      <div class="inner">
                          <h3><?php echo $pendientes; ?></h3>
                          <p>PENDIENTES</p>
                      </div>
                       <a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
              </div>

              <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3><?php echo $revision; ?></h3>
                        <p>REVISIÓN</p>
                    </div>
                     <a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>

              <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo $rechazados; ?></h3>
                        <p>RECHAZADOS</p>
                    </div>
                     <a href="#" class="small-box-footer">Más info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>



            </div><!-- /.box -->
                    
                    
                </div>
            </div><!-- /.row -->
        </section>
        <section>
        
        <section>
        
        
        
    </div><!-- /.content -->
 
    
<?php include "footer.php"; ?>
<script>
    $(function(){
    $("input[name='file']").on("change", function(){
        var formData = new FormData($("#formulario")[0]);
        var ruta = "action/uploadprofile.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(datos)
            {
                $("#respuesta").html(datos);
            }
        });
    });
    });
</script>


