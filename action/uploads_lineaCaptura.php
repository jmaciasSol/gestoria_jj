<?php session_start();

$_SESSION['tiempo'] = time();

include_once "../config/config.php";
require '../composer/vendor/autoload.php';
include_once "encript.php";
include "../config/configS3.php";

  $bucket = 'crfact';
  $datos = $con->query("SELECT * FROM tbl_keys");
  $d = $datos->fetch_all(MYSQLI_ASSOC);

  $key = $d[count($d)-1]['KEY_'];
  $secrectkey = $d[count($d)-1]['SECRETKEY'];

  $S3 = connectionS3($key,$secrectkey);

date_default_timezone_set ('America/Mexico_City');

$user_id = $_SESSION['user_id'];
$datos = $con->query("SELECT * FROM tbl_usuarios WHERE ID = $user_id");
$d = $datos->fetch_object();
$correo = $d->EMAIL;

  $id=$_POST['id'];

  $id_str = strval($id);
  $folio = '0';

  while(strlen($folio.$id_str)  < 11 ){
    $folio = $folio.'0';
  }


$filename = $_FILES['file']['name'];
$location = '../storage'.$filename;
$uploadOk = 1;
$imageFileType = pathinfo($location,PATHINFO_EXTENSION);


/* Valid Extensions */
$valid_extensions = array("jpg","jpeg","png","xls","pdf","xml");
/* Check file extension */
if( !in_array(strtolower($imageFileType),$valid_extensions) ) {
   $uploadOk = 0;
}

if($uploadOk == 0){
   echo 0;
}else{
   /* Upload file */
    $extension=pathinfo($filename,PATHINFO_EXTENSION);
    
    $name_file= $folio.$id_str.'_'."LineaCaptura_".strtoupper($extension).'_'.date("YmdHis");

   if(move_uploaded_file($_FILES['file']['tmp_name'],'../storage'."/".$name_file.".".$extension)){

     $complete_name_file = $name_file.".".$extension;

     $key = $name_file.".".$extension;
     $source = '../storage'."/".$name_file.".".$extension;

      upload($bucket,$key,$source,$S3);


      $sql = "INSERT INTO tbl_bitacora_d(FOLIO, PATH, NOMBRE, USUARIO, TIPO) VALUES($id,'$complete_name_file','$complete_name_file','$correo', 0)";
      $con->query($sql);

      $dir = '../storage/';
      foreach(glob($dir.'*.*') as $v){
          unlink($v);
      }

      echo 1;
   }else{
      echo 0;
   }
}

?>