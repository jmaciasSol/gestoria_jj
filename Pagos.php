<?php
$active = "active";
include "head.php";
include "header.php";
include "aside.php";
include "action/encript.php";
include_once "config/config.php";


$_SESSION['tiempo'] = time();

?>

<div class="content-wrapper">
  <section class="content-header" hidden>
    <h1></h1>
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Importar Activaciones</li>
    </ol>
  </section>

  <!DOCTYPE html>
  <html>

  <body>
    <div class="container-fluid row">
      <div class="col-lg-4 col-xs-6">
        <h1 style="padding-top: 25px;">&nbsp;&nbsp;PAGOS</h1>
      </div>
    </div>
    <div id="loader" style="display: none"></div>
    <form id="formZip" method="POST" action="downloadZip.php" hidden>
      <input type="text" name="download" value="1">
    </form>

    
  <form id="formDescargarDocumento" method="post" action="download.php" hidden>
    <input id="keyvalue" type="text" name="key">
  </form>


    <div class="row">
      <div class="col-lg-11">
        <button class="btn btn-success pull-right" id="btn-pay" ><i class="fa fa-money " aria-hidden="true" onclick="start_loader()" >&nbsp;&nbsp;COBRAR</i></button>
      </div>
    </div>
    <div id="loader" style="display: none"></div>
     <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
                <form>
                    <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="vin">VIN</label>
                                <input type="text" class="form-control" id="vin" name="vin" minlength="17"maxlength="17" pattern="[A-Za-z0-9]{17}" title="El VIN es una combinación de 17 letras y números">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="numero_cliente">Número Cliente</label>
                                <input type="text" class="form-control" id="numero_cliente" name="numero_cliente" maxlength="8" pattern="[0-9]{8}" title="El Número de cliente debe contener 8 dígitos, no se permiten letras ni caracteres especiales.">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="numero_placas">Número de placas</label>
                                <input type="text" class="form-control" id="numero_placas" name="numero_placas" minlength="8" maxlength="8" pattern="[0-9]{8}" title="El Folio debe contener 8 dígitos, no se permiten letras ni caracteres especiales." >
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" pattern="[A-Za-z]{8}" title="El Nombre de cliente debe contener sólo letras, no se permiten digitos ni caracteres especiales.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="fecha_1">Fecha 1</label>
                                <input type="date" class="form-control" id="fecha_1" name="fecha_1">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="fecha_2">Fecha 2</label>
                                <input type="date" class="form-control" id="fecha_2" name="fecha_2">
                            </div>
                        </div>

                        <?php 

                          $sqlEstados = "SELECT EDO FROM v_info"; 

                          $resultEstados = mysqli_query($con, $sqlEstados);
                          $datosEstados = mysqli_fetch_all($resultEstados, MYSQLI_ASSOC);


                          //var_dump($datosEstados);

                        ?>

                        
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="form-group">
                                <label for="estado">Edo Emplacamiento</label>
                                <select name="proceso" id="estado" class="form-control">
                                  <option value="" selected>Seleccionar </option>
                                  <?php foreach ($datosEstados as $edo): ?>
                                      <option value="<?php echo $edo['EDO']; ?>"><?php echo $edo['EDO']; ?></option>
                                  <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <label for="cobrado">Estatus</label>
                                <select name="estatus" id="cobrado" class="form-control">
                                    <option value="" selected >Seleccionar </option>
                                    <option value="0">No Cobrado</option>
                                    <option value="1">Cobrado</option>
                                    
                                </select>
                            </div>
                        </div>
                      
                    </div>
                    <div class="row">
                        <div class="container">
                            <div class="col pull-left">
                                <button type="button" name="buscar" id="buscar" class="btn btn-info" style="font-size: 20px;">
                                    <i class="fa fa-search"></i>
                                    <b>&nbsp; &nbsp; Buscar</b>
                                </button>
                                <button type="reset" id="borrar" name="borrar" class="btn btn-danger" style="font-size: 20px;">
                                    <i class="glyphicon glyphicon-erase" aria-hidden="true"></i>
                                    <b>&nbsp; &nbsp;Borrar</b>
                                </button>
                            </div>
                        </div>
                    </div>
                    </div>
                </form>
            </div>


      <div class="container-fluid">
        <div class="row" id="tabla" style=" overflow-x: auto;">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <table border="1" id="myTable" class="table-bordered table-hover" style="width:100%" >
              <thead>
                <th class="centrar" >VIN</th>
                <th class="centrar" >PLACA</th>
                <th class="centrar" >ESTATUS</th>
                <th class="centrar" >FECHA</th>
                <th class="centrar" >DOCUMENTOS</th>
                <th class="centrar" >EVENTOS</th>
                <th class="centrar" >DERECHOS</th>
                <th class="centrar" >HONORARIOS</th>
                <th class="centrar" >MENSAJERIA</th>
                <th class="centrar" >COBRAR</th>
              </thead>
              <tbody id="respuesta">
           
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!--Final de la Tabla-->
  </body>

    <!--Modal Documentos-->
    <div class="modal fade modal-xl" id="Modal-Documentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center">
              <div class="modal-content" style=" min-width:700px !important; ">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">
                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                      </button>
                      <h4 class="modal-title" id="myModalLabel"><b>DOCUMENTOS</b></h4>
                  </div>
                  <div class="container text-left" >
                      <label class="modal-title" id="label_bitacora"></label>
                  </div>
                  <div class="modal-body" style="overflow-y: auto;">
                      <div class="outer" style=" min-height: 200px !important;">
                      <table border="1" id="bitacora">
                          <thead>
                              <tr>
                              <th class="cabecera_popup" style="width: 100px;" >FECHA</th>
                              <th class="cabecera_popup">DOCUMENTO</th>
                              <th class="cabecera_popup" style="width: 100px;">Descargar</th>

                              </tr>
                          </thead>
                          <tbody id="documentos">
                          </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
          </div>
      </div>
    </div>

    <div class="modal fade modal-xl" id="Modal-Bitacora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
          <div class="modal-content" style=" min-width:1000px !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><b>BITACORA</b></h4>
            </div>
            <div class="container text-left" >
                <label class="modal-title" id="label_bitacora"></label>
            </div>
            <div class="modal-body"  style="overflow-y: auto;">
              <div class="outer" style=" min-height: 200px !important;">                            
                <table border="1" style="border-collapse: collapse; width: 100%;">
                    <thead>
                        <tr>
                        <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">NO. CLIENTE</th>
                        <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">FECHA</th>
                        <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">ROL</th>
                        <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">USUARIO</th>
                        <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">PROCESO</th>
                        <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">ESTATUS</th>
                        <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">MOTIVO INCOMPLETO</th>
                        <th style="text-align:center; background-color: #00c0ef; position: sticky; top: 0;">COMENTARIO</th>
                        </tr>
                    </thead>
                    <tbody id="respuesta_bitacora">
                    </tbody>
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade modal-xl" id="Modal-Derechos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center">
              <div class="modal-content" style=" min-width:700px !important;">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">
                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                      </button>
                      <h4 class="modal-title" id="myModalLabel"><b>DERECHOS</b></h4>
                  </div>
                 
                  <div class="modal-body">

                      <table border="1">
                          <thead>
                            <tr>
                              <th class="cabecera_popup" style="width: 100px;">FOLIO</th>
                              <th class="cabecera_popup" style="width: 100px;">DERECHOS</th>
                              <th class="cabecera_popup" style="width: 100px;">TENENCIA</th>
                              <th class="cabecera_popup" style="width: 100px;">TARJETA</th>
                              <th class="cabecera_popup" style="width: 100px;">OTRO</th>
                              <th class="cabecera_popup" style="width: 100px;">TOTAL</th>
                              <th class="cabecera_popup" style="width: 100px;">PAGAR</th>
                            </tr>
                          </thead>
                          <tbody id="derechos">
                          </tbody>
                      </table>
                      
                    
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
          </div>
      </div>
    </div>

    <div class="modal fade modal-xl" id="Modal-Honorarios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center" style=" min-width:400px !important;">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">
                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                      </button>
                      <h4 class="modal-title" id="myModalLabel"><b>HONORARIOS</b></h4>
                  </div>
                  <div class="modal-body">
                    
                      <table border="1" width="100%">
                          <thead>
                            <tr>
                              <th class="cabecera_popup" >MONTO</th>
                              <th class="cabecera_popup" >MONTO_OTRO</th>
                              <th class="cabecera_popup" >NOMBRE</th>
                             
                              <th class="cabecera_popup" style="width: 100px;">PAGAR</th>
                            </tr>
                          </thead>
                          <tbody id="honorarios">
                          </tbody>
                      </table>
                    
                    
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
          </div>
      </div>
    </div>

    <div class="modal fade modal-xl" id="Modal-Mensajeria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center" style=" min-width:400px !important;">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">
                          <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                      </button>
                      <h4 class="modal-title" id="myModalLabel"><b>MENSAJERIA</b></h4>
                  </div>
                  <div class="modal-body">
                    
                      <table border="1" width="100%">
                          <thead>
                            <tr>
                              <th class="cabecera_popup" >MONTO</th>
                              <th class="cabecera_popup" >MONTO_OTRO</th>
                              <th class="cabecera_popup" >NOMBRE</th>
                             
                              <th class="cabecera_popup" style="width: 100px;">PAGAR</th>
                            </tr>
                          </thead>
                          <tbody id="mensajeria">
                          </tbody>
                      </table>
                    
                    
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  </div>
              </div>
          </div>
      </div>
    </div>

  </html>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>

  <script type="text/javascript">

    let idsArray = [];

    $(document).ready(function() {

      var table = $("#myTable").DataTable({
          columnDefs: [{
            targets: "_all",
            orderable: false
          }]
      });

      table.columns.adjust().draw();
    
    });

    $('#buscar').click(function() {

        let vin = document.getElementById('vin').value;
        let numero_cliente = document.getElementById('numero_cliente').value;
        let numero_placas = document.getElementById('numero_placas').value;
        let nombre = document.getElementById('nombre').value;
        let fecha_1 = document.getElementById('fecha_1').value;
        let fecha_2 = document.getElementById('fecha_2').value;
        let estado = document.getElementById('estado').value;
        let cobrado = document.getElementById('cobrado').value;

        console.log(vin);
        console.log(numero_cliente);
        console.log(numero_placas);
        console.log(nombre);
        console.log(fecha_1);
        console.log(fecha_2);
        console.log(estado);

        let respuesta = document.querySelector('#respuesta');
        respuesta.innerHTML = '';

        //Validación formulario vacío
        if (vin == "" && numero_cliente == "" && numero_placas == "" && nombre == "" && fecha_1 == "" && fecha_2 == ""  && estado == "" && cobrado == "") {
            swal({
                title: "Atención!",
                text: "El formulario está vacío, por favor intente de nuevo.",
                icon: "warning",
                button: "Reintentar",
            });
        }else if(estado !== "" && fecha_1 === "" && fecha_2 === ""){
          swal({
                title: "Atención!",
                text: "Busqueda por Estado de Emplacamiento necesita un rango de fechas",
                icon: "warning",
                button: "Reintentar",
            });
        }
        else{
            $.ajax({
                url: 'payments.php',
                type: 'POST',
                async: true,
                data: {
                    'vin': vin,
                    'numero_cliente': numero_cliente,
                    'numero_placas': numero_placas,
                    'nombre': nombre,
                    'fecha_1': fecha_1,
                    'fecha_2': fecha_2,
                    'estado' : estado,
                    'cobrado' : cobrado
                },
                success: function(response) {

                    if (response == "") {
                        swal({
                            title: "Aviso!",
                            text: "No encontramos ningún registro con los datos ingresados.",
                            icon: "warning",
                            button: "Reintentar",
                        });
                    } else {
                      
                        
                        let disabled = '';


                        for (let i = 0; i < response.length; i++) {

                          let color_derechos = '';
                          let color_honorarios = '';
                          let color_mensajeria = '';
                          let color_cobrar = '';
                          
                          switch(response [i][6]){

                            case '-1' :
                                      color_derechos = 'yellow' 
                            break;

                            case '0' :
                                      color_derechos = 'red' 
                            break;

                            case '1' :
                                      color_derechos = 'green' 
                            break;
                          
                          }

                          switch(response [i][7]){

                            case '-1' :
                                      color_honorarios = 'yellow' 
                            break;

                            case '0' :
                                      color_honorarios = 'red' 
                            break;

                            case '1' :
                                      color_honorarios = 'green' 
                            break;

                          }

                          switch(response [i][8]){

                            case '-1' :
                                      color_mensajeria = 'yellow' 
                            break;

                            case '0' :
                                      color_mensajeria = 'red' 
                            break;

                            case '1' :
                                      color_mensajeria = 'green' 
                            break;

                          }

                          switch(response [i][9]){

                            case '-1' :
                                      color_cobrar = 'yellow'
                                     
                            break;

                            case '0' :
                                      color_cobrar = 'red' 
                                     
                            break;

                            case '1' :
                                      color_cobrar = 'green'
                                      
                            break;

                          }

                          if (color_cobrar === 'green' || color_cobrar === 'red') {
                            disabled = 'disabled';
                            console.log(disabled);
                          }else{
                            disabled = '';
                            console.log(disabled);
                          }

                          respuesta.innerHTML += `
                            <tr align="center">
                                <td>${response [i][0]}</td>
                                <td>${response [i][1]}</td>
                                <td>${response [i][2]}</td>
                                <td>${response [i][5]}</td>
                                <td>
                                    <a data-toggle="modal" class="popup-documentos" id="${response[i][11]}" href="#Modal-Documentos">Ver</a>
                                </td>
                                <td>
                                    <a data-toggle="modal" class="popup-bitacora" id="${response[i][11]}" href="#Modal-Bitacora">Ver</a>
                                </td>
                                <td>
                                    <a data-toggle="modal" class="popup-derechos" id="${response[i][11]}" href="#Modal-Derechos">Ver</a>
                                    <i id="semaforo_derechos${response[i][11]}" class="fa fa-circle fa-2x" aria-hidden="true" style="vertical-align: middle; margin-left: 15px; color:${color_derechos};  "></i>
                                </td>
                                <td>
                                    <a data-toggle="modal" class="popup-honorarios" id="${response[i][11]}" href="#Modal-Honorarios">Ver</a>
                                    <i id="semaforo_honorarios${response[i][11]}" class="fa fa-circle fa-2x" aria-hidden="true" style="vertical-align: middle; margin-left: 15px; color:${color_honorarios};  "></i>
                                </td>
                                <td>
                                    <a data-toggle="modal" class="popup-mensajeria" id="${response[i][11]}" href="#Modal-Mensajeria">Ver</a>
                                    <i id="semaforo_mensajeria${response[i][11]}" class="fa fa-circle fa-2x" aria-hidden="true" style="vertical-align: middle; margin-left: 15px; color:${color_mensajeria};  "></i>
                                </td>
                                <td>
                                    <input type="checkbox" class="form-group check" value="${response[i][11]}" ${disabled} >
                                    <i id="semaforo_cobrar${response[i][11]}" class="fa fa-circle fa-2x" aria-hidden="true" style="vertical-align: middle; margin-left: 15px; color:${color_cobrar};  "></i>
                                </td>
                            </tr> `;
                        }
                    }

                }
            });
        }
    
    });


    $(document).on('click', '.popup-documentos', function(){

      let identificador= this.id;
      let values = identificador.split("_");
      let id = values[0];
      let no_cliente = values[0];
      let respuesta_documentos = document.querySelector('#documentos');
      respuesta_documentos.innerHTML = '';

      $.ajax({
        url:'action/getDocumentos.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id' : id
        },
        success: function(resp) {
          for(let i= 0; i<resp.length; i++){
            respuesta_documentos.innerHTML += `
            <tr align="center">
              <td>${resp[i]['FECHA']}</td>
              <td>${resp[i]['DOCUMENTO']}</td>
              <td><button class="descargarDocumento btn-default" id="${resp[i]['DOCUMENTO']}">Descargar</button></td>
            </tr>`;
          }
        },
        error: function(request, err){
            console.log(err);
        }
      });

    });




    $(document).on('click', '.popup-bitacora', function(){

      let id_cliente = $(this).attr('id');
      let respuesta_bitacora = document.querySelector('#respuesta_bitacora');
      respuesta_bitacora.innerHTML = '';

      $.ajax({
        url:'popup_bitacora.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id_cliente' : id_cliente
        },
        success: function(respuesta) {
          if (respuesta == "") {
            swal({
                title: "Aviso!",
                text: "No se encontraron datos.",
                icon: "warning",
                button: "Reintentar",
            });
          }else {

            for(let i= 0; i<respuesta.length; i++){
              respuesta_bitacora.innerHTML += `
                <tr align="center">
                    <td>${respuesta[i][0]}</td>
                    <td>${respuesta[i][1]}</td>
                    <td>${respuesta[i][2]}</td>
                    <td>${respuesta[i][3]}</td>
                    <td>${respuesta[i][4]}</td>
                    <td>${respuesta[i][5]}</td>
                    <td>${respuesta[i][8]}</td>
                    <td>${respuesta[i][6]}</td>
                </tr>`;
            }
          }
        },
        error: function(request, err){
            console.log(err);
        }
      });
                 
    });

    $(document).on('click', '.popup-derechos', function(){

      let id_cliente = $(this).attr('id');
      let respuesta_bitacora = document.querySelector('#derechos');
      respuesta_bitacora.innerHTML = '';

      $.ajax({
        url:'popup_derechos.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id_cliente' : id_cliente
        },
        success: function(respuesta) {
          if (respuesta == "") {
            swal({
                title: "Aviso!",
                text: "No se encontraron datos.",
                icon: "warning",
                button: "Reintentar",
            });
          }else {

            console.log(respuesta);
            
            let disabled = "";

            for(let i= 0; i<respuesta.length; i++){

              if(respuesta[0][5] === '0'){
                disabled = '';
              }else{                
                disabled = 'disabled';
              }

              respuesta_bitacora.innerHTML += `
                <tr align="center">
                    <td>${respuesta[i][6]}</td>
                    <td>
                     <input type='text' id='derechos_${respuesta[i][6]}' class='cotizacion' value='${respuesta[i][0]}' maxlength="6" size="4">
                     </td>
                    <td>
                      <input type='text' id='tenencia_${respuesta[i][6]}' class='cotizacion' value='${respuesta[i][1]}' maxlength="6" size="4">
                   </td>
                    <td>
                      <input type='text' id='tarjeta_${respuesta[i][6]}' class='cotizacion' value='${respuesta[i][2]}' maxlength="6" size="4">
                    </td>
                    <td>
                      <input type='text' id='otro_${respuesta[i][6]}' class='cotizacion' value='${respuesta[i][3]}' maxlength="6" size="4">
                      <input type='text' id='derechos_id_${respuesta[i][6]}' class='cotizacion' value='${respuesta[i][7]}' maxlength="6" size="4" hidden>
                    </td>
                    <td id='total_${respuesta[i][6]}'>${respuesta[i][4]}</td>
                    <td>
                      <button class='btn btn-success' onclick='actualizar("${respuesta[i][6]}")' ${disabled} >
                        <i class="fa fa-check" aria-hidden="true"></i>
                      </button>
                    </td>
                </tr>`;
            }
          }
        },
        error: function(request, err){
            console.log(err);
        }
      });
                 
    });

    $(document).on('click', '.descargarDocumento', function(){

      $('#keyvalue').val(this.id);

      document.getElementById("formDescargarDocumento").submit();          

    });


    $(document).on('click', '.popup-honorarios', function(){

      let id_cliente = $(this).attr('id');
      let respuesta_bitacora = document.querySelector('#honorarios');
      respuesta_bitacora.innerHTML = '';

      $.ajax({
        url:'popup_honorarios.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id_cliente' : id_cliente
        },
        success: function(respuesta) {
          if (respuesta == "") {
            swal({
                title: "Aviso!",
                text: "No se encontraron datos.",
                icon: "warning",
                button: "Reintentar",
            });
          }else {

            console.log(respuesta);

            let disabled = "";

            for(let i= 0; i<respuesta.length; i++){

              if(respuesta[0][3] === '0'){
                disabled = '';
                console.log("button disabled");
              }else{                
                disabled = 'disabled';
                console.log("button enable");
              }

              respuesta_bitacora.innerHTML += `
                <tr align="center">
                    <td>
                     <input type='text' id='montoh_${respuesta[i][4]}' class='cotizacion' value='${respuesta[i][0]}' maxlength="6" size="4">
                     </td>
                    <td>
                      <input type='text' id='otroh_${respuesta[i][4]}' class='cotizacion' value='${respuesta[i][1]}' maxlength="6" size="4">
                      <input type='text' id='honorarios_id_${respuesta[i][4]}' class='cotizacion' value='${respuesta[i][5]}' maxlength="6" size="4" hidden>
                    </td>
                    <td>
                      ${respuesta[i][2]}
                    </td>
                   
                    <td>
                      <button class='btn btn-success' onclick='actualizar_honorarios("${respuesta[i][4]}")' ${disabled} >
                        <i class="fa fa-check" aria-hidden="true"></i>
                      </button>
                    </td>
                </tr>`;
            }
          }
        },
        error: function(request, err){
            console.log(err);
        }
      });
                 
    });

    $(document).on('click', '.popup-mensajeria', function(){

      let id_cliente = $(this).attr('id');
      let respuesta_bitacora = document.querySelector('#mensajeria');
      respuesta_bitacora.innerHTML = '';


      $.ajax({
        url:'popup_mensajeria.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id_cliente' : id_cliente
        },
        success: function(respuesta) {
          if (respuesta == "") {
            swal({
                title: "Aviso!",
                text: "No se encontraron datos.",
                icon: "warning",
                button: "Reintentar",
            });
          }else {

            console.log(respuesta);

            let disabled = "";
            
            for(let i= 0; i<respuesta.length; i++){

              if(respuesta[0][3] === '0'){
                disabled = '';
              }else{                
                disabled = 'disabled';
              }

              respuesta_bitacora.innerHTML += `
                <tr align="center">
                    <td>
                     <input type='text' id='montom_${respuesta[i][4]}' class='cotizacion' value='${respuesta[i][0]}' maxlength="6" size="4">
                     </td>
                    <td>
                      <input type='text' id='otrom_${respuesta[i][4]}' class='cotizacion' value='${respuesta[i][1]}' maxlength="6" size="4">
                      <input type='text' id='id_mensajeria_${respuesta[i][4]}' value='${respuesta[i][5] }' hidden>
                    </td>
                    <td>
                      ${respuesta[i][2]}
                    </td>
                   
                    <td>
                      <button class='btn btn-success' onclick='actualizar_mensajeria("${respuesta[i][4]}")' ${disabled} >
                        <i class="fa fa-check" aria-hidden="true"></i>
                      </button>
                    </td>
                </tr>`;
            }
            
          }
        },
        error: function(request, err){
            console.log(err);
        }
      });
                 
    });


    $(document).on('click', '.check', function(){

      idsArray = [];

      let checkboxes = document.querySelectorAll('input[type=checkbox]:checked')

      for (var i = 0; i < checkboxes.length; i++) {
        idsArray.push(checkboxes[i].value)
      }
                 
    });

    function start_loader() {

      //console.log('here');
      document.getElementById("loader").style.display = "block";

    }


    $('#btn-pay').click(function(){

        console.log("here");
        console.log(idsArray);
        
        $.ajax({
        url:'update_pagos.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'idsArray' : idsArray,
        },
        success: function(respuesta) {
          
          $.ajax({
            url:'makeZip.php',
            type: 'POST',
            async: true,
            dataType: "json",
            data: {
                'idsArray' : idsArray,
            },
            success: function(respuesta) {
              $("#formZip").submit();
              document.getElementById("loader").style.display = "none";

              setInterval(cleanZip, 2000);

              function cleanZip() {
                $.ajax({
                  url: 'action/cleanFolderZip.php',
                  type: 'post',
                  data: {
                  },
                });
              } 

              document.getElementById("buscar").click();
            },
            error: function(request, err){
                console.log(err);
            }
          });
          

        },
        error: function(request, err){
            console.log(err);
        }
      });
      

    });

   
    function formatoMexico(number){

      const exp = /(\d)(?=(\d{3})+(?!\d))/g;
      const rep = '$1,';
      let arr = number.toString().split('.');
      arr[0] = arr[0].replace(exp,rep);
      
      return arr[1] ? arr.join('.'): arr[0];

    }

    function actualizar(id){

      console.log(id);
      
      let derechos = parseInt($('#derechos_'+id).val());
      let tenencia = parseInt($('#tenencia_'+id).val());
      let tarjeta = parseInt($('#tarjeta_'+id).val());
      let otro = parseInt($('#otro_'+id).val());
      let derechos_id = parseInt($('#derechos_id_'+id).val());

      let semaforo_derechos = document.getElementById('semaforo_derechos'+derechos_id);
      
      console.log(derechos);
      console.log(tenencia);
      console.log(tarjeta);
      console.log(otro);

      $('#total_'+id).text(formatoMexico(derechos+tenencia+tarjeta+otro));

        
        $.ajax({
          url: 'action/pagos_derechos.php',
          type: 'post',
          data: {
            'id': id,
            'derechos' : derechos,
            'tenencia' : tenencia,
            'tarjeta' : tarjeta,
            'otros' : otro
          },
          success: function(response){
            swal('Monto Actualizado', '', 'success');
            semaforo_derechos.style.color = "green";
          },
          error: function(request, err) {
            console.log(err);
          }
        });
        
    }

    function actualizar_honorarios(id){

      console.log(id);
      
      let montoh = parseInt($('#montoh_'+id).val());
      let otroh = parseInt($('#otroh_'+id).val());
      let honorarios_id = parseInt($('#honorarios_id_'+id).val());
      let semaforo_honorarios = document.getElementById('semaforo_honorarios'+honorarios_id);

      console.log(montoh);
      console.log(otroh);
     
        $.ajax({
          url: 'action/pagos_honorarios.php',
          type: 'post',
          data: {
            'id': id,
            'montoh' : montoh,
            'otroh' : otroh
          },
          success: function(response){
            swal('Monto Actualizado', '', 'success');
            semaforo_honorarios.style.color = "green";
          },
          error: function(request, err) {
            console.log(err);
          }
        });
        
    }

    $('#borrar').click(function() {
      respuesta.innerHTML = '';
    });

    function actualizar_mensajeria(id){

      console.log(id);
      
      let montom = parseInt($('#montom_'+id).val());
      let otrom = parseInt($('#otrom_'+id).val());
      let id_mensajeria = parseInt($('#id_mensajeria_'+id).val());
      console.log("semaforo mensajeria");
        console.log(id_mensajeria);
      console.log("semaforo mensajeria");
      let semaforo_mensajeria = document.getElementById('semaforo_mensajeria'+id_mensajeria);

      console.log(montom);
      console.log(otrom);
     
        
        $.ajax({
          url: 'action/pagos_mensajeria.php',
          type: 'post',
          data: {
            'id': id,
            'montom' : montom,
            'otrom' : otrom
          },
          success: function(response){
            swal('Monto Actualizado', '', 'success');
            semaforo_mensajeria.style.color = "green";
          },
          error: function(request, err) {
            console.log(err);
          }
        });
        
    }

  </script>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>