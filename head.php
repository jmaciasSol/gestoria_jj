<?php
    session_start();
    
    if ($_SESSION['user_id'] == '') {
        header('Location: index.php');
    }

    if($_SESSION['tiempo']) {
        //Tiempo en segundos para dar vida a la sesión.
        $inactivo = 600;//20min en este caso.

        //Calculamos tiempo de vida inactivo.

        $vida_session = time() - $_SESSION['tiempo'];

            //Compraración para redirigir página, si la vida de sesión sea mayor a el tiempo insertado en inactivo.
            if($vida_session > $inactivo)
            {
                //Removemos sesión.
                session_unset();
                //Destruimos sesión.
                session_destroy();              
                //Redirigimos pagina.
                header("Location: index.php");

                exit();
            }

    }else{
        $_SESSION['tiempo'] = time();
    }




    include "config/config.php";

    $my_user_id=$_SESSION['user_id'];
    $query=mysqli_query($con,"SELECT * from tbl_usuarios where ID=$my_user_id");
    while ($row=mysqli_fetch_array($query)) {
        $fullname = $row['NOMBRECOMPLETO'];
        $email = $row['EMAIL'];
        //$profile_pic = $row['image'];
        $created_at = $row['FECHA_CREACION'];

        $contabilidad= $row['CONTABILIDAD'];
        $admin_crfact= $row['ADMIN_C'];
    	$admin_jj= $row['ADMIN_JJ'];
	    $usuario=$row['USUARIO_C'];
        $comercial=$row['COMERCIAL'];
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>.:Gestoria J&J:.</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="Last-Modified" content="0">
    <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
    <meta http-equiv="Pragma" content="no-cache">

     <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

     <!---->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">




      <!-- Bootstrap 3.3.6 -->
      <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
      <!-- Font Awesome -->
        <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

      <!-- Ionicons -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
      <!-- DataTables -->
      <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!-- <link rel="stylesheet" type="text/css" href="css/reset.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">

    <!-- micss -->
    <link rel="stylesheet" href="css/micss.css?var=<?php echo rand(); ?>"/>

   
     <!--  <link rel="stylesheet" href="css/micss.css?v=<?php echo time(); ?>" /> -->    
    <style>
            .skin-black .main-header .logo {
                background-color: #dd4b39;
                color: #fff;
            }
            .skin-black .main-header .logo:hover {
                background-color: #dd4b39;
                color: #fff;
            }
            .skin-black .main-header .navbar {
                background-color: #dd4b39;
                color: #fff;
            }
            .skin-black .main-header li.user-header{
                background-color: #dd4b39;
                color: #fff;
            }
            .skin-black .main-header .navbar .sidebar-toggle:hover{
                background-color: #dd4b39;
                color: #fff;
            }

            .skin-black .main-header .navbar>.sidebar-toggle {
                color: #fff;
                border-right: 1px solid #dd4b39;
            }



            .skin-black .main-header .navbar .nav>li>a:hover, .skin-black .main-header .navbar .nav>li>a:active, .skin-black .main-header .navbar .nav>li>a:focus, .skin-black .main-header .navbar .nav .open>a, .skin-black .main-header .navbar .nav .open>a:hover, .skin-black .main-header .navbar .nav .open>a:focus, .skin-black .main-header .navbar .nav>.active>a {
                background: #dd4b39;
                color: #fff;
            }

            .skin-black .main-header .navbar .nav>li>a:hover, .skin-black .main-header .navbar .nav>li>a:active, .skin-black .main-header .navbar .nav>li>a:focus, .skin-black .main-header .navbar .nav .open>a, .skin-black .main-header .navbar .nav .open>a:hover, .skin-black .main-header .navbar .nav .open>a:focus, .skin-black .main-header .navbar .nav>.active>a:hover {
                background: #d24837;
                color: #fff;
            }

            .skin-black .main-header .navbar .nav>li>a, .skin-black .main-header .navbar .nav>li>a:active, .skin-black .main-header .navbar .nav>li>a:focus, .skin-black .main-header .navbar .nav .open>a, .skin-black .main-header .navbar .nav .open>a:hover, .skin-black .main-header .navbar .nav .open>a:focus, .skin-black .main-header .navbar .nav>.active>a {
                color: #fff;
            }

            .skin-black .main-header .navbar .navbar-custom-menu .navbar-nav>li>a, .skin-black .main-header .navbar .navbar-right>li>a {
                border-left: 1px solid #dd4b39;
                border-right-width: 0;
            }
                
            .skin-black .main-header>.logo {
                background-color: #d24837;
                color: #fff;
                border-bottom: 0 solid transparent;
                border-right: 1px solid #dd4b39;
            }
            .skin-black .main-header>.logo:hover {
                background-color: #d24837;
            }



    </style>            

</head>
