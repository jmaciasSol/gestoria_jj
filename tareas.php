<?php

$active = "active";
include "head.php";
include "header.php";
include "aside.php";
include_once "config/config.php";
include_once "action/encript.php";

$id = $_POST['id'];

$datos = $con->query("SELECT * FROM v_tareas T WHERE T.HJID_TBL_CONTRATOS = $id");
$datos_2 = $datos->fetch_object();
?>

<div class="content-wrapper">
    <section class="content-header" hidden>
        <h1></h1>
        <ol class="breadcrumb">
            <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Importar Activaciones</li>
        </ol>
    </section>
    <!DOCTYPE html>
    <html>
        <head>
            <title></title>
        </head>
        <body>
            <h1 style="padding-top: 25px;">&nbsp;&nbsp;Tareas</h1>
            <div id="loader" style="display: none"></div>
            <!--Inicio de Formulario-->
            <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
                <form>
                    <div class="container" style="width: 100%">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                <button type="button" name="regresar" id="regresar" class="btn btn-primary" style="font-size: 20px;" onclick="location.href='find_your_client.php'">
                                    <i class="fa fa-arrow-left"></i>
                                    <b>&nbsp; &nbsp; Regresar</b>
                                </button>
                                </div>
                            </div>
                            
                        </div>  
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label for="nombre">Nombre</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre" value= "<?php echo encrypt_decrypt('decrypt',$datos_2->NOMBRE)?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="rfc">RFC</label>
                                    <input type="text" class="form-control" id="rfc" name="rfc" value= "<?php echo $datos_2->RFC;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="tipo_persona">T. Persona</label>
                                    <input type="text" class="form-control" id="tipo_persona" name="tipo_persona" value= "<?php echo $datos_2->TIPO_PERSONA;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="estado">Estado</label>
                                    <input type="text" class="form-control" id="estado" name="estado" value= "<?php echo $datos_2->EDO_ORIGEN;?>" disabled="">
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="fecha_1">
                                    <label for="tel_otro">Tel. Otro</label>
                                    <input type="input" class="form-control" id="tel_otro" name="tel_otro" value= "<?php echo $datos_2->TEL_OTRO;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="tel_celular">Tel. Celular</label>
                                    <input type="input" class="form-control" id="tel_celular" name="tel_celular" value= "<?php echo $datos_2->TEL_CELULAR;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="tel_casa">Tel. Casa</label>
                                    <input type="text" class="form-control" id="tel_casa" name="tel_casa" value= "<?php echo $datos_2->TEL_CASA;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="tel_trabajo">Tel. Trabajo</label>
                                    <input type="text" class="form-control" id="tel_trabajo" name="tel_trabajo" value= "<?php echo $datos_2->TEL_TRABAJO;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="input" class="form-control" id="email" name="email" value= "<?php echo $datos_2->EMAIL;?>" disabled="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="no_cliente">No. Cliente</label>
                                    <input type="input" class="form-control" id="no_cliente" name="no_cliente" value= "<?php echo $datos_2->NO_CLIENTE;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="folio">Folio</label>
                                    <input type="input" class="form-control" id="folio" name="folio" value= "<?php echo $datos_2->FOLIO;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="vin">VIN</label>
                                    <input type="input" class="form-control" id="vin" name="vin" value= "<?php echo $datos_2->VIN;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="tipo_auto">T. Auto</label>
                                    <input type="input" class="form-control" id="tipo_auto" name="tipo_auto" value= "<?php echo $datos_2->TIPO_AUTO;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="marca">Marca</label>
                                    <input type="input" class="form-control" id="marca" name="marca" value= "<?php echo $datos_2->MARCA;?>" disabled="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="modelo">Modelo</label>
                                    <input type="input" class="form-control" id="modelo" name="modelo" value= "<?php echo $datos_2->MODELO;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="fecha_2">Versión</label>
                                    <input type="input" class="form-control" id="fecha_2" name="fecha_2" value= "<?php echo $datos_2->VERSION;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="anio">Año</label>
                                    <input type="input" class="form-control" id="anio" name="anio" value= "<?php echo $datos_2->ANIO;?>" disabled="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="edo_cotizacion">E.Cotización</label>
                                    <input type="input" class="form-control" id="edo_cotizacion" name="edo_cotizacion" value= "<?php echo $datos_2->EDO_COTIZACION;?>" disabled="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="tramite">Trámite</label>
                                    <?php 
                                        $sql = "SELECT ID, TRAMITE FROM tbl_cattramites C WHERE C.ID>1";
                                        $result = mysqli_query($con, $sql);
                                        $items=mysqli_fetch_all($result, MYSQLI_ASSOC);
                                    ?>
                                    
                                    <select id="tramite" class="form-control">
                                        <?php  
                                            foreach ($items as $item) {
                                                echo "<option value='$item[ID]'>$item[TRAMITE]</option>";
                                            }

                                        ?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label for="entrega">Entregar en</label>
                                    <select name="proceso" id="entrega" class="form-control">
                                        <option value="">Seleccionar </option>
                                        <option value="0">Casa</option>
                                        <option value="1">Trabajo</option>
                                        <option value="2">Sucursal y Agencia</option>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-2 mb-3">
                                    <div class="form-group" style="padding-top: 28px;">
                                    <button type="button" name="generar" id="generar" class="btn btn-success" style="font-size: 20px;" >
                                        <i class="fa fa-plus "></i>
                                        <b>&nbsp; &nbsp; Generar</b>
                                    </button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </form>
            </div>
            <!--Fin formulario-->
            <!--Inicio Tabla-->
            <div class="container-fluid" hidden>
                <div class="row" id="tabla">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <table id="ResultTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <th style="text-align:center">NO. CLIENTE</th>
                                <th style="text-align:center">NOMBRE DEL CLIENTE</th>
                                <th style="text-align:center">FECHA DE ALTA</th>
                                <th style="text-align:center">ASIGNADO</th>
                                <th style="text-align:center">PROCESO</th>
                                <th style="text-align:center">ESTATUS</th>
                                <th style="text-align:center">EXPEDIENTE</th>
                                <th style="text-align:center">DOCUMENTOS</th>
                                <th style="text-align:center">BITACORA</th>
                            </thead>
                            <tbody id="respuesta">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--Fin tabla-->
            <!--Script necesarios-->
            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

            <!--Script para el ajax-->
            <script type="text/javascript">
                //Preparar tabla
                $(document).ready(function() {
                    $('#ResultTable').DataTable();
                });

                $('#generar').click(function(){

                    $.ajax({
                        url: "action/updatelayout.php",
                        type: 'POST',
                        async : true,
                        data:{ 
                            'opcion' : $("#tramite").val()
                        },
                        success: function(resp) {
                            swal("Tarea Generada", "", "success");
                        },
                        error: function(request,err){
                            console.log("error")
                            console.log(err)
                        }
                    });

                });

            </script>

        </body>

    </html>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>