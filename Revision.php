<link rel="icon" href="images/JJ.ico">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



<?php 

    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 


    if($_SESSION['usuario'] == 'Comercial') {
    session_destroy();
    $_SESSION = array();

    header('Location: index.php'); 
    }


     function alert($msg,$val) {

        if($val==1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val==2){

            echo "<script type='text/javascript'>swal('Error Al Agregar','$msg','error');</script>";
        }else if($val==3){
             echo "<script type='text/javascript'>swal('Registro Movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Error Al Mover','$msg','error');</script>";
        }


            
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

        switch ($_GET['success']) {
            case 'true':
                alert("Se han cargado exitosamente",1); 
                break;
            

            case 'false':
                alert("",2);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }

      if(!empty($_GET['move2_success'])){

        switch ($_GET['move2_success']) {
            case 'true':
                alert("A Tabla Rechazados",3); 
                break;
            

            case 'false':
                alert("",4);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }
    }




    
        function getOnlyAllFiles($arreglo,$files_path,$folio,$id_str){

          $archivos = [];

          foreach ($arreglo as $item ) {
            if ($item != '.' && $item != '..') {
              $value_item = array(
                'nombre' => $item,
                'fecha' => filemtime($files_path.$folio.$id_str.'/'.$item)
              );
              array_push($archivos, $value_item);
            }
          }

          return $archivos;
        }

        function array_sort($array, $on, $order){
            $new_array = array();
            $sortable_array = array();

            if (count($array) > 0) {
                foreach ($array as $k => $v) {
                    if (is_array($v)) {
                        foreach ($v as $k2 => $v2) {
                            if ($k2 == $on) {
                                $sortable_array[$k] = $v2;
                            }
                        }
                    } else {
                        $sortable_array[$k] = $v;
                    }
                }

                switch ($order) {
                    case SORT_ASC:
                        asort($sortable_array);
                    break;
                    case SORT_DESC:
                        arsort($sortable_array);
                    break;
                }

                foreach ($sortable_array as $k => $v) {
                    $new_array[$k] = $array[$k];
                }
            }

            return $new_array;
        }
?>

<div class="content-wrapper"><!-- Content Wrapper. Contains page content --
   <section class="content-header">
  <section class="content-header"> Content Header (Page header) -->
     
        
<?php
//America/Mexico_City
include_once "config/config.php";
date_default_timezone_set ('America/Mexico_City');
setlocale(LC_ALL, 'es_MX');
$datos = $con->query("SELECT * FROM v_revision");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<h1>&nbsp; &nbsp; &nbsp; REVISI&Oacute;N</h1>

<div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
  <div class="container row">
    <div class="col-md-3">
    <form method="post" id="" action="make_excel.php" enctype="multipart/form-data" role="form">
      <div <?php if($usuario == 0 && $comercial == 0 && $admin_crfact == 0){echo "hidden";} ?> >
        <div class="row">
          <button type="submit" class="btn btn-success" id="btn-submit" style="font-size: 20px;"><i class="fa fa-cloud-download" aria-hidden="true">
          </i><b>&nbsp; &nbsp;Descargar Datos</b></button>
        </div>
        <div class="row" id="div_file_name" style="display: none; width: 500px;" >
          <input type="text" id="name_file" style="width: 390px;" disabled>
        </div>
      </div>
    </form>
    </div>
    <div class="col-md-8">
    <form method="post" id="" action="update_cotizaciones.php" enctype="multipart/form-data" role="form">
       <div class="row">
          <label class="custom-file-upload btn-info" style="padding-bottom: 10px; border-radius: 5px; border:solid 1px; ">
          <input type="file" name="name" class="custom-file-upload" id="carga_archivo">
            <i class="fa fa-cloud-upload"></i>&nbsp; &nbsp;Cargar Archivo
          </label>
          <button type="submit" class="btn btn btn-primary" id="btn-update" style="font-size: 20px;" onclick="start_loader()" disabled><i class="fa fa-cloud-download" aria-hidden="true">
          </i><b>&nbsp; &nbsp;Actualizar Datos</b></button>
       </div>
    </form>
    </div>
    </div>
  </div>

<?php if($datos->num_rows>0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table table-bordered table-hover nowrap" style="width:100%" >
  <thead>
    <th style="text-align:center">FECHA</th>
    <th style="text-align:center">NO CLIENTE</th>
		<th style="text-align:center">NOMBRE</th>

    <th style="text-align:center">VALOR FACTURA</th>
    <th style="text-align:center">EDO ORIGEN</th>
    <th style="text-align:center">PDV</th>

    <th style="text-align:center">DOCUMENTOS</th>

    <th style="text-align:center">VENDEDOR</th>
    <th style="text-align:center">EJECUTIVO</th>
    <th style="text-align:center">USUARIO</th> 
    
    <th style="text-align:center">ESTATUS</th>
    <th style="text-align:center">GUARDAR</th>
    <th style="text-align:center">RECHAZAR</th>


  </thead>
  <tbody>
  <?php while($d= $datos->fetch_object()):?>
    
    <tr align="center">
      <td ><?php 
        echo 
        '<a data-toggle="modal" href="#Modal_datos_'.$d->NO_CLIENTE.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></a>
        <div class="modal fade" id="Modal_datos_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                            </button>
                             <h3 class="modal-title" id="myModalLabel"><b>'.$d->NOMBRE.'</b></h3>

                        </div>
                        <div class="modal-body" style="text-align:left;">
                         <ul>
                          <li>FOLIO: '.$d->FOLIO.'</li>
                          <li>TIPO: '.$d->TIPO.'</li>
                          <li>MARCA: '.$d->MARCA.'</li>
                          <li>MODELO: '.$d->MODELO.'</li>
                          <li>VERSION: '.$d->VERSION.'</li>
                          <li>AÑO: '.$d->ANIO.'</li>
                         </ul>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
      ?>
      <?php echo "&nbsp".$d->FECHA; ?></td>
      <td ><?php echo $d->NO_CLIENTE; ?></td>
      <td ><?php echo $d->NOMBRE; ?></td>

      <td ><?php echo '$'.number_format($d->VALOR_FACTURA,2); ?></td>
      <td ><?php echo $d->EDO_ORIGEN; ?></td>
      <td ><?php echo $d->PDV; ?></td>
      <td>

        <?php
         echo 
          '<button class="popup-documentos btn btn-primary" id="'.$d->ID.'_'.$d->NO_CLIENTE.'" 
            data-toggle="modal" data-target="#Modal-Documentos"><i class="fa fa-file-text-o" aria-hidden="true"></i>
          </button>';
        ?>
      </td>

      <td ><?php echo $d->VENDEDOR; ?></td> 
      <td><?php echo $d->EJECUTIVO; ?></td>
      <td><?php $CORREO = explode("@", $d->USUARIO);  echo $CORREO[0]; ?></td>
      
      <td><?php echo'
        <select name="estatus_'.$d->NO_CLIENTE.'" id="estatus_'.$d->ID.'" class="estatus">
          <option value=20';if($d->ESTATUS == 'PENDIENTE') echo ' selected'; echo ' disabled >'.'PENDIENTE'.'</option>
          <option value=23';if($d->ESTATUS == 'INCOMPLETO') echo ' selected'; echo '>'.'INCOMPLETO'.'</option>
          <option value=30>'.'COMPLETO'.'</option> 
        </select>';  
        ?>
      </td>
      <td>
        <?php
        echo '
          <form action="changeStatus_revision.php" method="POST" style="display:block; margin:auto;">
        <input type="text" name="ID" value="'.$d->ID.'" hidden >
        <input type="text" name="value_estatus" id="value_estatus_'.$d->ID.'" hidden>
        <button class="cotizar btn btn-primary btn-success" id="Guardar_'.$d->ID.'" 
        ><i class="fa fa-save" aria-hidden="true"></i></button>
          </form>
        ';
        ?>
      </td>
      <td>
        <?php  
        echo '
        <form id="formRechazar_'.$d->NO_CLIENTE.'" action="rechazados.php" method="POST" style="display:block; margin:auto;">
          <input type="text" name="ID" value=" <?php echo $d->ID; ?>" hidden >
        </form>
        <button class="btn btn-danger" onclick="return messageRechazar('.$d->NO_CLIENTE.'); return false;"
          ><i class="fa fa-times" aria-hidden="true"></i></button>
        ';
        ?>
      </td>
    </tr>

  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
<?php else:?>
  <h3>No hay Datos</h3>
<?php endif; ?>


 <div class="modal fade modal-xl" id="Modal-Documentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style=" min-width:1000px !important;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"><b>Bitacora</b></h4>
                </div>
                <div class="container text-left" >
                    <label class="modal-title" id="label_bitacora"></label>
                </div>
                <div class="modal-body" style="overflow-y: auto; height: 150px;">
                    <table border="1" id="bitacora" style="border-collapse: collapse; width: 100%;">
                        <thead>
                            <tr>
                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">FECHA</th>
                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">DOCUMENTO</th>
                            <th style="text-align:center; background-color:#00c0ef; position: sticky; top: 0;">Descargar</th>

                            </tr>
                        </thead>
                        <tbody id="documentos">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="formDescargarDocumento" method="post" action="downloadS3.php" hidden>
  <input id="keyvalue" type="text" name="key">
</form>


</body>



<script>




$(document).ready( function () {

   $("#myTable").DataTable({
      
       "scrollX": true
    
    });
            
});

$(document).on('click', '.descargarDocumento', function(){

  $('#keyvalue').val(this.id);

  document.getElementById("formDescargarDocumento").submit();          

});


$(document).on('click', '.popup-documentos', function(){

  let identificador= this.id;
  let values = identificador.split("_");
  let id = values[0];
  let no_cliente = values[0];
  let respuesta_documentos = document.querySelector('#documentos');
  respuesta_documentos.innerHTML = '';

      $.ajax({
        url:'action/getDocumentos.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id' : id
        },
        success: function(resp) {
          for(let i= 0; i<resp.length; i++){
            respuesta_documentos.innerHTML += `
            <tr align="center">
              <td>${resp[i]['FECHA']}</td>
              <td>${resp[i]['DOCUMENTO']}</td>
              <td><button class="descargarDocumento btn-default" id="${resp[i]['DOCUMENTO']}">Descargar</button></td>
            </tr>`;
          }
        },
        error: function(request, err){
            console.log(err);
        }
    });

});

 $("#carga_archivo").change(function(){

      var path= $(this).val();
      var name  = path.split('\\');
      var ext = name[2].split(".");

      console.log("carga archivo");

      if(ext[1] == 'xlsx'){
        console.log(ext[1]);

        $("#name_file").val("Archivo: "+name[2]);

        console.log(name);
        $("#btn-update").attr("disabled",false);
        document.getElementById("div_file_name").style.display = "block";
       
      }else{
        console.log(ext[1]);
        $("#btn-update").attr("disabled",true);
        swal('Formato no permitido','Cargue un documento .xlsx', 'error');

      }

  });
      
$( ".estatus" ).change(function() {


  var identificador= this.id;
  var values = identificador.split("_");
  
  var id = values[1];

  var estatus =$(this).val();

   console.log($('#value_estatus_'+id).val()); 


  $('#value_estatus_'+id).val(estatus);

  console.log(estatus);
  console.log(id);
  console.log($('#value_estatus_'+id).val()); 
 
});


  function messageRechazar(no_cliente){
   
    swal({
    title: '',
    text: "¿Desea rechazar este registro?",
    icon: "info",
    buttons: ["Cancelar",true],
    dangerMode: true,
    })
    .then((value) => {
      if (value){
        document.getElementById("formRechazar_"+no_cliente).submit();        
      }
    });

  }
      
</script>

</html>

</div> <!-- este div cierra es el del principio -->


<?php include "footer.php"; ?>
