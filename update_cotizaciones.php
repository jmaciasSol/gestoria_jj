<?php
include_once "config/config.php";
include "class.upload.php";

session_start();

$user_id=$_SESSION["user_id"];
$datos = $con->query("SELECT EMAIL FROM tbl_usuarios WHERE ID = $user_id");

$d= $datos->fetch_object();
$correo=$d->EMAIL;
$registros_fallidos = array();
$registros_exitosos = array();

if(isset($_FILES["name"])){
  $up = new Upload($_FILES["name"]);
  if($up->uploaded){
    $up->Process("./Output/");
    if($up->processed){
            /// leer el archivo excel
            require_once 'PHPExcel/Classes/PHPExcel.php';
            $archivo = "Output/".$up->file_dst_name;
            $inputFileType = PHPExcel_IOFactory::identify($archivo);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($archivo);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

            /*
            if($archivo){
                $ruta_destino_archivo = "Output/".$_FILES["name"]["name"];
                $archivo_ok = move_uploaded_file($_FILES["name"]['tmp_name'], $ruta_destino_archivo);
            }else{
                $ruta_destino_archivo='';
            }*/

            if($highestColumn != 'Q'){
                echo "<script>
                    window.location = './Revision.php?success=error';
                </script>"; 
            }else{


                for ($row = 2; $row <= $highestRow; $row++){ 

                    $no_cliente = $sheet->getCell("A".$row)->getValue();
                    $nombre = $sheet->getCell("B".$row)->getValue();
                    $rfc = $sheet->getCell("C".$row)->getValue();
                    $tipo = $sheet->getCell("D".$row)->getValue();
                    $calle = $sheet->getCell("E".$row)->getValue();

                    $n_int = $sheet->getCell("F".$row)->getValue();
                    $n_ext = $sheet->getCell("G".$row)->getValue();
                    $colonia = $sheet->getCell("H".$row)->getValue();
                    $municipio = $sheet->getCell("I".$row)->getValue();
                    $ciudad = $sheet->getCell("J".$row)->getValue();

                    $cp = $sheet->getCell("K".$row)->getValue();
                    $estado = $sheet->getCell("L".$row)->getValue(); 
                    $tel_casa = $sheet->getCell("M".$row)->getValue();

                    $tel_trabajo = $sheet->getCell("N".$row)->getValue();
                    $tel_otro = $sheet->getCell("O".$row)->getValue(); 
                    $tel_celular = $sheet->getCell("P".$row)->getValue();
                    $email = $sheet->getCell("Q".$row)->getValue();


                    $registro = array(
                        'no_cliente' => $no_cliente,
                        'nombre' => $nombre,
                        'rfc' => $rfc,
                        'tipo' => $tipo,
                        'calle' => $calle,
                        'n_int' => $n_int,
                        'n_ext' => $n_ext,
                        'colonia' => $colonia,
                        'municipio' => $municipio,
                        'ciudad' => $ciudad,
                        'cp' => $cp,
                        'estado' => $estado,
                        'tel_casa' => $tel_casa,
                        'tel_trabajo' => $tel_trabajo,
                        'tel_otro' => $tel_otro,
                        'tel_celular' => $tel_celular,
                        'email' => $email
                    );

                     $sql =" 
                    UPDATE tbl_clientes c, tbl_contratos t
                        SET
                            c.RFC = '$rfc'
                          , c.TIPO = '1'
                          , c.CALLE = '$calle'
                          , c.N_INT = '$n_int'    
                          , c.N_EXT = '$n_ext'
                          , c.COLONIA = '$colonia'
                          , c.MUNICIPIO = '$municipio'
                          , c.CIUDAD = '$ciudad'
                          , c.CP = '$cp'
                          , c.ESTADO = '$estado'
                          , c.TEL_CASA = '$tel_casa'
                          , c.TEL_TRABAJO = '$tel_trabajo'
                          , c.TEL_OTRO = '$tel_otro'
                          , c.TEL_CELULAR = '$tel_celular'
                          , c.EMAIL = '$email'
                    WHERE c.ID = t.HJID_CLIENTES
                    AND t.NO_CLIENTE = '$no_cliente' ";

                      $con->query($sql);    

                       print "<script>window.location='Revision.php';</script>";


                    /*
                     $sql ="INSERT INTO tbl_staging(TRAMITE, NO_CLIENTE, NOMBRE, TIPO, MARCA, MODELO, VERSION, ANIO, VALOR_FACTURA, EDO_ORIGEN, PDV, VENDEDOR, EJECUTIVO, EDO_COTIZACION_1,EDO_COTIZACION_2,EDO_COTIZACION_3,USUARIO)
                            VALUES ";
                    */

                }
            
            }
        }       
    }
}

?>
