
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



<?php 
    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 


    function alert($msg,$val) {

        if($val==1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val==2){

            echo "<script type='text/javascript'>swal('Error Al Agregar','$msg','error');</script>";
        }else if($val==3){
             echo "<script type='text/javascript'>swal('Registro Movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Error Al Mover','$msg','error');</script>";
        }


            
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

        switch ($_GET['success']) {
            case 'true':
                alert("Se han cargado exitosamente",1); 
                break;
            

            case 'false':
                alert("",2);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }

      if(!empty($_GET['move_success'])){

        switch ($_GET['move_success']) {
            case 'true':
                alert("A tabla Aceptados",3); 
                break;
            

            case 'false':
                alert("",4);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }


    }

?>

<div class="content-wrapper" ><!-- Content Wrapper. Contains page content --
   <section class="content-header">
  <section class="content-header"> Content Header (Page header) -->
     <section class="content-header" hidden>
            <h1></h1>
            <ol class="breadcrumb">
                <li><a href="home.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Importar Activaciones</li>
            </ol>
      </section>

        
<?php

//session_start();

//Modificar 

include_once "config/config.php";
$datos = $con->query("select * from tbl_staging where No_CLIENTE <> '' ");

$activos= $con->query("SELECT COUNT(*) Abiertos FROM db_sistema.tbl_staging;");

?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<h1 style="padding-top: 25px;">&nbsp;&nbsp;COTIZACIONES</h1>

<div id="loader" style="display: none" ></div>


  <div style="padding-top: 50px; padding-bottom: 15px; padding-left:25px; font-size: 20px;">
    <form method="post" id="addproduct" action="import_4.php" enctype="multipart/form-data" role="form">
      <div <?php if($_SESSION["usuario"]=="Admin_JJ" || $usuario==0){echo "hidden";} ?> >
        <div class="row">
          <label class="custom-file-upload btn-info" style="padding-bottom: 10px; border-radius: 5px; border:solid 1px; ">
          <input type="file" name="name" class="custom-file-upload" id="carga_archivo" placeholder="Archivo (.xlsx)">
            <i class="fa fa-cloud-upload"></i>&nbsp; &nbsp;Cargar Archivo
          </label>
          <button type="submit" class="btn btn-success" style="font-size: 20px;" onclick="start_loader()" ><i class="fa fa-cloud-download" aria-hidden="true">
          </i><b>&nbsp; &nbsp;Importar Datos</b></button>
        </div>
      </div>
      <div class="row" id="div_file_name" style="display: none; width: 500px;" >
        <input type="text" id="name_file" style="width: 390px;" disabled>
      </div>
    </form>
  </div>

<?php if($datos->num_rows>0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table table-bordered table-hover nowrap" style="width:100%" >
  <thead>
        
          <th  style="text-align:center">Fecha</th>
          <th style="text-align:center">N°cliente</th>
            <th  style="text-align:center">Referencia</th>
            <th  style="text-align:center">Serie de la unidad</th>
            <th  style="text-align:center">Nombre del cliente</th>
            <th style="text-align:center">Entidad</th>
            <th style="text-align:center">Valor de la unidad</th>
            <th style="text-align:center">Ejecutivo</th>
            <th style="text-align:center">Regional</th>
            <th style="text-align:center">Agencia</th>
            <th <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> style="text-align:center">Factura</th>
            <th <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> style="text-align:center">Proceso de Cotizaci&oacute;n</th>

            <th style="text-align:center">Estado 1</th> 
            <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 1</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 1</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Aceptar</th>

            <th  style="text-align:center">Estado 2</th>
            <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 2</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 2</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Aceptar</th>

            <th style="text-align:center">Estado 3</th>
            <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 3</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Cotizaci&oacute;n 3</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Aceptar</th>

            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Comprobante</th>
            <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Guardar</th>

            <th <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> style="text-align:center">Rechazar</th>

             <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Actualizar</th>

  </thead>
  <tbody>
  <?php while($d= $datos->fetch_object()):?>
    <tr align="center">
    <td ><?php echo $d->created_at; ?></td>
    <td ><?php echo $d->No_CLIENTE; ?></td>
    <td class="<?php echo "vin_".$d->No_CLIENTE; ?>"><?php echo $d->REFERENCIA; ?></td>
    <td class="<?php echo "vin_".$d->No_CLIENTE; ?>"><?php echo $d->SERIE_DE_UNIDAD; ?></td>
    <td class="<?php echo "nombre_".$d->No_CLIENTE; ?>"><?php echo $d->NOMBRE_DEL_CLIENTE; ?></td>
    <td class="<?php echo "entidad_".$d->No_CLIENTE; ?>"><?php echo $d->ENTIDAD_SOLICITADA; ?></td>
    <td class="<?php echo "entidad_".$d->No_CLIENTE; ?>"><?php echo $d->VALOR_DE_LA_UNIDAD; ?></td>
    <td class="<?php echo "entidad_".$d->No_CLIENTE; ?>"><?php echo $d->EJECUTIVO; ?></td>
    <td class="<?php echo "entidad_".$d->No_CLIENTE; ?>"><?php echo $d->REGIONAL; ?></td>
    <td class="<?php echo "entidad_".$d->No_CLIENTE; ?>"><?php echo $d->AGENCIA; ?></td>
     
   
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
        <label class=" custom-file-upload">
        <input type="file" class="factura" accept="application/pdf" id="Factura_<?php echo $d->No_CLIENTE; ?>" >
        <i class="fa fa-cloud-upload"></i>
        </label>
     </td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
      <?php
          echo '

        <button class="cotizar btn-default" id="_'.$d->No_CLIENTE.'" 
        data-toggle="modal" data-target="#myModal_'.$d->No_CLIENTE.'" style="width: 80px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </button>

        <div class="modal fade" id="myModal_'.$d->No_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                          </button>
                           <h4 class="modal-title" id="myModalLabel"><b>Iniciar Proceso</b></h4>

                      </div>
                      <div class="modal-body">
                        <table style="width:100%">
                          <thead>
                            <th style="text-align:center">Cotizaci&oacute;n</th>
                            <th style="text-align:center">Estado</th>
                            <th style="text-align:center">Valor</th>
                            <th style="text-align:center"></th>
                          </thead>

                          <tbody>
                            <tr style="text-align:center">
                            <td>1</td>
                            <td>'.$d->estado1.'
                            </td>
                            <td>'.$d->cotizacion1.'</td>
                            <td><input type="checkbox" id="cbox1" value="first_checkbox"></td>
                            </tr>
                            <tr style="text-align:center">
                            <td>2</td>
                            <td>'.$d->estado2.'
                            </td>
                            <td>'.$d->cotizacion2.'</td>
                            <td><input type="checkbox" id="cbox1" value="first_checkbox"></td>
                            </tr>
                            <tr style="text-align:center">
                            <td>3</td>
                            <td>'.$d->estado3.'
                            </td>
                            <td>'.$d->cotizacion3.'</td>
                            <td><input type="checkbox" id="cbox1" value="first_checkbox"></td>
                            </tr>
                          </tbody>

                        </table>

                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      ';

    ?>

    </td>
   
    <td><?php echo $d->estado1; ?></td>
    <td 
      <?php 
        if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} 
      ?> 
    >
      <input type="text" id="<?php echo "valor1_".$d->No_CLIENTE;?>" class="cotizacion" <?php if($d->estado1 == null){echo "value='0' disabled";} else {echo "value=".$d->cotizacion1;} ?>>
    </td>
    

    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><?php echo $d->cotizacion1; ?></td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><button class="cotizar btn btn-success"
    <?php if($d->estado1 == null || $d->cotizacion1 == 0){echo "disabled";}?> 
    id="<?php echo "cotizar1_".$d->No_CLIENTE; ?>" ><i class="fa fa-check" aria-hidden="true"></i></button></td>

    <td><?php echo $d->estado2; ?></td>
    <td 
      <?php 
        if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} 
      ?> 
    >
      <input type="text" id="<?php echo "valor2_".$d->No_CLIENTE; ?>" class="cotizacion" <?php if($d->estado2 == null){echo "value='0' disabled";}else {echo "value=".$d->cotizacion2;} ?>>
    </td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><?php echo $d->cotizacion2; ?></td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><button class="cotizar btn btn-success"
    <?php if($d->estado2 == null || $d->cotizacion2 == 0){echo "disabled";} ?> id="<?php echo "cotizar2_".$d->No_CLIENTE; ?>"  ><i class="fa fa-check" aria-hidden="true"></i></button></td>


    <td ><?php echo $d->estado3; ?></td>
    <td 
      <?php 
        if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} 
      ?> 
    >
      <input type="text" id="<?php echo "valor3_".$d->No_CLIENTE;?>" class="cotizacion" <?php if($d->estado3 == null){echo "value='0' disabled";}else {echo "value=".$d->cotizacion3;} ?>>
    </td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><?php echo $d->cotizacion3; ?></td>
    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>><button class="cotizar btn btn-success"
    <?php if($d->estado3 == null || $d->cotizacion3 == 0){echo "disabled";} ?> id="<?php echo "cotizar3_".$d->No_CLIENTE; ?>"  ><i class="fa fa-check" aria-hidden="true"></i></button></td>

    <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?> >
          <label class=" custom-file-upload">
          <input type="file" id="<?php echo "Comprobante_".$d->No_CLIENTE; ?>" accept="application/pdf" disabled>
          <i class="fa fa-cloud-upload"></i>
          </label>
            <!--<div class="new_Btn"><i class="fa fa-cloud-upload"></i>Factura</div><br>
            <input  type='file' style="display:none"/><br> -->
     </td>
     <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>  >
      <?php
          echo '

     <form action="en_proceso.php" method="POST" style="margin-top:15px;">
      <input type="text" name="numero_cliente" value="'.$d->No_CLIENTE.'" hidden >
      <input id="num_button_'.$d->No_CLIENTE.'" name="opcion" type="text" hidden>
      <button class="btn btn-primary" id="Guardar_'.$d->No_CLIENTE.'" onclick="mi_funcion('.$d->No_CLIENTE.')" disabled 
      ><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
      </form>
      ';

    ?>
      </td>



    <td <?php  if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>>
      <?php
        echo '
      <form action="rechazados2.php" method="post">
      <input type="text" name="numero_cliente" value="'.$d->No_CLIENTE.'" hidden >
      <button class="btn btn-danger" id="Rechazar_"'.$d->No_CLIENTE.'"  
      ><i class="fa fa-times" aria-hidden="true"></i></button>
      </form>     
        ';  
      ?>         
    </td>

     <td <?php  if($_SESSION["usuario"]!="Admin_JJ"){echo "hidden";} ?> >
      <?php  echo '
            <button class="btn-click-action btn btn-success" id="button_'.$d->No_CLIENTE.'" name="button_'.$d->No_CLIENTE.'"
            ><i class="fa fa-refresh" ></i></button>



            ';  
      ?>
    </td>

  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
</div>
<?php else:?>
  <h3 style="padding-left:15px;">NO SE HAN CARGADO DATOS</h3>
<?php endif; ?>
  
</body>



<script>

$(".btn-click-action").click(function() {

  var name= $(this).attr('name');
       
  var identificador = name.split("_");
   
  var valor1= "valor1_"+identificador[1];
  var valor2= "valor2_"+identificador[1];
  var valor3= "valor3_"+identificador[1];
  
  var nombre = $('.nombre_'+identificador[1]).text();
  var entidad = $('.entidad_'+identificador[1]).text();
  var vin = $('.vin_'+identificador[1]).text();

  if($("#"+valor1).val() == "" || $("#"+valor2).val() == "" || $("#"+valor3).val() == ""){
  
  
    swal("Campos Vacios", "Llene los campos de cotizacion", "info");
  
    //alert("derechos y honorarios no pueden ser vacios");
  
  }else{
  
    
    $.ajax({
        url: "action/updatelayout.php",
        type: 'POST',
       
        async : true,
        data:{ 
          'valor1' : $("#"+valor1).val(),
          'valor2' : $("#"+valor2).val(),
          'valor3' : $("#"+valor3).val(),
          'nombre' : nombre,  
          'entidad' : entidad,
          'vin' : vin,
          'id' : identificador[1]
         } ,
        success: function(resp) {

      
      //alert("Datos Actualizados"); 
      swal("Datos Actualizados", "", "success");
      
      
      window.location.reload(true);

        },
        error: function(request,err){
      console.log("error")
      console.log(err)
      

        }
    });
    
  }
  


});


  function start_loader(){

    document.getElementById("loader").style.display = "block";
  }

  $(".factura").change(function(){

        var identificador=$(this).attr('id');
        var id = identificador.split("_");
        var archivos = new FormData();
        var factura = $('#Factura_'+id[1])[0].files[0];
        //var comprobante = $('#Comprobante_'+id)[0].files[0];
        archivos.append('factura',factura);
        archivos.append('id',id[1]);

        swal({
          title: factura.name,
          text: "¿Cargar archivo?",
          icon: "info",
          buttons: ["Cancelar",true],
          dangerMode: true,
        })
        .then((value) => {
          if (value) {

            $.ajax({
                url: 'action/uploads_factura.php',
                type: 'post',
                data: archivos,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response > 0){
                        swal("Archivo cargado exitosamente", {
                          icon: "success",
                        });
                    }else{
                        alert('Error al cargar el archivo','','error');
                    }
                },
            });
            
          } else {
            $('#Factura_'+id[1]).val("");
          }
        });
  });

  function view(valor){
            console.log(valor.name);
        }


  function mi_funcion(id){

    //alert("Boton Donde se activo "+id);

        var archivos = new FormData();
        //var factura = $('#Factura_'+id)[0].files[0];
        var comprobante = $('#Comprobante_'+id)[0].files[0];
        
       // archivos.append('factura',factura);
        archivos.append('comprobante',comprobante);
        archivos.append('id',id);

        //console.log(fd['file1']);

         document.getElementById("loader").style.display = "block";

        $.ajax({
            url: 'action/uploads_files.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response){
                if(response > 0){
                    //$("#img").attr("src",response); 
                    //$(".preview img").show(); // Display image element
                    //setTimeout(function(){ alert("Hello"); }, 3000);
                    //alert("Archivos Subidos Correctamente");
                    //swal( {closeOnClickOutside: false, timer: 4000},"Archivos Cargados","","success");
                }else{
                    alert('file not uploaded');
                }
            },
        });

  }


 $("#carga_archivo").change(function(){

    var path= $(this).val();
    
    var name  = path.split('\\');

    $("#name_file").val("Archivo: "+name[2]);

    console.log(name);

     document.getElementById("div_file_name").style.display = "block";

 });


$(".cotizar").click(function(){

      var name= $(this).attr('id');
       
      var identificador = name.split("_");

      console.log(identificador);

      $("#num_button_"+identificador[1]).val(identificador[0]);


      switch(identificador[0]){

        case "cotizar1" :
           document.getElementById("cotizar2_"+identificador[1]).disabled = true;
           document.getElementById("cotizar3_"+identificador[1]).disabled = true;
          break;

        case "cotizar2" :
          document.getElementById("cotizar1_"+identificador[1]).disabled = true;
          document.getElementById("cotizar3_"+identificador[1]).disabled = true;
          break;

        case "cotizar3" :
          document.getElementById("cotizar1_"+identificador[1]).disabled = true;
          document.getElementById("cotizar2_"+identificador[1]).disabled = true;
          break;

      }


      document.getElementById("Factura_"+identificador[1]).disabled = false;
      document.getElementById("Comprobante_"+identificador[1]).disabled = false;
      document.getElementById("Guardar_"+identificador[1]).disabled = false;
        

      /*
        desabilitar los demas botones

      */



      $.ajax({
            url: 'action/set_autorizacion.php',
            type: 'post',
            data: {'name': identificador[0],
                  'id' : identificador[1]
                  },
           
            success: function(response){
                
            },
        });




    });


$(document).ready( function () {

   $("#myTable").DataTable({
      
       "scrollX": true
    
    });
        
});
      
      
      





</script>

</html>

</div> <!-- este div cierra es el del principio -->

<?php include "footer.php"; ?>

<!--

 

  </form>

  -->