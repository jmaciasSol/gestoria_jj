<?php
class Cl_User
{
	/**
	 * @var va a contener la conexión de base de datos
	 */
	protected $_con;
	
	/**
	 * Inializar DBclass
	 */
	public function __construct()
	{
		$db = new Cl_DBclass();
		$this->_con = $db->con;
	}
	
	/**
	 * Registro de usuarios
	 * @param array $data
	  */
	public function registration( array $data )
	{
		if( !empty( $data ) ){
			
			// Trim todos los datos entrantes:
			$trimmed_data = array_map('trim', $data);
			
			
			
			// escapar de las variables para la seguridad
			$name = mysqli_real_escape_string( $this->_con, $trimmed_data['name'] );
			$password = mysqli_real_escape_string( $this->_con, $trimmed_data['password'] );
			$cpassword = mysqli_real_escape_string( $this->_con, $trimmed_data['confirm_password'] );
			
			
			// Verifica la direccion de correo electrónico:
			if (filter_var( $trimmed_data['email'], FILTER_VALIDATE_EMAIL)) {
				$email = mysqli_real_escape_string( $this->_con, $trimmed_data['email']);
			} else {
				throw new Exception( "Por favor, introduce una dirección de correo electrónico válida!" );
			}
			
			
			if((!$name) || (!$email) || (!$password) || (!$cpassword) ) {
				throw new Exception( FIELDS_MISSING );
			}
			if ($password !== $cpassword) {
				throw new Exception( PASSWORD_NOT_MATCH );
			}
			$password = md5( $password );
			$query = "INSERT INTO user (user_id, name, email, password, created) VALUES (NULL, '$name', '$email', '$password', CURRENT_TIMESTAMP)";
			if(mysqli_query($this->_con, $query)){
				mysqli_close($this->_con);
				return true;
			};
		} else{
			throw new Exception( USER_REGISTRATION_FAIL );
		}
	}
	/**
	 * Este metodo para iniciar sesión
	 * @param array $data
	 * @return retorna falso o verdadero
	 */
	public function login( array $data )
	{
		$_SESSION['logged_in'] = false;
		if( !empty( $data ) ){
			
			// Trim todos los datos entrantes:
			$trimmed_data = array_map('trim', $data);
			
			// escapar de las variables para la seguridad
			$email = mysqli_real_escape_string( $this->_con,  $trimmed_data['email'] );
			$password = mysqli_real_escape_string( $this->_con,  $trimmed_data['password'] );
				
			if((!$email) || (!$password) ) {
				throw new Exception( LOGIN_FIELDS_MISSING );
			}
			$password = md5( $password );
			$query = "SELECT user_id, name, email, created FROM users where email = '$email' and password = '$password' ";
			$result = mysqli_query($this->_con, $query);
			$data = mysqli_fetch_assoc($result);
			$count = mysqli_num_rows($result);
			mysqli_close($this->_con);
			if( $count == 1){
				$_SESSION = $data;
				$_SESSION['logged_in'] = true;
				return true;
			}else{
				throw new Exception( LOGIN_FAIL );
			}
		} else{
			throw new Exception( LOGIN_FIELDS_MISSING );
		}
	}
	
	/**
	 * El siguiente metodo para verificar los datos de la cuenta para el cambio de contraseña
	 * @param array $data
	 * @throws Exception
	 * @return boolean
	 */
	
	public function account( array $data )
	{
		if( !empty( $data ) ){
			// Trim todos los datos entrantes:
			$trimmed_data = array_map('trim', $data);
			
			// escapar de las variables para la seguridad
			$password = mysqli_real_escape_string( $this->_con, $trimmed_data['password'] );
			$cpassword = $trimmed_data['confirm_password'];
			$user_id = mysqli_real_escape_string( $this->_con, $trimmed_data['user_id'] );
			
			if((!$password) || (!$cpassword) ) {
				throw new Exception( FIELDS_MISSING );
			}
			if ($password !== $cpassword) {
				throw new Exception( PASSWORD_NOT_MATCH );
			}
			$password = md5( $password );
			$query = "UPDATE user SET password = '$password' WHERE id = '$user_id'";
			if(mysqli_query($this->_con, $query)){
				mysqli_close($this->_con);
				return true;
			}
		} else{
			throw new Exception( FIELDS_MISSING );
		}
	}
	
	/**
	 * Este metodo para cerrar las sesión
	 */
	public function logout()
	{
		session_unset();
		session_destroy();
		header('Location: index.php');
	}
	
	/**
	 * Esto restablece la contraseña actual y la nueva contraseña para enviar correo
	 * @param array $data
	 * @throws Exception
	 * @return boolean
	 */
	public function forgetPassword( array $data )
	{
		if( !empty( $data ) ){
			
			// escapar de las variables para la seguridad
			$email = mysqli_real_escape_string( $this->_con, trim( $data['email'] ) );
			
			if((!$email) ) {
				throw new Exception( FIELDS_MISSING );
			}
			$password = $this->randomPassword();
			$password1 = sha1(md5( $password ));
			$query = "UPDATE tbl_usuarios SET PSW = '$password1' WHERE EMAIL = '$email'";
			if(mysqli_query($this->_con, $query)){
				mysqli_close($this->_con);

				require 'PHPMailer/PHPMailerAutoload.php';
				//$to = $email;
				//$subject = "Solicitud de actualizacion de password";

				/*
				$txt = "Estimado usuario,
					Recientemente se envió una solicitud para restablecer una contraseña 
					para su cuenta. 
				Si esto fue un error, simplemente ignore  estecorreoelectrónico y no pasará nada.
				Para restablecer su contraseña, ingrese su nueva contraseña ".$password;
				

				*/

				$mail = new PHPMailer();

				$mail->isSMTP();
						
				$mail->SMTPAuth = true;
				$mail->SMTPSecure = 'tls';
				
				$mail->Host = 'smtp.hostinger.com';
				$mail->Port = 587;
				$mail->isHTML(true);
				//$mail->AddEmbeddedImage('indice.jpeg', 'logo', 'indice.jpeg');
				$mail->Username = 'alta.sistema@jjsolucionesempresariales.com';
				$mail->Password = 'mmy8~K5L';
				$mail->CharSet = 'UTF-8';

				$mail->setFrom('notificaciones@jjsolucionesempresariales.com', 'Restrablecer Contraseña');

				$mail->addAddress($email);
				



				//$headers = "From: notificaciones@jjsolucionesempresariales.com";

				$body='	<div style="text-align: center; background-color:#d2d6de; border: 2px solid; width: 600px; height: 440px;">  
			        <p><img width="300" height="100" src="https://www.testjjsolucionesempresariales.com/images/logito.png"></p>
					
			        <div style="text-align: center; background-color:white;">
					<h1>Estimado Usuario</h1>
					<h2>Recientemente se envió una solicitud para restablecer una contraseña 
						para su cuenta. 
						Si esto fue un error, simplemente ignore  estecorreoelectrónico y no pasará nada.</h2>
					<h3> Este es tu su contraseña temporarl -> '.$password.'</h3>
					</div>
					
					<div style="text-align: center; background-color:#d2d6de;">
						<p><h4>Equipo de Operaciones JJ Gestoría<h4></p>	

						<table style="background:#367fa9; width: 100%;">
					        <tbody>
					            <tr>
					                <td style="text-align:center;"><a moz-do-not-send="true" style="text-decoration:none; color:gray;" href="https://jjsolucionesempresariales.com" title="Web: "><img
					              src="http://i.stack.imgur.com/k55SU.png"/></a> 
					                </td>
					                <td style="text-align:center;"><a moz-do-not-send="true" style="text-decoration:none; color:white;" href="mailto:notificaciones@jjsolucionesempresariales.com" title="Email: notificaciones@jjsolucionesempresariales.com"><img
					              src="http://i.stack.imgur.com/i6A3g.png"/></a> 
					                </td>
					                <td style="text-align:center;"> <a moz-do-not-send="true" style="text-decoration:none; color:white;" href="tel:5558008962" title="Phone: +1 (800) 783-1989"><img
					              src="http://i.stack.imgur.com/Pmjgu.png"/></a> 
					                </td>
					            </tr>
					        </tbody>
					    </table>  

				    	<p><h5>JJ Intelligent Benchmark SA de CV<h5></p>

					</div>
				</div>';

					
				mail($to,$subject,$body,$headers);
				return true;
			}
		} else{
			throw new Exception( FIELDS_MISSING );
		}
	}
	
	/**
	 * Esto generará una contraseña aleatoria
	 * @return string
	 */
	
	private function randomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //recuerde que debe declarar $pass como un array
		$alphaLength = strlen($alphabet) - 1; //poner la longitud -1 en caché
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //convertir el array en una cadena
	}
}