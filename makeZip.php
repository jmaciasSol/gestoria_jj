<?php

include_once "config/config.php";
include "action/encript.php";
require 'composer/vendor/autoload.php';

session_start();
  
  use Aws\S3\S3Client;
  use Aws\Exception\AwsException;

  $idsArray = $_POST['idsArray'];
  $ids = implode(',', $idsArray);


  
  $datos = $con->query("SELECT * FROM tbl_keys WHERE ID = 1");
  $d = $datos->fetch_all(MYSQLI_ASSOC);

  $user_id = $_SESSION['user_id'];
  $datos_user = $con->query("SELECT * FROM tbl_usuarios WHERE ID = $user_id");
  $d_u = $datos_user->fetch_object();

 
  $key = $d[0]['KEY_'];
  $secrectkey = $d[0]['SECRETKEY'];

  $S3_Options = [
    'version' => 'latest',
    'region' => 'us-west-2',
    'credentials' => [

      'key' => encrypt_decrypt('decrypt', $key),
      'secret' => encrypt_decrypt('decrypt', $secrectkey)
    ]
  ];


  $S3 = new S3Client($S3_Options);

  if (!file_exists('storage/folder_zips/Comprobantes')) {
    mkdir('storage/folder_zips/Comprobantes', 0777, true);
  }
  
  $sql_s3 = "SELECT
   CONCAT(CO.NO_CLIENTE,'_' ,CO.VIN,'_' ,CO.NO_PLACAS) FILE_NAME
       , BI.PATH
       , BI.ID
  FROM db_sistema.tbl_bitacora_d BI
  INNER JOIN db_sistema.tbl_contratos CO ON BI.FOLIO = CO.ID
  WHERE BI.TIPO = 1
  AND CO.ID IN($ids)";
  

  $result = mysqli_query($con, $sql_s3);
  $documentos = mysqli_fetch_all($result, MYSQLI_ASSOC);
  
  $date = date("YmdHis");
  
  $zip = new ZipArchive();
  $nombreArchivoZip = "storage/folder_zips/Files_Zip.zip";
 

  if (!$zip->open($nombreArchivoZip, ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
      exit("Error abriendo ZIP en $nombreArchivoZip");
  }

  foreach ($documentos as $documento) {
 
    if($S3->doesObjectExist( 'crfact', $documento['PATH'] )){
  
      $file_name = $documento['FILE_NAME'];
      $file_name_data = explode('_', $file_name);

      $file_path = $documento['PATH'];
      $file_path_data = explode('_', $file_path);


      $newNameFile = $file_name_data[0].'_'.$file_path_data[1].'_'.$file_path_data[2].'_'.$file_path_data[3];


      $getFile = $S3->getObject([
        'Key' => $documento['PATH'],
        'Bucket' => 'crfact',
        'SaveAs' => 'storage/folder_zips/Comprobantes/'.$newNameFile
      ]);

    }else{
      echo 'No Exist';
      $zip->close();
    }

  }


  $dirpath = 'storage/folder_zips/Comprobantes';

  $archivos = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($dirpath),
    RecursiveIteratorIterator::LEAVES_ONLY
  );

  foreach ($archivos as $archivo) {
     
      if ($archivo->isDir()) {
          continue;
      }

      $rutaAbsoluta = $archivo->getRealPath();
    
      $nombreArchivo = substr($rutaAbsoluta, strlen($dirpath) + 1);
    
    $d = (explode("/",$nombreArchivo));
    
    $zip->addFile($rutaAbsoluta, 'Comprobantes/'.$d[(count($d)-1)]);
    
  }

  $zip->addFile('storage/folder_zips/Excel.xlsx', 'Excel.xlsx');
 
  $resultado = $zip->close();

  array_map( 'unlink', array_filter((array) glob("storage/folder_zips/Comprobantes/*") ) );

  echo json_encode(true);
?>