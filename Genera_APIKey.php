<link rel="icon" href="images/JJ.ico">
<?php 
    $active1="active";
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 
    $sql = "SELECT * FROM users";

    if(!empty($_GET)){

        if($_GET["success"]==true){
            echo "
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
            <script>
                swal('Contraseña Restablecida','cierre sesión para comprobar','success');
            </script>";
        }else{
            echo "
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
            <script>
                swal('Error al actualizar','Intente m&aacute;s tarde','error');
            </script>";
        }

    }else{

    }

    
?>
    <div class="content-wrapper">
      <h1>La API KEY te permitirá realizar operaciones, no la compartas, el uso de ella es tu responsabilidad</h1>
      
      <form action="action/makeapikey.php" method="post">
        <div class="container">
          <button type="submit" class="btn btn-success" id="btn-submit" style="font-size: 20px;"><i class="fa fa-cloud-download" aria-hidden="true">
          </i><b>&nbsp; &nbsp;Generar API KEY</b></button>
        </div>
      </form>

    </div>
 
    
<?php include "footer.php"; ?>

