<td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>  >
      <?php

        $ficheros  = scandir('storage/Carpeta_'.$d->referencia);

        $factura="";
        $comprobante="";

        foreach ($ficheros as $fichero) {
          if($fichero == "Factura.pdf"){
            $factura=$fichero;
          }
          if($fichero == "Comprobante.pdf"){
            $comprobante=$fichero;
          }
        }
          
        echo '

        <button class="cotizar btn btn-primary" id="_'.$d->referencia.'" 
        data-toggle="modal" data-target="#myModal'.$d->referencia.'"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>

        <div class="modal fade" id="myModal'.$d->referencia.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                          </button>
                           <h4 class="modal-title" id="myModalLabel"><b>Administración de Documentos</b></h4>
                           <br>
                           <h5>Documentos de: '.$d->nombre_cliente.'</h5>
                      </div>
                      <div class="modal-body">
                        <table style="width:100%">
                          <thead>
                            <th>VIN</th>
                            <th>Tipo</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                          </thead>

                          <tbody>';

                            if(count($ficheros)==3){

                              echo '
                              <tr style="align:center">                            
                              <td>'.$d->vin.'</td>
                               <td>Archivo.pdf&nbsp;<i class="fa fa-file-text-o" aria-hidden="true"></i>
                              </td>
                              <td><a href="storage/Carpeta_'.$d->referencia.'/Comprobante.pdf" download="Comprobante">Comprobante</a></td>
                              <td>'.$d->f_alta.'</td>
                              </tr>';
                              
                            }else if(count($ficheros) > 3){

                              echo '
                              <tr style="align:center">
                              <td>'.$d->vin.'</td>
                               <td>Archivo.pdf&nbsp;<i class="fa fa-file-text-o" aria-hidden="true"></i>
                              </td>

                              <td><a href="storage/Carpeta_'.$d->referencia.'/Factura.pdf" download="Factura">Factura</a></td>
                              
                              <td>'.$d->f_alta.'</td>
                              </tr>

                              <tr style="align:center">
                              
                              <td>'.$d->vin.'</td>
                               <td>Archivo.pdf&nbsp;<i class="fa fa-file-text-o" aria-hidden="true"></i>
                              </td>

                              <td><a href="storage/Carpeta_'.$d->referencia.'/Comprobante.pdf" download="Comprobante">Comprobante</a></td>
                              
                              <td>'.$d->f_alta.'</td>
                              </tr>';
                              
                            }

                            


                            

                            echo'
                          </tbody>

                        </table>

                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      ';

    ?>
    </td>