<link rel="icon" href="images/JJ.ico">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



<?php 
    $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 


     function alert($msg,$val) {

        if($val==1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val==2){

            echo "<script type='text/javascript'>swal('Error Al Agregar','$msg','error');</script>";
        }else if($val==3){
             echo "<script type='text/javascript'>swal('Registro Movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Error Al Mover','$msg','error');</script>";
        }


            
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

        switch ($_GET['success']) {
            case 'true':
                alert("Se han cargado exitosamente",1); 
                break;
            

            case 'false':
                alert("",2);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }

      if(!empty($_GET['move2_success'])){

        switch ($_GET['move2_success']) {
            case 'true':
                alert("A Tabla Rechazados",3); 
                break;
            

            case 'false':
                alert("",4);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }
    }
?>

<div class="content-wrapper"><!-- Content Wrapper. Contains page content --
   <section class="content-header">
  <section class="content-header"> Content Header (Page header) -->
     
        
<?php



include "dbconect.php";
$datos = $con->query("SELECT  
        f_alta,
        n_contrato, 
        referencia, 
        vin, 
        nombre_cliente, 
        estado, 
        cotizacion,
        EJECUTIVO, 
        REGIONAL, 
        AGENCIA,
        edo_factura,
        email_acepta
FROM db_sistema.tbl_revision;");
?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<h1>&nbsp; &nbsp; &nbsp; En proceso de revisi&oacute;n</h1>

<?php if($datos->num_rows>0):?>
  
  <div class="container-fluid" >
  <div class="row" id="tabla">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1"id="myTable"class="table table-bordered table-hover nowrap" style="width:100%" >
  <thead>
        <th style="text-align:center">Fecha Alta</th>
    		<th style="text-align:center">N°contracto</th>
        <th  style="text-align:center">Referencia</th>
        <th  style="text-align:center">vin</th>
        <th  style="text-align:center">Nombre del cliente</th>
        <th style="text-align:center">Entidad</th>
        <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Ver</th>
        <th style="text-align:center">Estado </th> 
        <th  style="text-align:center">Cotizaci&oacute;n </th>
        <th style="text-align:center">Ejecutivo </th> 
        <th  style="text-align:center">Regional </th>
        <th  style="text-align:center">Agencia </th>
        <th  style="text-align:center">Usuario Acepto</th>
        <th  style="text-align:center">Aceptar</th>
        <th  style="text-align:center">Rechazar</th>

  </thead>
  <tbody>
  <?php while($d= $datos->fetch_object()):?>
    <tr align="center">
       <td ><?php echo $d->f_alta; ?></td>
    <td ><?php echo $d->n_contrato; ?></td>
     <td class="<?php echo "vin_".$d->referencia; ?>"><?php echo $d->referencia; ?></td>
       <td class="<?php echo "vin_".$d->referencia; ?>"><?php echo $d->vin; ?></td>
         <td class="<?php echo "vin_".$d->referencia; ?>"><?php echo $d->nombre_cliente; ?></td>
    <td class="<?php echo "entidad_".$d->referencia; ?>"><?php echo $d->edo_factura; ?></td>
     <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>  >
      <?php

        $ficheros  = scandir('storage/Carpeta_'.$d->referencia);

        $factura="";
        $comprobante="";

        foreach ($ficheros as $fichero) {
          if($fichero == "Factura.pdf"){
            $factura=$fichero;
          }
          if($fichero == "Comprobante.pdf"){
            $comprobante=$fichero;
          }
        }
          
        echo '

        <button class="cotizar btn btn-primary" id="_'.$d->referencia.'" 
        data-toggle="modal" data-target="#myModal'.$d->referencia.'"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>

        <div class="modal fade" id="myModal'.$d->referencia.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
              <div class="modal-dialog vertical-align-center">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                          </button>
                           <h4 class="modal-title" id="myModalLabel"><b>Administración de Documentos</b></h4>
                           <br>
                           <h5>Documentos de: '.$d->nombre_cliente.'</h5>
                      </div>
                      <div class="modal-body">
                        <table style="width:100%">
                          <thead>
                            <th>VIN</th>
                            <th>Tipo</th>
                            <th>Nombre</th>
                            <th>Fecha</th>
                          </thead>

                          <tbody>';

                            if(count($ficheros)==3){

                              echo '
                              <tr style="align:center">                            
                              <td>'.$d->vin.'</td>
                               <td>Archivo.pdf&nbsp;<i class="fa fa-file-text-o" aria-hidden="true"></i>
                              </td>
                              <td><a href="storage/Carpeta_'.$d->referencia.'/Comprobante.pdf" download="Comprobante">Comprobante</a></td>
                              <td>'.$d->f_alta.'</td>
                              </tr>';
                              
                            }else if(count($ficheros) > 3){

                              echo '
                              <tr style="align:center">
                              <td>'.$d->vin.'</td>
                               <td>Archivo.pdf&nbsp;<i class="fa fa-file-text-o" aria-hidden="true"></i>
                              </td>

                              <td><a href="storage/Carpeta_'.$d->referencia.'/Factura.pdf" download="Factura">Factura</a></td>
                              
                              <td>'.$d->f_alta.'</td>
                              </tr>

                              <tr style="align:center">
                              
                              <td>'.$d->vin.'</td>
                               <td>Archivo.pdf&nbsp;<i class="fa fa-file-text-o" aria-hidden="true"></i>
                              </td>

                              <td><a href="storage/Carpeta_'.$d->referencia.'/Comprobante.pdf" download="Comprobante">Comprobante</a></td>
                              
                              <td>'.$d->f_alta.'</td>
                              </tr>';
                              
                            }

                            


                            

                            echo'
                          </tbody>

                        </table>

                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      ';

    ?>
    </td>
  


    <td><?php echo $d->estado; ?></td>
    <td ><?php echo $d->cotizacion; ?></td>
    <td><?php echo $d->EJECUTIVO; ?></td>
    <td ><?php echo $d->REGIONAL; ?></td>
    <td ><?php echo $d->AGENCIA; ?></td>
    <td ><?php $correo = explode("@", $d->email_acepta);  echo $correo[0]; ?></td>
       
   
    

   
    
    
   
     <td <?php if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>  >
      <?php
          echo '

      <form action="aceptados.php" method="POST" style="margin-top:15px;">
      <input type="text" name="numero_cliente" value="'.$d->referencia.'" hidden >
      <button class="cotizar btn btn-primary btn-success" id="Guardar_'.$d->referencia.'" 
      ><i class="fa fa-check" aria-hidden="true"></i></button>
    </form>
        ';

    ?>
      </td>

    <td <?php  if($_SESSION["usuario"]=="Admin_JJ"){echo "hidden";} ?>>
      <form action="rechazados.php" method="POST" style="margin-top:15px;">
        <input type="text" name="numero_cliente" value=" <?php echo $d->referencia; ?>" hidden >
      <button class="btn btn-danger"   
      ><i class="fa fa-times" aria-hidden="true"></i></button>
      </form>                
    </td>

    

  <?php endwhile; ?>
  </tbody>
</table>
</div>
</div>
</div>
<?php else:?>
  <h3>No hay Datos</h3>
<?php endif; ?>

</body>



<script>

$(".btn-click-action").click(function() {

  var name= $(this).attr('name');
       
  var identificador = name.split("_");
   
	var valor1= "valor1_"+identificador[1];
	var valor2= "valor2_"+identificador[1];
	var valor3= "valor3_"+identificador[1];
  
  var nombre = $('.nombre_'+identificador[1]).text();
  var entidad = $('.entidad_'+identificador[1]).text();
  var vin = $('.vin_'+identificador[1]).text();

	if($("#"+valor1).val() == "" || $("#"+valor2).val() == "" || $("#"+valor3).val() == ""){
	
	
		swal("Campos Vacios", "Llene los campos de cotizacion", "info");
 	
		//alert("derechos y honorarios no pueden ser vacios");
	
	}else{
	
    
		$.ajax({
		    url: "action/updatelayout.php",
		    type: 'POST',
		   
		    async : true,
		    data:{ 
		    	'valor1' : $("#"+valor1).val(),
          'valor2' : $("#"+valor2).val(),
          'valor3' : $("#"+valor3).val(),
          'nombre' : nombre,	
          'entidad' : entidad,
          'vin' : vin,
		    	'id' : identificador[1]
		     } ,
		    success: function(resp) {

			
			//alert("Datos Actualizados"); 
			swal("Datos Actualizados", "", "success");
			
			
			window.location.reload(true);

		    },
		    error: function(request,err){
			console.log("error")
			console.log(err)
			

		    }
		});
		
	}
	
	//}

});


$(document).ready( function () {

   $("#myTable").DataTable({
    	
    	 "scrollX": true
    
    });
    	
    /*

    $(".cotizar").click(function(){

      var name= $(this).attr('id');
       
      var identificador = name.split("_");

      console.log(identificador);


      document.getElementById("Factura_"+identificador[1]).disabled = false;
      document.getElementById("Comprobante_"+identificador[1]).disabled = false;
      document.getElementById("Guardar_"+identificador[1]).disabled = false;
      document.getElementById("Rechazar_"+identificador[1]).disabled = false;


    });
    */
    	
});
    	
    	
    	





</script>

</html>


























</div> <!-- este div cierra es el del principio -->


<?php include "footer.php"; ?>
