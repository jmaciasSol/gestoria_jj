<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="images/JJ.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>.:Gestoria J&J:.</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">


</head>
<body class="hold-transition register-page">
    <div class="register-box">
        <div id="result"></div>
        <div class="register">
            <a href="#"><b><img width="400" height="200" src="images/logito.png"></b></a>
        </div>
        <div class="register-box-body">
            <p class="login-box-msg">Activar Usuario</p>
            <form method="post" action="action/checkcode.php">
                <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" placeholder="Correo Electrónico" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="codigo" class="form-control" placeholder="Codigo" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <p class="text-muted text-right">campos obligatorios* </p>
                <div class="row">
                    <div class="col-xs-8">
                        
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button id="save_data" type="submit" class="btn btn-primary btn-block btn-flat">Validar</button>
                    </div><!-- /.col -->
                </div>
            </form>

        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <!-- jQuery 2.2.3 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>

</body>
</html>
