<link rel="icon" href="images/JJ.ico">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>  


<?php 
  $active="active"; 
    include "head.php"; 
    include "header.php"; 
    include "aside.php"; 
?>

<div class="content-wrapper"><!-- Content Wrapper. Contains page content --
   <section class="content-header">
  <section class="content-header"> Content Header (Page header) -->
     <section class="content-header">
            <h1></h1>
            <ol class="breadcrumb">
                <li><a href="home.php"><i class="fa fa-dashboard"></i> </a></li>
                <li class="active">Tramites</li>
            </ol>
        </section>


<?php

include_once "config/config.php";
//$datos = $con->query("select * from tbl_aceptados");
$datos= $con->query("SELECT 
	   id, 
       f_alta, 
       n_contrato, 
       referencia, 
       vin, 
       nombre_cliente,
       factura,
       edo_factura,
       estatus, 
       comentarios,
       gestor, 
       DATEDIFF(now(), f_alta) dias
       
FROM db_sistema.tbl_aceptados;");
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<h1>&nbsp; &nbsp; &nbsp; Tr&aacute;mites</h1>
<br><br>
<div id="loader" style="display: none"></div>
<?php if($datos->num_rows>0):?>

	
	
	<div class="container-fluid" >
	<div class="row ">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
	<table border="1" id="myTable" class="table table-bordered table-hover nowrap" style="width:100%" >
	<thead>
		<th style="text-align:center">Fecha Alta</th>
		<th style="text-align:center">N°Contrato</th>
		<th style="text-align:center">Referencia</th>
        <th style="text-align:center">VIN</th>
        <th style="text-align:center">Nombre del cliente</th>
        <th style="text-align:center" hidden>Clave Vehicular</th>
        <th style="text-align:center">Estado</th>
        
        <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">C&oacute;digo</th>
        <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Gestor</th>
        <th hidden style="text-align:center">Gestor</th>
        <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Estatus</th>
       <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Comentarios</th>
       <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Estatus</th>
       <th <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Comentarios</th>
       <th style="text-align:center">Dias</th>
       <th <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?> style="text-align:center">Guardar</th>
                
                
                	
	</thead>
	<tbody>
	<?php 
			
	while($d= $datos->fetch_object()):?>
		<tr align="center">
		<td><?php echo $d->f_alta; ?></td>
		<td><?php echo $d->n_contrato; ?></td>
		<td><?php echo $d->referencia; ?></td>
		<td><?php echo $d->vin; ?></td>
		<td><?php echo $d->nombre_cliente; ?></td>
		<td hidden><?php echo $d->clave_vehiculo; ?></td>
		<td><?php echo $d->factura; ?></td>
		<td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>
		  
		    <select  class="btn-iso-action" id="Iso_<?php echo $d->id; ?>">

			    	<option value="1_001">1_001</option>
		            <option value="1_002">1_002</option>       
		            <option value="2_003">2_003</option>
		                   
		    </select>   
		</td>
		<td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>


			<select id="Gestor_<?php echo $d->id; ?>" class="gestores" data-width="55px">
                <option value="" disabled selected>Selecione una opcion</option>
             </select>
                
        </td>
        <td hidden><?php echo $d->gestor; ?></td>
		
		
        <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>
          
            <select  class="select-change-action" id="Estatus_<?php echo $d->id; ?>">
	            <option value="" disabled selected>Sin Asignar</option>             
                    <option value="2" <?php if($d->estatus == 2){echo "selected";} ?> >En curso</option>        
                    <option value="3" <?php if($d->estatus == 3){echo "selected";} ?> >Finalizado</option>       
                    <option value="4" <?php if($d->estatus == 4){echo "selected";} ?> >Entregado</option>  
            </select>   
        </td>



        <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>
            <input type="text" id="Comentario_<?php echo $d->id; ?>" value="<?php echo $d->comentarios ?>" class="form-control">
        </td>

         <td <?php 

        	if($d->estatus== 0 || $d->estatus== 1 ){

        		$var="Sin Asignar";
        	}else if($d->estatus == 2){

        		$var="En Curso";
        	}

        	else{
        		$var="Finalizado";
        	}


        	if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> ><?php echo $var ?>
        		
        </td>

        <td <?php if($_SESSION["usuario"] == "Admin_JJ"){echo "hidden";} ?> ><?php echo $d->comentarios ?></td>




        <td><?php echo $d->dias; ?></td>

         <td <?php if($_SESSION["usuario"] != "Admin_JJ"){echo "hidden";} ?>>
        <?php  echo '
           
                       
                            <input type="button" class="btn-click-action
                              " id="button_'.$d->id.'" name="button_'.$d->id.'"
                                 value="Guardar" />

                        
                        ';   
        ?>
        </td>
		</tr>
	

	<?php  endwhile; ?>
	</tbody>
</table>
</div>
</div>
</div>
<?php else:?>
	<h3>No hay Datos</h3>
<?php endif; ?>

</body>






<script>

$( ".btn-iso-action" ).change(function() {


	var iso =$(this).val();
	var name= this.id;
	var identificador = name.split("_");
	
	
//	$('#Gestor_'+identificador[1]);
	
	
	/*
	console.log(iso);
	console.log(name);	
	console.log(identificador);
	console.log(identificador[1]);
	*/
	
//alert($(this).val());

	
		

	$.ajax({
		    url: "action/get_gestores.php",
		    type: 'POST',
		    dataType :'json',
		    async : true,
		    data:{ 
		    	'iso' : iso	
		     } ,
		    success: function(resp) {

			
			console.log(resp);

			var datos = Object.values(resp);
				  
				  
		        var html = '';
		        var i;
		        for(i=1; i< datos.length; i++){ 
		            html += '<option value="'+datos[i]+'">'+datos[i]+'</option>';
		        }
		        $('#Gestor_'+identificador[1]).html(html);

			//console.log(html);
			//console.log(datos);
			

		       // var estados = document.getElementById("estados");
		       // estados.value=resp[0].idestado;

		       			
			//alert("Datos Actualizados"); 
			
			//window.location.reload(true);

		    },
		    error: function(request,err){
			console.log("error")
			console.log(err)
			

		    }
		});
		
		
		
	
		
	
  
});







$('.select-change-action').on('change', function() {
   
   //alert($(this).val());
   
   
   if($(this).val() ==4){
   
   	//alert("Cambiar de Tabla");
   
   }else if($(this).val() ==2){
   
   
    var name= this.id;
   	
	var identificador = name.split("_");
    	var Gestor= "Gestor_"+identificador[1];
	var Iso= "Iso_"+identificador[1];

	
	
	//console.log($("#"+Gestor).val());

	/*
	$.ajax({
	    url: "action/pagar.php",
	    type: 'POST',
	     dataType :'json',
	    async : true,
	    data:{ 
	    	'Gestor' : $("#"+Gestor).val(),
	    	'Iso' : $("#"+Iso).val(),
	    	'id' : identificador[1]
	     } ,
	    success: function(resp) {
	    
	    
	    console.log(resp);

		//alert("Se ha pagado"); 
		
		//window.location.reload(true);

	    },
	    error: function(request,err){
		console.log("error")
		console.log(err)
		

	    }
	});
	*/
   
   
   }else{
   
   	
   	
   	var name= this.id;
	
	var identificador = name.split("_");
   
   	
   	/*
   	$.ajax({
	    url: "action/cambiarGestoria.php",
	    type: 'POST',
	   
	    async : true,
	    data:{ 
	    
	    	'id' : identificador[1]
	     } ,
	    success: function(resp) {

		alert("Se ha movido el registro"); 
		
		window.location.reload(true);

	    },
	    error: function(request,err){
		console.log("error")
		console.log(err)
		

	    }
	}); 
	
	*/
	
	
	
	
	
   	
   	
   
   }
   
});





    
$(document).ready( function () {
    
    $("#myTable").DataTable({
    	
    	//responsive: true
    
    	"scrollX": true

    });
    	
    /*

    function(){

 		$.ajax({
			    url: "action/get.php",
			    type: 'POST',
			   
			    async : true,
			    data:{ 
			    	'gestor' : $("#"+Gestor).val(),	
			    	'estatus' : $("#"+Estatus).val(),
			    	'comentario' : $("#"+Comentario).val(),
			    	'iso' : $("#"+iso).val(),
			    	'id' : identificador[1]
			     } ,
			    success: function(resp) {

				
				alert("Datos Actualizados"); 
				
				window.location.reload(true);

			    },
			    error: function(request,err){
				console.log("error")
				console.log(err)
				

			    }
			});
		
    }

    */
    	
    	
    $(".dataTable").on('click','.btn-click-action',function(){
    	 
    	var name= $(this).attr('name');
    	 
	var identificador = name.split("_");
	
	var Gestor= "Gestor_"+identificador[1];
	var Estatus= "Estatus_"+identificador[1];
	var iso= "Iso_"+identificador[1];
	var Comentario= "Comentario_"+identificador[1];
	
		
	if($("#"+Gestor).val() == null || $("#"+Estatus).val()== null || $("#"+iso).val() == null ){
	
		alert("Estatus y Gestor e Iso no pueden ser vacios");
	
	}
	
	
	
	
	else{
	
	
		if($("#"+Estatus).val() == 2){
		
		
			$.ajax({
			    url: "action/pagar.php",
			    type: 'POST',
			     dataType :'json',
			    async : true,
			    data:{ 
			    	'Gestor' : $("#"+Gestor).val(),
			    	'Iso' : $("#"+iso).val(),
			    	'id' : identificador[1]
			     } ,
			    success: function(resp) {
			    
			    
			    console.log(resp);

				//alert("Se ha pagado"); 
				
				//window.location.reload(true);

			    },
			    error: function(request,err){
				console.log("error")
				console.log(err)
				

			    }
			});
		
		
			
			$.ajax({
			    url: "action/editAceptados.php",
			    type: 'POST',
			   
			    async : true,
			    data:{ 
			    	'gestor' : $("#"+Gestor).val(),	
			    	'estatus' : $("#"+Estatus).val(),
			    	'comentario' : $("#"+Comentario).val(),
			    	'iso' : $("#"+iso).val(),
			    	'id' : identificador[1]
			     } ,
			    success: function(resp) {

				
				alert("Datos Actualizados"); 
				
				window.location.reload(true);

			    },
			    error: function(request,err){
				console.log("error")
				console.log(err)
				

			    }
			});
		
		
		
		}else{
		
		
			$.ajax({
			    url: "action/cambiarGestoria.php",
			    type: 'POST',
			   
			    async : true,
			    data:{ 
			    
			    	'id' : identificador[1]
			     } ,
			    success: function(resp) {

				alert("Se ha movido el registro"); 
				
				window.location.reload(true);

			    },
			    error: function(request,err){
				console.log("error")
				console.log(err)
				

			    }
			}); 
				
		
		
		
		}
		
		
		
		
		
	}
	
	
	/*
	$.ajax({
	    url: "action/cambiarGestoria.php",
	    type: 'POST',
	   
	    async : true,
	    data:{ 
	    
	    	'id' : identificador[1]
	     } ,
	    success: function(resp) {

		//alert("Se ha movido el registro"); 
		
		//window.location.reload(true);

	    },
	    error: function(request,err){
		console.log("error")
		console.log(err)
		

	    }
	});
	
	
	
	
	
	*/

	
    	 
    	
    
    });
    	
    	
    	
} );

</script>
</html>


</div> <!-- este div cierra es el del principio -->


<?php include "footer.php"; ?>





