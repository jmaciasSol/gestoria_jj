<?php 

  require 'PHPExcel/Classes/PHPExcel.php';
  require 'action/encript.php';

  function makeExcelFile($mydata){

      $objPHPExcel = new PHPExcel();

      $objPHPExcel->getProperties()
      ->setCreator('Aldo Rodrigo')
      ->setTitle('Example Excel with PHP')
      ->setDescription('Example Document')
      ->setKeywords('excel phpexcel php')
      ->setCategory('Examples');

      $objPHPExcel->setActiveSheetindex(0);
      $objPHPExcel->getActiveSheet()->setTitle('First Sheet');
      $from = "A1"; // or any value
      $to = "N1"; // or any value
      $objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
      $objPHPExcel->getActiveSheet()
            ->getStyle("$from:$to")
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('FFFF00');


        $objPHPExcel->getActiveSheet()->setCellValue('A'.(1), 'FECHA_SOLICITUD');
        $objPHPExcel->getActiveSheet()->setCellValue('B'.(1), 'NO_CLIENTE');
        $objPHPExcel->getActiveSheet()->setCellValue('C'.(1), 'VIN');
        $objPHPExcel->getActiveSheet()->setCellValue('D'.(1), 'NOMBRE');
        $objPHPExcel->getActiveSheet()->setCellValue('E'.(1), 'EDO_ORIGEN');
        $objPHPExcel->getActiveSheet()->setCellValue('F'.(1), 'EDO_EMPLACAMIENTO');
        $objPHPExcel->getActiveSheet()->setCellValue('G'.(1), 'MTO_DERECHOS');
        $objPHPExcel->getActiveSheet()->setCellValue('H'.(1), 'MTO_TENENCIA');
        $objPHPExcel->getActiveSheet()->setCellValue('I'.(1), 'MTO_TARJETA');
        $objPHPExcel->getActiveSheet()->setCellValue('J'.(1), 'MTO_OTRO');
        $objPHPExcel->getActiveSheet()->setCellValue('K'.(1), 'MTO_TOTAL');
        $objPHPExcel->getActiveSheet()->setCellValue('L'.(1), 'HONORARIOS');
        $objPHPExcel->getActiveSheet()->setCellValue('M'.(1), 'MENSAJERIA');
        $objPHPExcel->getActiveSheet()->setCellValue('N'.(1), 'FECHA_FINALIZADO');
        
        

          $cont = 0;
          foreach ($mydata as $row ) {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.($cont+2), $row['FECHA_SOLICITUD']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($cont+2), $row['NO_CLIENTE']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($cont+2), $row['VIN']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.($cont+2), encrypt_decrypt('decrypt', $row['NOMBRE']));
            $objPHPExcel->getActiveSheet()->setCellValue('E'.($cont+2), $row['EDO_ORIGEN']);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.($cont+2), $row['EDO_EMPLACAMIENTO']);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($cont+2), $row['MTO_DERECHOS']);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($cont+2), $row['MTO_TENENCIA']);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.($cont+2), $row['MTO_TARJETA']);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.($cont+2), $row['MTO_OTRO']);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.($cont+2), $row['MTO_TOTAL']);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.($cont+2), $row['HONORARIOS']);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.($cont+2), $row['MENSAJERIA']);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.($cont+2), $row['FECHA_FINALIZADO']);
            
            $cont++;
          }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save(str_replace(__FILE__,'storage/folder_zips/Excel.xlsx',__FILE__));

  }


?>