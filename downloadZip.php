<?php

    $directorio = 'storage/folder_zips/';
    $filename = date("YmdHis");
    $fichero = scandir($directorio);

    foreach ($fichero as $file ) {
      if(is_file ( $directorio.'/'.$file )){
        rename($directorio.'/'.$file, $directorio.'/'.$filename.'.zip' );
      }
    }
    
    if($_POST['download'] == 1){
       $file = 'storage/folder_zips/'.$filename.'.zip';

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$filename.".zip");
        header("Content-Transfer-Encoding: binary");
        header("Content-Type: binary/octet-stream");
        readfile($file);

    }else{
      redirect('Pagos?error=true');
    }
    

?>
