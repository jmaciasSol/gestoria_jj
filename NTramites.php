<link rel="icon" href="images/JJ.ico">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js" ></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script> 
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



<?php 
    $active="active"; 
   include "head.php";
    include "header.php"; 
    include "aside.php"; 

    $_SESSION['tiempo'] = time();

 

     function alert($msg,$val) {

        if($val==1){
            echo "<script type='text/javascript'>swal('Datos Cargados','$msg','success');</script>";
        }else if($val==2){

            echo "<script type='text/javascript'>swal('Error Al Agregar','$msg','error');</script>";
        }else if($val==3){
             echo "<script type='text/javascript'>swal('Registro Movido','$msg','success');</script>";
        }else{
          echo "<script type='text/javascript'>swal('Error Al Mover','$msg','error');</script>";
        }


            
    }

    if(empty($_GET)){
        
    }else{

        if(!empty($_GET['success'])){

        switch ($_GET['success']) {
            case 'true':
                alert("Se han cargado exitosamente",1); 
                break;
            

            case 'false':
                alert("",2);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }

      if(!empty($_GET['move2_success'])){

        switch ($_GET['move2_success']) {
            case 'true':
                alert("A Tabla Rechazados",3); 
                break;
            

            case 'false':
                alert("",4);
                break;

            default:
                //alert("Dimensión Max: 1280 * 720 / Tamaño Max: 1 MB",0);
                break;
        }
      }
    }
?>

<div class="content-wrapper"><!-- Content Wrapper. Contains page content --
   <section class="content-header">
  <section class="content-header"> Content Header (Page header) -->
     
        
<?php
  include_once "action/encript.php";
  include_once "config/config.php";
  date_default_timezone_set ('America/Mexico_City');
  $datos = $con->query("SELECT * FROM v_tramites");
?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
<h1>&nbsp; &nbsp; &nbsp; TR&Aacute;MITES</h1>

<?php if($datos->num_rows>0): ?>
   <div id="loader" style="display: none"></div>
  <div class="container-fluid" >
  <div class="row" id="tabla" style=" overflow-x: auto;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
  <table border="1" id="myTable" class="table table-bordered table-hover nowrap" style="width:100%;" >
  <thead>
    <th class="centrar">NO CLIENTE</th>
    <th class="centrar">FECHA SOLICITUD</th>
    <th class="centrar">VIN</th>
    <th class="centrar" >NOMBRE</th>

    <th class="centrar">TRAMITE</th>
    <th class="centrar">EDO PLACAS</th>
    <th class="centrar">FECHA</th>
    <th class="centrar">DIAS</th>

    <th class="centrar">COTIZACIÓN</th>
    <th class="centrar">DOCUMENTOS</th>    
    <th class="centrar">GESTOR</th>
    <th class="centrar">ESTATUS</th> 
    <th class="centrar">COMENTARIOS </th>
    
    <th class="centrar">GUARDAR</th>
    <th class="centrar">DOCUMENTOS</th>
    <th class="centrar">EVENTOS</th>

  </thead>
  <tbody>
  <?php while($d= $datos->fetch_object()):?>
    
    <tr align="center">

      <td ><?php 
        echo 
        '<a data-toggle="modal" href="#Modal_datos_'.$d->NO_CLIENTE.'"><i class="fa fa-plus-circle  icon" aria-hidden="true"></i></a>
        <div class="modal fade" id="Modal_datos_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                            </button>
                             <h3 class="modal-title" id="myModalLabel"><b>'.encrypt_decrypt('decrypt',$d->NOMBRE).'</b></h3>

                        </div>
                        <div class="modal-body" style="text-align:left;">
                         <ul>
                          <li>FOLIO: '.$d->FOLIO.'</li>
                          <li>NO_PLACAS: '.$d->NO_PLACAS.'</li>
                          <li>TIPO: '.$d->TIPO.'</li>
                          <li>AUTO: '.$d->MARCA.'</li>
                          <li>VALOR FACTURA: $'.number_format($d->VALOR_FACTURA,2).'</li>                          
                          <li>MTO TOTAL: $'.number_format($d->MTO_TOTAL,2).'</li>
                         </ul>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';?>
      <?php echo "&nbsp".$d->NO_CLIENTE; ?> 
      </td>
      <td ><?php echo $d->FECHA_SOLICITUD; ?></td>
      <td id=""><?php echo $d->VIN; ?></td>
      <td ><?php echo encrypt_decrypt('decrypt',$d->NOMBRE); ?></td>

      <td ><?php echo $d->TRAMITE; ?></td>
      <td ><?php echo $d->EDO_PLACAS; ?></td>
      <td ><?php echo $d->FECHA; ?></td>
      <td ><?php echo $d->DIAS; ?></td>
      </td>

      <input type="text" value="<?php echo $d->NO_PLACAS; ?>"  id="<?php echo 'placas_'.$d->NO_CLIENTE; ?>" hidden>
      <input type="text" value="<?php echo $d->VIN; ?>"  id="<?php echo 'vin_'.$d->NO_CLIENTE; ?>" hidden >

      <td>
        <?php echo '
        <a class="llamar-popup-cot '.encrypt_decrypt('decrypt',$d->NOMBRE).'" id="'.$d->ID.'" data-toggle="modal" data-target="#Modal-Cotizacion">Actualiza</a>
        <input type="text" id="ID_'.$d->NO_CLIENTE.'" value="'.$d->ID.'" hidden />
        '; ?>
        
      </td>
      <td>
        <?php echo '
           <a id="documentos'.$d->ID.'" data-toggle="modal" data-target="#Modal-Documentos'.$d->ID.'">Carga</a>
          '; 
        ?>
        <div class="modal fade" id="Modal-Documentos<?php echo $d->ID; ?>" tabindex="-1" role="dialog"  
          aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
              <div class="modal-content" style=" min-width:600px !important;">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Cerrar</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel"><b>CARGAR DOCUMENTOS</b></h4>
                </div>
                <div class="container text-left">
                  <label class="modal-title"></label>
                </div>
                <div class="modal-body">
                  <table id="modal-table" border="1" class="table-bordered table-hover" style="width:100%">
                    <thead>
                      <th class="cabecera_popup" >DOCUMENTO</th>
                      <th class="cabecera_popup" >TIPO</th>
                      <th class="cabecera_popup" ></th>                   
                    </thead>
                    <tbody>
                      <tr class="centrar">
                        <td>LINEA CAPTURA</td>
                        <td>PDF</td>
                        <td>
                          <label class="custom-file-upload ">
                            <input id="<?php echo 'LineaCaptura_'.$d->NO_CLIENTE; ?>" type="file" class="linea" accept="application/pdf">
                            <i class="fa fa-cloud-upload"></i>
                          </label>
                        </td>
                      </tr>
                      <tr class="centrar">
                        <td>COMPROBANTE</td>
                        <td>PDF</td>
                        <td>
                          <label class="custom-file-upload ">
                            <input id="<?php echo 'Comprobante_'.$d->NO_CLIENTE; ?>" type="file" class="comprobante" accept="application/pdf" >
                            <i class="fa fa-cloud-upload"></i>
                          </label>
                        </td>
                      </tr>
                      <tr class="centrar">
                        <td>TARJETA</td>
                        <td>PDF</td>
                        <td>
                          <label class="custom-file-upload ">
                            <input id="<?php echo 'Tarjeta_'.$d->NO_CLIENTE; ?>" type="file" class="tarjeta" accept="application/pdf">
                            <i class="fa fa-cloud-upload"></i>
                          </label>
                        </td>
                      </tr>
                     
                      <tr class="centrar" <?php if($d->TIPO == 'NUEVO'){ echo 'hidden'; } ?>>
                        <td>FOTO</td>
                        <td>IMAGE*</td>
                        <td>
                          <label class="custom-file-upload ">
                            <input id="<?php echo 'Foto_'.$d->NO_CLIENTE; ?>" type="file" class="foto" accept="image/x-png,image/gif,image/jpeg">
                            <i class="fa fa-cloud-upload"></i>
                          </label>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </td>
  
   
      <td>
        <?php 
          $querygestores = "SELECT G.CODIGO, G.NOMBRE FROM tbl_gestores G
            WHERE G.ACTIVO = 1 AND G.ISO = '$d->EDO_PLACAS'";

          $result = mysqli_query($con, $querygestores);
          $data_gestores = mysqli_fetch_all($result, MYSQLI_ASSOC);
        ?>

        <select class='gestores' id="gestores_<?php echo $d->ID; ?>">
          <option selected disabled>Seleccione un Gestor</option>
          <?php  

            $selected = '';
            $codigo = '';
            $cod_g = '';

            foreach ($data_gestores as $dg) {
              if($dg['NOMBRE'] == $d->GESTOR){
                $selected = ' selected';
                 $cod_g = $dg['CODIGO'];
              }else{
                $selected = '';
              }

               $codigo = $dg['CODIGO'];

              echo "<option value=".$dg['CODIGO'].$selected.">".$dg['NOMBRE']."</option>";
            }
          ?>
          
        </select>
      </td>

      <td><?php 

        $desabilitar1 = '';
        $desabilitar2 = '';
        $desabilitar3 = '';
        $desabilitar4 = '';
       


        switch ($d->ESTATUS) {
          case 'ASIGNADO':
            $desabilitar1 = ' disabled';
            $desabilitar2 = '';
            $desabilitar3 = '';
            $desabilitar4 = '';
            
            break;

          case 'INCOMPLETO':
            $desabilitar1 = ' disabled';
            $desabilitar2 = ' disabled';
            $desabilitar3 = '';
            $desabilitar4 = '';
            
          break;
          
          case 'EN CURSO':
            $desabilitar1 = ' disabled';
            $desabilitar2 = ' disabled';
            $desabilitar3 = ' disabled';
            $desabilitar4 = '';
            
          break;

          case 'COMPLETO':
            $desabilitar1 = ' disabled';
            $desabilitar2 = ' disabled';
            $desabilitar3 = ' disabled';
            $desabilitar4 = ' disabled';
            
          break;
          
        
        }

        echo'
          <select name="estatus_'.$d->NO_CLIENTE.'" id="estatus_'.$d->ID.'" class="estatus">
            <option value=30' ;if($d->ESTATUS == 'PENDIENTE'){ echo ' selected ';} echo ' disabled >'.'PENDIENTE'.'</option>
            <option value=35' ;if($d->ESTATUS == 'ASIGNADO'){ echo ' selected '; } echo $desabilitar1.' >'.'ASIGNADO'.'</option>
            <option value=33' ;if($d->ESTATUS == 'INCOMPLETO') {echo ' selected ';} echo $desabilitar2.' >'.'INCOMPLETO'.'</option>
            <option value=34' ;if($d->ESTATUS == 'EN CURSO') {echo ' selected ';} echo $desabilitar3.' >'.'EN CURSO'.'</option>
            <option value=36' ;if($d->ESTATUS == 'COMPLETO') {echo ' selected '; } echo $desabilitar4.' >'.'COMPLETO'.'</option>
            <option value=47';if($d->ESTATUS == 'ENTREGADO') {echo ' selected ';} echo ' >'.'ENTREGADO'.'</option>
          </select>';  
          ?>
      </td>  
      <td><?php echo'
        <a data-toggle="modal" href="#Modal_comentarios_'.$d->NO_CLIENTE.'">Nuevo</a>
        <div class="modal fade" id="Modal_comentarios_'.$d->NO_CLIENTE.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h4>COMENTARIOS </h4>
                        </div>
                        <div class="modal-body" style="text-align:left;">
                          <textarea class="comentarios" id="textarea_'.$d->ID.'" style="width:100%;"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        ?>
      </td>
      <td>
        <?php
        echo '
          <button type="button" class="cotizar btn btn-primary btn-success vert-aligned" id="Guardar_'.$d->ID.'" 
            onclick="popup_entregado('.$d->ID.')"><i class="fa fa-save" aria-hidden="true"></i></button>
          <form id="Form_Guardar'.$d->ID.'" action="changeStatus_tramites.php" method="POST" style="margin-top:15px;">
            <input type="text" name="ID" value="'.$d->ID.'" hidden >
            <input type="text" id="value_estatus_'.$d->ID.'" name="value_estatus" value="'.$d->ESTATUS.'"  hidden >
            <input type="text" id="comentario_'.$d->ID.'" name="comentario" hidden >
            <input type="text" id="codigogestor_'.$d->ID.'" name="codigo_gestor" value = "'.$cod_g.'" hidden>
          </form>
        ';
        ?>
      </td>
      <td>
        <?php
         echo 
          '<a class="popup-documentos" id="'.$d->ID.'_'.$d->NO_CLIENTE.'" data-toggle="modal" data-target="#Modal-Documentos">Ver</a>
          ';
        ?>
      </td>
      <td>
        <?php 
          echo '
          <a class="llamar-popup" id="'.$d->ID.'" data-toggle="modal" data-target="#Modal-Bitacora">Ver</a>
            '; 
        ?>
      </td>
         <div hidden>
        <input id="vin_<?php echo $d->ID; ?>" type="text" name="vin" value="<?php echo $d->VIN; ?>">
        <input id="placas_<?php echo $d->ID; ?>" type="" name="placas" value="<?php echo $d->NO_PLACAS; ?>">
      </div>
    </tr>

<div class="modal fade modal-xl" id="Modal-Entregado_<?php echo $d->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
          </button>
          <h4 class="modal-title" id="myModalLabel"><b>INGRESE LOS DATOS</b></h4>
        </div>
        <div class="container text-left" >
            <label class="modal-title"></label>
        </div>
        <div class="modal-body">
          <?php 
              $querymensajeros = "SELECT M.CODIGO, M.NOMBRE FROM tbl_mensajeros M WHERE M.ACTIVO = 1";

              $result = mysqli_query($con, $querymensajeros);
              $data_mensajeros = mysqli_fetch_all($result, MYSQLI_ASSOC);

          ?>
          <form>
           <div class="row">
              <div class="col-xs-6">
                <label for="entregadoPor">Entregado por:</label>
                 <select class="form-control" id="entregadoPor<?php echo $d->ID; ?>">
                   <option selected disabled>Seleccione un Mensajero</option>
                  <?php 
                    foreach ($data_mensajeros as $mensajero) {

                        echo "<option value=".$mensajero['CODIGO'].">".$mensajero['NOMBRE']."</option>";
                      }  
                  ?>
                </select>
              </div>
              <div class="col-xs-6">
                <label for="entregadoA">Entregado a:</label>
                <input type="text" class="form-control" id="entregadoA<?php echo $d->ID;  ?>" aria-describedby="entregadoA" placeholder="Ingrese el destinatario">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6">
                <label for="montoAPagar">Monto a pagar:</label>
                <input type="text" class="form-control" id="montoAPagar<?php echo $d->ID;  ?>" value="200" aria-describedby="montoAPagar">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <label for="comentarios">Comentarios:</label>
                <input type="text" class="form-control" id="comentarios_popup<?php echo $d->ID;  ?>" aria-describedby="comentarios" placeholder="Agrega Comentarios">
              </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                <label for="comentarios">Evidencia:</label>
                <input type="file" multiple="multiple" class="form-control" id="evidencia_popup<?php echo $d->ID;  ?>" accept="application/pdf" style="display:block;">
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                 <button type="button" class="btn btn-primary pull-right" onclick="validarDatos(<?php 
                   $n = encrypt_decrypt('decrypt',$d->NOMBRE);
                                            echo $d->ID.",'".$n."',".
                                            $d->NO_CLIENTE;

                  ?>);">Aceptar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-xl" id="Modal-Incompleto_<?php echo $d->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="vertical-alignment-helper">
                      <div class="modal-dialog vertical-align-center">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel"><b>MOTIVO INCOMPLETO</b></h4>
                          </div>
                          <div class="container text-left" >
                              <label class="modal-title"></label>
                          </div>
                          <div class="modal-body">
                              <?php 
                                  $query = "SELECT ID, MOTIVO FROM DB_CATALOGOS.TBL_MOTIVOS_INCOMPLETO
                                  WHERE ACTIVO = 1;";

                                  $result = mysqli_query($con, $query);
                                  $data_files = mysqli_fetch_all($result, MYSQLI_ASSOC);

                              ?>
                            <form>
                             <div class="row">
                                <div class="col-xs-6">
                                  <label for="entregadoA">Entregado a:</label>
                                  <select class="form-control" id="codigoFiles<?php echo $d->ID; ?>">
                                    <option selected disabled>Seleccione una opcion</option>
                                    <?php 
                                      foreach ($data_files as $df) {

                                          echo "<option value=".$df['ID'].">".$df['MOTIVO']."</option>";
                                        }  
                                    ?>
                                  </select>
                                </div>
                              </div>
                              <div class="row">
                              <div class="col-xs-12">
                                <label for="comentarios">Comentarios:</label>
                                <input type="text" class="form-control" id="comentarios_popup_incompletos<?php echo $d->ID;  ?>" aria-describedby="comentarios" placeholder="Agrega Comentarios">
                              </div>
                            </div>
                            <br>
                              <div class="row">
                                <div class="col-xs-12">
                                  <button type="button" class="btn btn-primary pull-right" onclick="sentIncompletos(
                                        <?php 
                                            $n = encrypt_decrypt('decrypt',$d->NOMBRE);
                                            echo $d->ID.','.$d->NO_CLIENTE.",'".$n."'"
                                        ?>
                                      );"

                                    >
                                      Aceptar
                                  </button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


  <?php endwhile; ?>


  </tbody>
</table>
</div>
</div>
</div>
<?php else:?>
  <h3>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; No hay Datos</h3>
<?php endif; ?>

<div class="modal fade modal-xl" id="Modal-Bitacora" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="vertical-alignment-helper">
    <div class="modal-dialog vertical-align-center">
      <div class="modal-content" style=" min-width:1000px !important;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title" id="myModalLabel"><b>Bitacora</b></h4>
        </div>
        <div class="container text-left" >
            <label class="modal-title" id="label_bitacora"></label>
        </div>
        <div class="modal-body " style="overflow-y: auto;">
          <div class="outer" style=" min-height: 200px !important;">
            <table border="1" id="bitacora" >
                <thead>
                    <tr>
                    <th class="cabecera_popup">NO CLIENTE</th>
                    <th class="cabecera_popup">FECHA</th>
                    <th class="cabecera_popup">ROL</th>
                    <th class="cabecera_popup">USUARIO</th>
                    <th class="cabecera_popup">PROCESO</th>
                    <th class="cabecera_popup">ESTATUS</th>
                    <th class="cabecera_popup">MOTIVO INCOMPLETO</th>
                    <th class="cabecera_popup">COMENTARIO</th>
                    </tr>
                </thead>
                <tbody id="respuesta_bitacora">
                </tbody>
            </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade modal-xl" id="Modal-Documentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="vertical-alignment-helper">
      <div class="modal-dialog vertical-align-center">
          <div class="modal-content" style=" min-width:1100px !important; ">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">
                      <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel"><b>Documentos</b></h4>
              </div>
              <div class="container text-left" >
                  <label class="modal-title" id="label_bitacora"></label>
              </div>
              <div class="modal-body" style="overflow-y: auto;">
                  <div class="outer" style=" min-height: 200px !important;">
                  <table border="1" id="bitacora">
                      <thead>
                          <tr>
                          <th class="cabecera_popup" style="width: 100px;" >FECHA</th>
                          <th class="cabecera_popup" style="width: auto;">DOCUMENTO</th>
                          <th class="cabecera_popup" style="width: 100px;">...</th>
                          <th class="cabecera_popup" style="width: 200;">Opciones</th>
                          <th class="cabecera_popup" style="width: 100px;">Guardar</th>

                          </tr>
                      </thead>
                      <tbody id="documentos">
                      </tbody>
                  </table>
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
          </div>
      </div>
  </div>
</div>

<form id="formDescargarDocumento" method="post" action="download.php" hidden>
  <input id="keyvalue" type="text" name="key">
</form>

<form id="formVerDocumento" method="post" action="seeFile.php" hidden>
  <input id="keyvalue" type="text" name="key">
</form>

<div id="dialog" style="display: none;">
      <div>
          <iframe id="frame"></iframe>
      </div>
</div>


</body>
<div class="modal fade" id="Modal-Cotizacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">
        <div class="vertical-alignment-helper">
          <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style=" min-width:1300px !important;">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only">Cerrar</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><b id="texto-modal"></b></h4>

              </div>
              <div class="container text-left">
                <label class="modal-title" id="label_' . $d->NO_CLIENTE . '"></label>
              </div>
              <div class="modal-body">
                <table id="modal-table" border="1" class="table-bordered table-hover" style="width:100%">
                  <thead>
                    <th class="cabecera_popup" style="width: 100px;" >COTIZACI&Oacute;N</th>
                    <th class="cabecera_popup" >ESTADO</th>
                    <th class="cabecera_popup" >TIPO</th>
                    <th class="cabecera_popup" >TRAMITE</th>
                    <th class="cabecera_popup" >SLA</th>
                    <th class="cabecera_popup" >AVG</th>

                    <th class="cabecera_popup" >DERECHOS</th>
                    <th class="cabecera_popup" >TENENCIA</th>
                    <th class="cabecera_popup" >TARJETA</th>
                    <th class="cabecera_popup" >OTRO</th>
                    <th class="cabecera_popup" >TOTAL</th>
                    <th class="cabecera_popup" >ESTATUS</th>
                    <th class="cabecera_popup" >ACTUALIZAR</th>
                  </thead>
                  <tbody id="respuesta_cotizacion">
                  </tbody>

                </table>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>

<script>

var gno_cliente;


$(document).ready( function () {

  var table = $("#myTable").DataTable({
      
  });

  table.columns.adjust().draw();
        
});

function validarDatos(id, nombre, no_cliente){

  console.log(id);

  let entregadoPor = $("#entregadoPor"+id).val();
  let entregadoA = $("#entregadoA"+id).val();
  let montoAPagar = $("#montoAPagar"+id).val();
  let comentarios = $("#comentarios_popup"+id).val();
  let codigo_gestor = $("#codigogestor_"+id).val();
  let tipo_unidad = $("#tipo_unidad" + id).val();

  //gno_cliente
  let vin = $("#vin_" + no_cliente).val();
  let placas = $("#placas_" + no_cliente).val();

  /*var files = $("#my-input")[0].files;
  
  for (var i = 0; i < evidencia.length; i++) {
    archivos.append('file_'+(i+1), files[i]);
  }
  */
 
  let files = $('#evidencia_popup'+id)[0].files;

  let archivos = new FormData();

  Object.keys(files).forEach(function(key) {
  console.log(key, files[key]);

  archivos.append('file'+ (parseInt(key)+1), files[key]);
  });

  archivos.append('tipo_unidad', tipo_unidad);
  archivos.append('id', id);

  archivos.append('archivo', 'Evidencia');
   
  console.log("tipo unidad");
  console.log(tipo_unidad);
  console.log("tipo unidad");

  console.log(entregadoPor);
  console.log(entregadoA);
  console.log(montoAPagar);

  if(entregadoPor && entregadoA && montoAPagar){

    swal({
      title: "¿Estás seguro de terminar el trámite?",
      text: "",
      icon: "info",
      buttons: true,
      dangerMode: true,
    })
    .then((value) => {

      $('#Modal-Entregado_'+id).modal('hide');
      document.getElementById("loader").style.display = "block";

      console.log(vin);
      console.log(placas);
      console.log(no_cliente);

      
      if(value){
        if(files !== ''){

        $.ajax({
              url: 'action/uploads_evidencia.php',
              type: 'post',
              data: archivos,
              contentType: false,
              processData: false,
              success: function(response) {
                  
                  $.ajax({
                    url:'changeStatus_tramites.php',
                    type: 'POST',
                    async: true,
                    data: {
                        'ID' : id,
                        'comentario' : comentarios,
                        'codigo_gestor' : codigo_gestor,
                        'entregadoA' : entregadoA,
                        'codigo_m' : entregadoPor,
                        'value_estatus' : '47',
                        'nombre' : nombre,
                        'no_cliente' :no_cliente,
                        'vin' : vin,
                        'placas' : placas
                    },
                    success: function(resp) {
                      location.reload(); 
                    },
                    error: function(request, err){
                        console.log(err);
                    }
                });

                  
                  files = '';
                }
              }); 
        }
      }
      else {
        //swal("Your imaginary file is safe!");
        //console.log("Nothing");
        document.getElementById("loader").style.display = "none";
      }

    });



  }else{
     swal({
      title: "Algunos campos estan vacios",
      text: "",
      icon: "info",
      buttons: ["Cancelar", true],
      dangerMode: true,
    })
  }
}


$(document).on('click', '.descargarDocumento', function(){

  $('#keyvalue').val(this.id);

  document.getElementById("formDescargarDocumento").submit();          

});

$(document).on('click', '.verDocumento', function(){

  //$('#keyvalue').val(this.id);


      $.ajax({
        url: 'show_file.php',
        type: 'post',
        data: {
          'key' : this.id
        },
        success: function(response) {

          //swal("Datos Actualizados", "", "success");
          window.open(response, "width=500,height=500,top=100,left=500");
        },
        error: function(request, err){
            console.log(err);
        }
      });

});


$(document).on('click', '.actualizarData', function(){

  //alert(this.id);

  let select_val = $('#tipos_documentos_'+this.id).val();
  
    let identificador= this.id;
    let values = identificador.split("_");
    let id = values[1];
    

    $.ajax({
      url: 'updateData.php',
      type: 'post',
      data: {
        'tbl_contratos' : id,
        'id_documento' : select_val
      },
      success: function(response) {

        swal("Datos Actualizados", "", "success");

      },
      error: function(request, err){
          console.log(err);
      }
    });
    

});



$(document).on('click', '.popup-documentos', function(){

  let identificador= this.id;
  let values = identificador.split("_");
  let id = values[0];
  let no_cliente = values[0];
  let respuesta_documentos = document.querySelector('#documentos');
  respuesta_documentos.innerHTML = '';

      $.ajax({
        url:'action/getDocumentos.php',
        type: 'POST',
        async: true,
        dataType: "json",
        data: {
            'id' : id
        },
        success: function(resp) {
          for(let i= 0; i<resp.length; i++){
            respuesta_documentos.innerHTML += `
            <tr align="center">
              <td>${resp[i]['FECHA']}</td>
              <td>${resp[i]['DOCUMENTO']}</td>
              <td>
                <!--<embed src="storage/doc.pdf" type="application/pdf" id="bloqueDocumento" height="400px" width="500">-->
                <button class="verDocumento btn-default" id="${resp[i]['DOCUMENTO']}">
                  <i class="fa fa-eye mx-1" aria-hidden="true"></i>
                </button>
                <button class="descargarDocumento btn-default" id="${resp[i]['DOCUMENTO']}">
                  <i class="fa fa-download" aria-hidden="true"></i>
                </button>
              </td>
              <td>
                <select name="options" id="tipos_documentos_${id}_${resp[i]['BIT_ID']}">
                  <option value="volvo" disabled>Seleccione una opcion</option>
                  <option value="1">FACTURA</option>
                  <option value="2">PÓLIZA DE SEGURO</option>
                  <option value="3">TARJETA DE CIRCULACIÓN</option>
                  <option value="4">CADENA DE FACTURAS</option>
                  <option value="5">LÍNEA DE CAPTURA</option>
                  <option value="6">COMPROBANTE DE PAGO</option>
                  <option value="7">PLACAS</option>
                  <option value="8">EVIDENCIA DE ENTREGA</option>
                  <option value="9">OTROS</option>

                </select>
              </td>
              <td><button class="actualizarData btn-default" id="${id}_${resp[i]['BIT_ID']}">Guardar</button></td>
            </tr>`;
          }
        },
        error: function(request, err){
            console.log(err);
        }
    });

});


$( ".comentarios" ).change(function() {

  var identificador= this.id;
  var values = identificador.split("_");
  
  var id = values[1];

  var estatus =$(this).val();

  console.log($('#textarea_'+id).val()); 

  $('#comentario_'+id).val($('#'+identificador).val());

});

$( ".gestores" ).change(function() {


  var identificador= this.id;
  var values = identificador.split("_");

  var id = values[1];
  
  console.log(id);

  $('#codigogestor_'+id).val($('#'+identificador).val());

});

  
$( ".estatus" ).change(function() {


  var identificador= this.id;
  var values = identificador.split("_");
  
  var id = values[1];

  var estatus =$(this).val();

  console.log($('#value_estatus_'+id).val()); 


  $('#value_estatus_'+id).val(estatus);

  console.log(estatus);
  console.log(id);
  console.log($('#value_estatus_'+id).val()); 
 
});

$(document).on('click', '.llamar-popup', function(){

  let id_cliente = $(this).attr('id');
  let respuesta_bitacora = document.querySelector('#respuesta_bitacora');
  respuesta_bitacora.innerHTML = '';

  $.ajax({
      url:'popup_bitacora.php',
      type: 'POST',
      async: true,
      dataType: "json",
      data: {
          'id_cliente' : id_cliente
      },
      success: function(respuesta) {
          if (respuesta == "") {
                  swal({
                      title: "Aviso!",
                      text: "No se encontraron datos.",
                      icon: "warning",
                      button: "Reintentar",
                  });
              }else {
                  //$('#respuesta-bitacora').val(respuesta_bitacora);
                  for(let i= 0; i<respuesta.length; i++){
                      respuesta_bitacora.innerHTML += `
                      <tr align="center">
                          <td>${respuesta[i][0]}</td>
                          <td>${respuesta[i][1]}</td>
                          <td>${respuesta[i][2]}</td>
                          <td>${respuesta[i][3]}</td>
                          <td>${respuesta[i][4]}</td>
                          <td>${respuesta[i][5]}</td>
                          <td>${respuesta[i][8]}</td>
                          <td>${respuesta[i][6]}</td>
                      </tr>
                      `
                  }
              }
      },
      error: function(request, err){
          console.log(err);
      }
  });

});

 
function formatoMexico(number){

  const exp = /(\d)(?=(\d{3})+(?!\d))/g;
  const rep = '$1,';
  let arr = number.toString().split('.');
  arr[0] = arr[0].replace(exp,rep);
  
  return arr[1] ? arr.join('.'): arr[0];

}


function sumar(num_cotizacion){

  let folio = $('#folio'+num_cotizacion).text();
  let derechos = parseInt($('#derechos_'+num_cotizacion).val());
  let tenencia = parseInt($('#tenencia_'+num_cotizacion).val());
  let tarjeta = parseInt($('#tarjeta_'+num_cotizacion).val());
  let otro = parseInt($('#otro_'+num_cotizacion).val());

  $('#total_'+num_cotizacion).text(formatoMexico(derechos+tenencia+tarjeta+otro));

  $.ajax({
    url: 'action/update_cotizacionesM.php',
    type: 'post',
    data: {
      'folio': folio,
      'derechos' : derechos,
      'tenencia' : tenencia,
      'tarjeta' : tarjeta,
      'otros' : otro
    },
    success: function(response){
      swal('Archivo cargado exitosamente', '', 'success');
    },
    error: function(request, err) {
      console.log(err);
    }
  });

}

function msmInTime(lb,text,id) {
  setTimeout(function(){  $('#'+lb+'_'+id).text(text) }, 1000);
}

function msmOutTime(lb,id) {
  setTimeout(function(){  $('#'+lb+'_'+id).text('') }, 5000);
}


function actualizar(id,value,type){

  let vin;
  let placas;

  if(type == 'vin'){
    vin = $('#textvin_'+id).val();
    placas = '';
  }
  
  if(type == 'placas'){
    placas = $('#textplacas_'+id).val();
    vin = '';
  }

   $.ajax({
        url:'action/updatevinplaca.php',
        type: 'POST',
        data: {
            'id' : id,
            'vin' : vin,
            'placas' : placas,
            'type' : type
        },
        success: function(resp) {

          $.ajax({
              url:'action/listar_tramites.php',
              type: 'POST',
              data: {
                  'id' : id
              },
              success: function(resp) {

                $('#textvin_'+id).val(resp[0]['VIN']);
                $('#textplacas_'+id).val(resp[0]['NO_PLACAS']);

                  
                  if(type == 'vin'){
                    msmInTime('lbvin','Actualizado',id);
                    msmOutTime('lbvin',id);  
                  }else if(type == 'placas'){
                    msmInTime('lbplacas','Actualizado',id);
                    msmOutTime('lbplacas',id);
                  }
              },
              error: function(request, err){
                  console.log(request);
                  console.log(err);
              }
          });
        },
        error: function(request, err){
            console.log(request);
            console.log(err);
        }
    });

}

function popup_entregado(id){

  let estatus = $('#value_estatus_'+id).val(); 
  let gestor = $('#gestores_'+id).val();

  console.log("estatus");
  console.log(estatus);
  console.log("estatus");
  console.log(id);


    console.log(gestor);

    if(gestor !== null){
      
      if(estatus !== '30' && estatus !== 'PENDIENTE' ){
        if(estatus === '47'){
          $('#Modal-Entregado_'+id).modal('show');
        }else if(estatus === '33'){
          $('#Modal-Incompleto_'+id).modal('show');
        }

        else{

          document.getElementById("loader").style.display = "block";
          document.getElementById("Form_Guardar"+id).submit(); 
          document.getElementById("loader").style.display = "none";
        }
        
      }else{
        swal({
          title: "Estatus no puede ser pendiente",
          text: "Ya tiene un gestor asignado",
          icon: "info",
          dangerMode: true,
        });
      }

      
    }else{
      swal({
          title: "El Gestor no puede ser vacio",
          text: "Seleccione un Gestor",
          icon: "info",
          dangerMode: true,
        });
    }
    

    
}

function sentIncompletos(id,no_cliente,nombre){

      let comentarios = $('#comentarios_popup_incompletos' + id).val();  
      let codigo = $('#codigoFiles' + id).val();  
      //let nombre = $('#n' + id).val();  



      console.log(comentarios);
      console.log(codigo);

      console.log(no_cliente);
      console.log(nombre);

      gno_cliente = no_cliente

      if(comentarios !== ''){

        $.ajax({
          url: 'action/incompletos.php',
          type: 'post',
          data: {
            'ID' : id ,
            'ESTATUS' : 33,
            'COMENTARIOS' : comentarios,
            'CODIGO' : codigo,
            'no_cliente' : no_cliente,
            'nombre' : nombre
          },
          success: function(response) {
            document.getElementById("loader").style.display = "none";
            console.log(response);
            location.reload(); 
          },
        });
      }else{
         swal({
          title: "Campo comentarios vacio",
          text: "Agrege un texto",
          icon: "info",
          dangerMode: true,
        });

      }
    

      
    }

//Funcion popup
$(document).on('click', '.llamar-popup-cot', function() {

  let id = $(this).attr('id');
  let nombre_cliente = $(this).attr('class');
  let spltittedName = id.substring(13);
  let texto_modal = document.querySelector('#texto-modal');
  let respuesta_cotizacion = document.querySelector('#respuesta_cotizacion');

  let vin = $('#vin_'+id).val();
  let placas = $('#placas_'+id).val();

  respuesta_cotizacion.innerHTML = '';
  texto_modal.innerHTML = '';
  
  $.ajax({
    url: 'action/listar_cotizaciones.php',
    type: 'POST',
    async: true,
    dataType: "json",
    data: {
      'id': id
    },
    success: function(respuesta) {
      texto_modal.innerHTML = `Iniciar Proceso de Cotización: ${spltittedName}`;
        
        let numberFormat = new Intl.NumberFormat();
        let valdisabled = '';

        for (let i = 0; i < respuesta.length; i++) {



          disabled = (respuesta[i]['ESTATUS'] !== 'ACEPTADA' ) ? 'disabled' : '' ;

          respuesta_cotizacion.innerHTML += `
            <tr align='center'>
              <td id='folio${i+1}' >${respuesta[i]['FOLIO']}</td>
              <td id='edo_origen${i+1}'>${respuesta[i]['EDO_COTIZACION']}</td>
              <td>${respuesta[i]['AUTOMATICA']}</td>
              <td>${respuesta[i]['TRAMITE']}</td>
              <td>${respuesta[i]['SLA']}</td>
              <td>${respuesta[i]['SLA_AVG']}</td>
              <td>
                <input type='text' id='derechos_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_DERECHOS']}' maxlength="6" size="4">
              </td>
              <td>
                <input type='text' id='tenencia_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_TENENCIA']}' maxlength="6" size="4">
              </td>
              <td>
                <input type='text' id='tarjeta_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_TARJETA']}' maxlength="6" size="4">
              </td>
              <td>
                <input type='text' id='otro_${i+1}' class='cotizacion' value='${respuesta[i]['MTO_OTRO']}' maxlength="6" size="4">
              </td>
              <td style='width:90px;' id='total_${i+1}' >${formatoMexico(parseInt(respuesta[i]['MTO_DERECHOS'])+parseInt(respuesta[i]['MTO_TENENCIA'])+
                parseInt(respuesta[i]['MTO_TARJETA'])+parseInt(respuesta[i]['MTO_OTRO']))}
              </td>
              <td id='estatus${i+1}' >${respuesta[i]['ESTATUS']}</td>
              <td>
                <button class='btn btn-info' onclick='sumar(${i+1})' ${disabled} >
                  <i class="fa fa-refresh" aria-hidden="true"></i>
                </button>
              </td>
            </tr>`
        }


         respuesta_cotizacion.innerHTML += `
          <br><br>
          <tr align='center'>
            <td>
              <input id='textvin_${id}' type='text' placeholder='VIN' value='${vin}' />
            </td>
            <td>
              <button id='vin' class='btn btn-info' onclick='actualizar(${id},"${vin}","vin")' > 
                <i class="fa fa-refresh" aria-hidden="true"></i>
              </button>
            </td>
            <td><label id='lbvin_${id}' style='color: #88cf87;'></label></td>
          </tr>
          <tr align='center'>
            <td>
              <input id='textplacas_${id}'type='text' placeholder='PLACAS' value='${placas}' />
            </td>
            <td>
              <button id='placas' class='btn btn-info' onclick='actualizar(${id},"${placas}","placas")'>
                <i class="fa fa-refresh" aria-hidden="true"></i>
              </button>
            </td>
            <td><label id='lbplacas_${id}' style='color: #88cf87;'></label></td>
          </tr>
          
          `;
    },
    error: function(request, err) {
      console.log(err);
    }
  });

});


$(".comprobante").change(function() {

    let identificador = $(this).attr('id');

    console.log(identificador);


    let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    let no_cliente = data[1];
    let archivos = new FormData();

    let factura = $('#Comprobante_' + no_cliente)[0].files[0];
    //.prop('files');
    //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

    let tipo_unidad = $("#unidad_" + no_cliente).text();
    let id = $('#ID_' + no_cliente).val();

    console.log(no_cliente);
    console.log(tipo_unidad);
    console.log(id);
    
    archivos.append('file', factura);
    archivos.append('no_cliente', no_cliente);
    archivos.append('tipo_unidad', tipo_unidad);
    archivos.append('id', id);
    archivos.append('archivo', 'Factura');
  

    swal({
        title: factura.name,
        text: "¿Desea cargar el archivo?",
        icon: "info",
        buttons: ["Cancelar", true],
        dangerMode: true,
      })
      .then((value) => {
        if (value) {
          $.ajax({
            url: 'action/uploads_comprobante.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response) {
              if (response > 0) {
                swal('Archivo cargado exitosamente', '', 'success');
                $('#Factura2_' + no_cliente).val('');
              } else {
                swal({
                  title: 'Error al cargar el archivo',
                  text: 'intentalo más tarde',
                  icon: 'error',
                  dangerMode: true,
                });
              }
            },
          });
        } else {
          $('#Comprobante_' + no_cliente).val('');
        }
      });
      

});

$(".tarjeta").change(function() {

    let identificador = $(this).attr('id');

    console.log(identificador);


    let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    let no_cliente = data[1];
    let archivos = new FormData();

    let factura = $('#Tarjeta_' + no_cliente)[0].files[0];
    //.prop('files');
    //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

    let tipo_unidad = $("#unidad_" + no_cliente).text();
    let id = $('#ID_' + no_cliente).val();

      console.log(no_cliente);
    console.log(tipo_unidad);
    console.log(id);
    
    archivos.append('file', factura);
    archivos.append('no_cliente', no_cliente);
    archivos.append('tipo_unidad', tipo_unidad);
    archivos.append('id', id);
    archivos.append('archivo', 'Factura');
  

    swal({
        title: factura.name,
        text: "¿Desea cargar el archivo?",
        icon: "info",
        buttons: ["Cancelar", true],
        dangerMode: true,
      })
      .then((value) => {
        if (value) {
          $.ajax({
            url: 'action/uploads_tarjeta.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response) {
              if (response > 0) {
                swal('Archivo cargado exitosamente', '', 'success');
                $('#Factura2_' + no_cliente).val('');
              } else {
                swal({
                  title: 'Error al cargar el archivo',
                  text: 'intentalo más tarde',
                  icon: 'error',
                  dangerMode: true,
                });
              }
            },
          });
        } else {
          $('#Tarjeta_' + no_cliente).val('');
        }
      });
      

});

$(".foto").change(function() {

    let identificador = $(this).attr('id');

    console.log(identificador);


    let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    let no_cliente = data[1];
    let archivos = new FormData();

    let factura = $('#Foto_' + no_cliente)[0].files[0];
    //.prop('files');
    //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

    let tipo_unidad = $("#unidad_" + no_cliente).text();
    let id = $('#ID_' + no_cliente).val();

    console.log(no_cliente);
    console.log(tipo_unidad);
    console.log(id);
    
    archivos.append('file', factura);
    archivos.append('no_cliente', no_cliente);
    archivos.append('tipo_unidad', tipo_unidad);
    archivos.append('id', id);
    archivos.append('archivo', 'Factura');
  

    swal({
        title: factura.name,
        text: "¿Desea cargar el archivo?",
        icon: "info",
        buttons: ["Cancelar", true],
        dangerMode: true,
      })
      .then((value) => {
        if (value) {
          $.ajax({
            url: 'action/uploads_foto.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response) {
              if (response > 0) {
                swal('Archivo cargado exitosamente', '', 'success');
                $('#Factura2_' + no_cliente).val('');
              } else {
                swal({
                  title: 'Error al cargar el archivo',
                  text: 'intentalo más tarde',
                  icon: 'error',
                  dangerMode: true,
                });
              }
            },
          });
        } else {
          $('#Foto_' + no_cliente).val('');
        }
      });
      

});

$(".linea").change(function() {

    let identificador = $(this).attr('id');

    console.log(identificador);


    let data = identificador.split("_"); // Obtiene la concatenación de tipo_unidad + no_cliente
    let no_cliente = data[1];
    let archivos = new FormData();

    let lineaCaptura = $('#LineaCaptura_' + no_cliente)[0].files[0];
    //.prop('files');
    //let factura2 = $('#Factura_' + no_cliente).prop('files')[0];

    let tipo_unidad = $("#unidad_" + no_cliente).text();
    let id = $('#ID_' + no_cliente).val();

    console.log(no_cliente);
    console.log(tipo_unidad);
    console.log(id);
    
    archivos.append('file', lineaCaptura);
    archivos.append('no_cliente', no_cliente);
    archivos.append('tipo_unidad', tipo_unidad);
    archivos.append('id', id);
    archivos.append('archivo', 'Factura');
  

    swal({
        title: lineaCaptura.name,
        text: "¿Desea cargar el archivo?",
        icon: "info",
        buttons: ["Cancelar", true],
        dangerMode: true,
      })
      .then((value) => {
        if (value) {
          $.ajax({
            url: 'action/uploads_lineaCaptura.php',
            type: 'post',
            data: archivos,
            contentType: false,
            processData: false,
            success: function(response) {
              if (response > 0) {
                swal('Archivo cargado exitosamente', '', 'success');
                $('#LineaCaptura_' + no_cliente).val('');
              } else {
                swal({
                  title: 'Error al cargar el archivo',
                  text: 'intentalo más tarde',
                  icon: 'error',
                  dangerMode: true,
                });
              }
            },
          });
        } else {
          $('#Foto_' + no_cliente).val('');
        }
      });
      

});


      
</script>

</html>

</div> <!-- este div cierra es el del principio -->


<?php include "footer.php"; ?>
