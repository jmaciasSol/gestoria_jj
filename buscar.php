<?php
session_start();
include_once "config/config.php";
include_once "action/encript.php";


$_SESSION['tiempo'] = time();

$tipo = 1;
$vin = $_POST['vin'];
$numero_cliente = $_POST['numero_cliente'];
$folio = $_POST['folio'];
$nombre = $_POST['nombre'];
$fecha_1 = $_POST['fecha_1'];
$fecha_2 = $_POST['fecha_2'];
$proceso = $_POST['proceso'];
$estatus = $_POST['estatus'];
$concatenacion = 0;


function showNames($allData,$newData){
    foreach ($allData as $data) {
        $array_temp = array();
        for($i = 0; $i < count($data); $i++){
            if($i == 3){
                array_push($array_temp, encrypt_decrypt('decrypt', $data[3]) );
            }else{
                array_push($array_temp, $data[$i]);   
            }
        }
        array_push($newData, $array_temp);
        $array_temp = array();
    }

    return $newData;
}

//Parte de vin
if (!empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and empty($proceso) and empty($estatus) ) {

    if (!empty($fecha_1) and !empty($fecha_2)) {
        $tipo = 1;
        $sp = $con->query("CALL customer($tipo,'$vin','$fecha_1','$fecha_2')");
        $sp_data = $sp->fetch_all();
        $datos = array();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode(showNames($sp_data,$datos));

    } else {
        $tipo = 1;
        $sp = $con->query("CALL customer($tipo,'$vin','1000-01-01','9999-12-31')");
        $sp_data = $sp->fetch_all();
        $datos = array();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode(showNames($sp_data,$datos));
    }
    
    //Parte Numero de Cliente
} elseif (empty($vin) and !empty($numero_cliente) and empty($folio) and empty($nombre) and empty($proceso) and empty($estatus)) {

    if (!empty($fecha_1) and !empty($fecha_2)) {
        $tipo = 2;
        $sp = $con->query("CALL customer($tipo,'$numero_cliente','$fecha_1','$fecha_2')");
        $sp_data = $sp->fetch_all();
        $datos = array();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode(showNames($sp_data,$datos));

    } else {
        $tipo = 2;
        $sp = $con->query("CALL customer($tipo,'$numero_cliente','1000-01-01','9999-12-31')");
        $sp_data = $sp->fetch_all();
        $datos = array();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode(showNames($sp_data,$datos));
    }
    
    //Para Folio
} elseif (empty($vin) and empty($numero_cliente) and !empty($folio) and empty($nombre) and empty($proceso) and empty($estatus)) {

    if (!empty($fecha_1) and !empty($fecha_2)) {
        $tipo = 3;
        $sp = $con->query("CALL customer($tipo,'$folio','$fecha_1','$fecha_2')");
        $sp_data = $sp->fetch_all();
        $datos = array();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode(showNames($sp_data,$datos));
    } else {
        $tipo = 3;
        $sp = $con->query("CALL customer($tipo,'$folio','1000-01-01','9999-12-31')");
        $sp_data = $sp->fetch_all();
        $datos = array();

        header('Content-type: application/json; charset=utf-8');
        echo json_encode(showNames($sp_data,$datos));
    }
    
    //Para nombre
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and !empty($nombre) and empty($proceso) and empty($estatus) and !empty($fecha_1) and !empty($fecha_2)) {
    $tipo = 4;
    $nombre = encrypt_decrypt('encrypt', $nombre);
    $sp = $con->query("CALL customer($tipo,'$nombre','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();
    $datos = array();

    header('Content-type: application/json; charset=utf-8');
    echo json_encode(showNames($sp_data,$datos));
    //Para Proceso
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and !empty($proceso) and (empty($estatus) && $estatus != '0') and !empty($fecha_1) and !empty($fecha_2)) {
    
    $tipo = 5;
    $sp = $con->query("CALL customer($tipo,'$proceso','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();
    $datos = array();

    header('Content-type: application/json; charset=utf-8');
    echo json_encode(showNames($sp_data,$datos));
    

    //Para Estatus
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and empty($proceso) and (!empty($estatus) || $estatus == '0') and !empty($fecha_1) and !empty($fecha_2)) {

    $tipo = 6;
    $sp = $con->query("CALL customer($tipo,'0','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();
    $datos = array();

    header('Content-type: application/json; charset=utf-8');
    echo json_encode(showNames($sp_data,$datos));

    //Para Proceso y Status
} elseif (empty($vin) and empty($numero_cliente) and empty($folio) and empty($nombre) and !empty($proceso) and (!empty($estatus) || $estatus == '0') and !empty($fecha_1) and !empty($fecha_2)) {
    
    $concatenacion = $proceso . $estatus;
    
    $tipo = 7;
    $sp = $con->query("CALL customer($tipo,'$concatenacion','$fecha_1','$fecha_2')");
    $sp_data = $sp->fetch_all();
    $datos = array();

    header('Content-type: application/json; charset=utf-8');
    echo json_encode(showNames($sp_data,$datos));
}
