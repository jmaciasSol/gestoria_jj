   <aside class="main-sidebar"><!-- Left side column. contains the logo and sidebar -->
        <section class="sidebar"><!-- sidebar: style can be found in sidebar.less -->
            <div class="user-panel"> <!-- Sidebar user panel -->
                 <div class="pull-left image">
                    <img src="images/profiles/default.png" class="img-circle" alt="User Image">
                    </div>
                <div class="pull-left info">
                    <p><?php echo $fullname; ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
             <ul class="sidebar-menu"><!-- sidebar menu: : style can be found in sidebar.less -->
                <li class="header">NAVEGACIÓN</li>

                    <li class="dropdown"  <?php if($comercial ==1 ){ echo "hidden";} ?>>
                                  
                        <a href="home.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
                        
                        <ul class="dropdown-menu" <?php if($usuario==1  || $contabilidad==1){echo "hidden";} ?>>
                            <li  >
                                <a href="home.php"><i class="fa fa-book"></i><span>Reportes</span></a>
                            </li>                   
                        </ul>     
                    </li>


                    <li class="dropdown" <?php if($contabilidad == 1 || $admin_crfact == 1){ echo "hidden";} ?>>
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                        <i class="fa fa-users "></i><span>Cotizaci&oacute;n</span></a>
                                <ul class="dropdown-menu">
                                        <li >
                                            <a href="Cotizacion.php"><i class="fa fa-file"></i> <span>Cotizaciones</span></a>
                                   
                                        </li>    
                           
                                        <li hidden>
                                            <a href="Revision.php"><i class="fa fa-file"></i> <span>Revisi&oacute;n</span></a>
                                   
                                        </li>
                                        <li <?php if($comercial == 0){ echo "hidden";} ?>>
                                            <a href="incompletos.php"><i class="fa fa-file"></i> <span>Incompletos</span></a>
                                   
                                        </li>

                                 </ul>
                    </li> 

                        <li class="dropdown" hidden>
                        <a href="newfolder.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" > <i class="fa fa-file"></i> <span>Facturas</span></a>
                                <ul class="dropdown-menu">

                                        <li  <?php if($contabilidad==1){ echo "hidden";} ?>>
                                            <a href="newfolder.php"><i class="fa fa-folder"></i> <span>Nueva carpeta</span></a>
                                        </li>

                                        <li  <?php if($contabilidad==1){ echo "hidden";} ?>>
                                            <a href="myfiles.php"><i class="fa fa-archive"></i> <span>Mis facturas</span></a>
                                        </li>
                                        
                                        <li <?php if($contabilidad==1){ echo "hidden";} ?> >
                                            <a href="newfile.php"><i class="fa fa-download"></i> <span>Nuevo Archivo</span></a>
                                        </li>
                
                                         <li >
                                            <a href="shared.php"><i class="fa fa-archive"></i> <span>Compartidos Conmigo</span></a>
                                        </li>
                                 </ul>
                    </li>         

                
                    <li class="dropdown" <?php if($contabilidad == 1 || $comercial==1){ echo "hidden";} ?>>
                        <a href="tbl_aceptados.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-upload"></i> <span>Seguimientos</span></a>
                        <ul class="dropdown-menu">
                               
                               <li>
                                    <a href="NTramites.php"><i class="fa fa-globe"></i> <span>Tramite</span></a>
                                </li>

                                <li>
                                    <a href="Pagos.php"><i class="fa fa-money"></i> <span>Pagos</span></a>
                                </li>                                        
                         </ul>
                    </li>  

                    <li class="dropdown" hidden>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                        <i class="fa fa-users "></i><span>Administracion</span></a>
                                <ul class="dropdown-menu">

                                        <li >
                                            <a href="#"><i class="fa fa-circle"></i> <span>Carga Bases Auditoria</span></a>
                                        </li>
                                        <li >
                                            <a href="#"><i class="fa fa-user-plus"></i><span>Gestion de usuarios</span></a>
                                        </li>
                                    
                                 </ul>
                    </li>

                       <li class="dropdown" hidden >
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-download"></i> <span>Otros Tramites</span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><i class="fa fa-phone-square"></i></i> <span>Call Center</span></a>
                                </li>
                            </ul>
                        </li> 


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-search"></i> <span><?php echo htmlspecialchars("Búsqueda"); ?></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="find_your_client.php"><i class="fa fa-search"></i></i> <span>Busca tu cliente</span></a>
                                </li>
                               <!-- <li>
                                    <a href="S3.php"><i class="fa fa-search"></i></i> <span>S3</span></a>
                                </li> -->
                            </ul>
                    </li>

                    <li class="dropdown" <?php if($admin_crfact == 0 && $admin_jj == 0){ echo "hidden";} ?>>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-lock"></i> <span><?php echo "API Key"; ?></span></a>
                            <ul class="dropdown-menu">
                                <li >
                                    <a href="Genera_APIKey.php"><i class="fa fa-key"></i></i> <span>Genera tu API Key</span></a>
                                </li>
                                <li <?php if($admin_jj == 0){ echo "hidden";} ?>>
                                    <a href="Registra_APIKey.php"><i class="fa fa-shield"></i></i> <span>Registra API Key</span></a>
                                </li>
                                <li>
                                    <a href="makeZip.php">Generar Zip</a>
                                </li>
                            </ul>
                    </li>
                    
            </ul>
        </section><!-- /.sidebar -->
    </aside>
