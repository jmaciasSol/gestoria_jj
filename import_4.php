<?php
include_once "config/config.php";
include "class.upload.php";

session_start();

$user_id=$_SESSION["user_id"];
$datos = $con->query("SELECT EMAIL FROM tbl_usuarios WHERE ID = $user_id");

$d= $datos->fetch_object();
$correo=$d->EMAIL;
$registros_fallidos = array();
$registros_exitosos = array();

if(isset($_FILES["name"])){
	$up = new Upload($_FILES["name"]);
	if($up->uploaded){
		$up->Process("./Output/");
		if($up->processed){
            /// leer el archivo excel
            require_once 'PHPExcel/Classes/PHPExcel.php';
            $archivo = "Output/".$up->file_dst_name;
            $inputFileType = PHPExcel_IOFactory::identify($archivo);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($archivo);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow();

            $highestColumn = $sheet->getHighestColumn();

            if($archivo){
                $ruta_destino_archivo = "Output/".$_FILES["name"]["name"];
                $archivo_ok = move_uploaded_file($_FILES["name"]['tmp_name'], $ruta_destino_archivo);
            }else{
                $ruta_destino_archivo='';
            }

            if($highestColumn != 'O'){
                echo "<script>
                    window.location = './Cotizacion.php?success=error';
                </script>"; 
            }else{


                for ($row = 2; $row <= $highestRow; $row++){ 

                    $no_cliente = $sheet->getCell("A".$row)->getValue();
                    $nombre = $sheet->getCell("B".$row)->getValue();
                    $tipo = $sheet->getCell("C".$row)->getValue();
                    $marca = $sheet->getCell("D".$row)->getValue();
                    $modelo = $sheet->getCell("E".$row)->getValue();

                    $version = $sheet->getCell("F".$row)->getValue();
                    $anio = $sheet->getCell("G".$row)->getValue();
                    $valor_factura = floatval($sheet->getCell("H".$row)->getValue());
                    $edo_origen = $sheet->getCell("I".$row)->getValue();
                    $pdv = $sheet->getCell("J".$row)->getValue();

                    $vendedor = $sheet->getCell("K".$row)->getValue();
                    $ejecutivo = $sheet->getCell("L".$row)->getValue(); 
                    $sheet->getCell("M".$row)->getValue();
                    $edo_cotizacion_1 = empty($sheet->getCell("M".$row)->getValue()) ? 'NA' : $sheet->getCell("M".$row)->getValue();
                    $edo_cotizacion_2 = empty($sheet->getCell("N".$row)->getValue()) ? 'NA' : $sheet->getCell("N".$row)->getValue();
                    $edo_cotizacion_3 = empty($sheet->getCell("O".$row)->getValue()) ? 'NA' : $sheet->getCell("O".$row)->getValue();


                    $registro = array(
                        'no_cliente' => $no_cliente,
                        'nombre' => $nombre,
                        'tipo' => $tipo,
                        'marca' => $marca,
                        'modelo' => $modelo,
                        'version' => $version,
                        'anio' => $anio,
                        'valor_factura' => $valor_factura,
                        'edo_origen' => $edo_origen,
                        'pdv' => $pdv,
                        'vendedor' => $vendedor,
                        'ejecutivo' => $ejecutivo,
                        'edo_cotizacion_1' => $edo_cotizacion_1,
                        'edo_cotizacion_2' => $edo_cotizacion_2,
                        'edo_cotizacion_3' => $edo_cotizacion_3,
                    
                    );


                    if($registro['edo_cotizacion_1'] == "NA" || empty($registro['no_cliente']) ){
                        array_push($registros_fallidos, $registro);
                        
                    }else{
                        array_push($registros_exitosos, $registro);
                    }

                }

                if(count($registros_fallidos) > 0){
                    if(count($registros_fallidos) == $highestRow-1){
                       echo "<script>
                        window.location = './Cotizacion.php?success=error';
                        </script>";
                    }else{
                        
                        foreach ($registros_exitosos as $registro_exitoso) {

                            $sql ="INSERT INTO tbl_staging(TRAMITE, NO_CLIENTE, NOMBRE, TIPO, MARCA, MODELO, VERSION, ANIO, VALOR_FACTURA, EDO_ORIGEN, PDV, VENDEDOR, EJECUTIVO, EDO_COTIZACION_1,EDO_COTIZACION_2,EDO_COTIZACION_3,USUARIO)
                            VALUES ";
                            
                            $tramite = 1;
                            $no_cliente = $registro_exitoso['no_cliente'];
                            $nombre = $registro_exitoso['nombre'];
                            $tipo = $registro_exitoso['tipo'];
                            $marca = $registro_exitoso['marca'];
                            $modelo = $registro_exitoso['modelo'];
                            $version = $registro_exitoso['version'];
                            $anio = $registro_exitoso['anio'];
                            $valor_factura = $registro_exitoso['valor_factura'];
                            $edo_origen = $registro_exitoso['edo_origen'];
                            $pdv = $registro_exitoso['pdv'];
                            $vendedor = $registro_exitoso['vendedor'];
                            $ejecutivo = $registro_exitoso['ejecutivo'];
                            $edo_cotizacion_1 = $registro_exitoso['edo_cotizacion_1'];
                            $edo_cotizacion_2 = $registro_exitoso['edo_cotizacion_2'];
                            $edo_cotizacion_3 = $registro_exitoso['edo_cotizacion_3'];
                            
                            $sql .= " (
                            '$tramite',
                            '$no_cliente',
                            '$nombre',
                            '$tipo',
                            '$marca',
                            '$modelo',
                            '$version',
                            '$anio',
                            '$valor_factura',
                            '$edo_origen',
                            '$pdv',
                            '$vendedor',
                            '$ejecutivo',
                            '$edo_cotizacion_1',
                            '$edo_cotizacion_2',
                            '$edo_cotizacion_3',
                            '$correo'
                            )"; 

                            $con->query($sql);                               
                        
                        }
                     
                        $_SESSION['success'] = "true";
                        print "<script>window.location='Cotizacion.php';</script>";
    
                    }
                    
                }else{

                        foreach ($registros_exitosos as $registro_exitoso) {
                        
                                $sql ="INSERT INTO tbl_staging(TRAMITE, NO_CLIENTE, NOMBRE, TIPO, MARCA, MODELO, VERSION, ANIO, VALOR_FACTURA, EDO_ORIGEN, PDV, VENDEDOR, EJECUTIVO, EDO_COTIZACION_1,EDO_COTIZACION_2,EDO_COTIZACION_3,USUARIO)
                                VALUES ";
                            
                                $tramite = 1;
                                $no_cliente = $registro_exitoso['no_cliente'];
                                $nombre = $registro_exitoso['nombre'];
                                $tipo = $registro_exitoso['tipo'];
                                $marca = $registro_exitoso['marca'];
                                $modelo = $registro_exitoso['modelo'];
                                $version = $registro_exitoso['version'];
                                $anio = $registro_exitoso['anio'];
                                $valor_factura = $registro_exitoso['valor_factura'];
                                $edo_origen = $registro_exitoso['edo_origen'];
                                $pdv = $registro_exitoso['pdv'];
                                $vendedor = $registro_exitoso['vendedor'];
                                $ejecutivo = $registro_exitoso['ejecutivo'];
                                $edo_cotizacion_1 = $registro_exitoso['edo_cotizacion_1'];
                                $edo_cotizacion_2 = $registro_exitoso['edo_cotizacion_2'];
                                $edo_cotizacion_3 = $registro_exitoso['edo_cotizacion_3'];
                                
                                $sql .= " (
                                '$tramite',
                                '$no_cliente',
                                '$nombre',
                                '$tipo',
                                '$marca',
                                '$modelo',
                                '$version',
                                '$anio',
                                '$valor_factura',
                                '$edo_origen',
                                '$pdv',
                                '$vendedor',
                                '$ejecutivo',
                                '$edo_cotizacion_1',
                                '$edo_cotizacion_2',
                                '$edo_cotizacion_3',
                                '$correo'
                                )"; 

                                $con->query($sql);

                        }

                        $archivo = (isset($_FILES["name"])) ? $_FILES["name"] : null;
           
                        $_SESSION['success'] = "true";
                        print "<script>window.location='Cotizacion.php';</script>";
                }
            }
        }	      
    }
}

?>
